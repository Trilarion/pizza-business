#ifndef _JS_WNDPROCS_H
#define _JS_WNDPROCS_H
/*----------------------------------------------------------------------------------------------*/
#include "data.h"
/*----------------------------------------------------------------------------------------------*/
// this structure is passed to the Recipes DlgProc
struct RecipeDlgData {
	List<RecipeData> *pRecipeList;
	List<IngCategory> *pIngCatList;
};
// this structure is passed to Recipe Builder DlgProc
struct RecipeDataEdit {
	RecipeData *pRecipeData;
	List<IngCategory> *pIngCatList;
};

struct IngCtrlData {
	IngCategory *pCat;
	IngData *pIngData;
};
/*----------------------------------------------------------------------------------------------*/
BOOL CALLBACK AboutProc(HWND hDlg, UINT iMessage, WPARAM wParam, LPARAM lParam);
BOOL CALLBACK AdProc(HWND hDlg, UINT iMessage, WPARAM wParam, LPARAM lParam);
BOOL CALLBACK AdvertisementProc(HWND hDlg, UINT iMessage, WPARAM wParam, LPARAM lParam);
BOOL CALLBACK ChairProc(HWND hDlg, UINT iMessage, WPARAM wParam, LPARAM lParam);
BOOL CALLBACK DescriptionProc(HWND hDlg, UINT iMessage, WPARAM wParam, LPARAM lParam);
BOOL CALLBACK FirstNameProc(HWND hDlg, UINT iMessage, WPARAM wParam, LPARAM lParam);
BOOL CALLBACK IngredProc(HWND hDlg, UINT iMessage, WPARAM wParam, LPARAM lParam);
BOOL CALLBACK IngredientProc(HWND hDlg, UINT iMessage, WPARAM wParam, LPARAM lParam);
BOOL CALLBACK MinMaxProc(HWND hDlg, UINT iMessage, WPARAM wParam, LPARAM lParam);
BOOL CALLBACK OptionsProc(HWND hDlg, UINT iMessage, WPARAM wParam, LPARAM lParam);
BOOL CALLBACK OvenProc(HWND hDlg, UINT iMessage, WPARAM wParam, LPARAM lParam);
BOOL CALLBACK PizzaBuilderProc(HWND hDlg, UINT iMessage, WPARAM wParam, LPARAM lParam);
BOOL CALLBACK RecipesProc(HWND hDlg, UINT iMessage, WPARAM wParam, LPARAM lParam);
BOOL CALLBACK TableProc(HWND hDlg, UINT iMessage, WPARAM wParam, LPARAM lParam);
/*----------------------------------------------------------------------------------------------*/
#endif