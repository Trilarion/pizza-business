Tony G, Sunday 25th 2002, 20:04 (GMT +02)
0.99.3 (unstable)

This is the third unstable release.
#Standard message#
If you want the menus to show up correctly, 
unpack graphics.zip to the Bin/ directory, 
and do not forget to move entities.pbd from data/ to Bin/ ..
(Note: *all* binary-related files should be placed in the same directory)

Changes:
--------
 - Most of the menu functions have been completed. 
   - See "TODO.txt" for more info.
 - this textfile has been reorganized, concerning the Standard message, below.
 - Loading and saving of games works!
 - Auto-pricing for pizzas is completed.
 - "End Turn" button in main window now ends the game's current turn.
 - "New Game" button in main menu window now starts a new game session.

(For more info, see "notes.txt")

 

Tony G, Sunday 18th 2002, 21:17 (GMT +02)
0.99.2 (unstable)

This is the second unstable release.
#Standard message#

Changes:
--------
 - Staff menu works in conjunction with entities.pbd
 - Main menu + Restaurant view (for future use)
 - Navigation menu on the right (mouse over effects)



Tony G, Monday 12th 2002, 23:45 (GMT +02)
0.99.1 (unstable)

This is the first unstable release.


Changes:
--------
First release. No changes ;)





Standard message:

It's a pure CVS sync, meant for people who want to experiment with
the code, and keep up with reasonable fresh changes to the code.
If you make any changes, please give me those files and I can add
the changes for the next release. These unstable releases will be made
once a week, because I believe more often wouldn't be necessary.
Just more hard work for the Freshmeat.net crew.

// tsarkon@users.sourceforge.net

