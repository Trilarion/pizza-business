#!/bin/sh
 
printf "Project Validator script 1.3\nby tsarkon <tsarkon@users.sourceforge.net>\n\n"
printf "Hi, $USER!\n"
printf "Checking number of lines in project ...\n"
printf "amount of cpp-code is \t\t\t`cat *.cpp | wc -l` lines\n"
printf "amount of header-code is \t\t`cat *.h | wc -l` lines\n"
echo   "---------------------------------------"
printf "amount of code in the whole project is \t`cat *.cpp *.h | wc -l` lines\n"
printf "length of Makefile is \t\t\t`cat Makefile | wc -l` lines\n"
printf "`pwd` contains \t`ls -l |wc -l` files\n"
printf "Directory tree structure, contains \t`ls -l -R | wc -l` files\n"
printf "Verifying integrity of license file ...\t"

	if [ ! -r `pwd`/COPYING ] && [ ! -r `pwd`/LICENSE ]; then
	echo "WARNING! Project license file is missing!"
	fi

	if [ -r `pwd`/COPYING ]; then
	LICENSE_FILE="COPYING"
	fi

        if [ -r `pwd`/LICENSE ]; then
        LICENSE_FILE="LICENSE"
        fi


	if [ ! `cat COPYING | wc --bytes |cut -c 3-7` = '17992' ]; then
	printf "WARNING! $LICENSE_FILE is corrupted or modified. This is an non-authorized license file!\n"
	fi

        if [ `cat COPYING | wc --bytes |cut -c 3-7` = '17992' ]; then
        printf "$LICENSE_FILE is verified as OK!\n"
        fi     

