#ifndef _JS_MAINWND_H
#define _JS_MAINWND_H
/*----------------------------------------------------------------------------------------------*/
#include "window.h"
#include "menuwnd.h"
#include "menu.h"
#include "hoverbtn.h"
#include "audio/oggplayer.h"
/*----------------------------------------------------------------------------------------------*/
class CMainWindow : public CWindow
{
public: // construction/destruction
	CMainWindow(HINSTANCE hInstance, CApplication *pApplication);
	~CMainWindow();

public:
	long WndProc(UINT iMessage, WPARAM wParam, LPARAM lParam);

private: // window message handlers
	void OnCommand(WPARAM wParam, LPARAM lParam);
	void OnDrawItem(LPDRAWITEMSTRUCT lpdis);
	void OnPaint();

private:
	// called from within DisplayMenuWindow()
	bool OnCommand_LoadGame();
	bool OnCommand_SaveGame();

private:
	void CreateNavButtons();
	void CreateMainWindow();
	void DisplayMenuWindow();
	void InitializeGDI();
	void Register();

private:
	CRestaurant* GetCurrentRestaurant();

private:
	static char szClassName[41];
	static bool registered;

	CMenuWindow *m_pMenuWindow;

	// menu stuff
	CMenu *m_pStaffMenu;

	// buttons
	CHoverButton *m_pBtnStats;
	CHoverButton *m_pBtnStaff;
	CHoverButton *m_pBtnBuySell;
	CHoverButton *m_pBtnPizzas;
	CHoverButton *m_pBtnBank;
	CHoverButton *m_pBtnEndTurn;
	CHoverButton *m_pBtnMenu;

	BOOL bShowFace;
	short m_navbar_height;
	short m_navbar_width;

	short m_navbtn_width;
	short m_navbtn_height;

	RECT m_rcBudget;

	COggPlayer m_OggPlayer;

private: // painting handles
	HBRUSH hbrBlack;
	HBITMAP hBmpBground;
	HDC memDC1;
	HBITMAP oldBmp1;
	HFONT hFontBudget;
};
/*----------------------------------------------------------------------------------------------*/
#endif