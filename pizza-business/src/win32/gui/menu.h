#ifndef _JS_MENUS_H
#define _JS_MENUS_H
/*----------------------------------------------------------------------------------------------*/
#include "window.h"
/*----------------------------------------------------------------------------------------------*/
class Menu;
/*----------------------------------------------------------------------------------------------*/
typedef struct tagMENUITEM{
	UINT command;
	char text[256];
}MENUITEM;
/*----------------------------------------------------------------------------------------------*/
class CMenu : public CWindow
{
public: // construction/destruction
	CMenu(HWND hParent, HINSTANCE hInstance);
	~CMenu();

public: // member functions
	void Register(HINSTANCE hInstance);
	long WndProc(UINT iMessage, WPARAM wParam, LPARAM lParam);

	// functions
	void Add_MenuItem(char* text, UINT command_id) { 
		strcpy(menu_items[menu_count].text, text);
		menu_items[menu_count].command = command_id;

		menu_count++; 
	}
	MENUITEM* GetMenuItem(int index) { return &menu_items[index-1]; }
	void SetCaption(char* newCaption) { strcpy(caption, newCaption); }
	void ShowMenu(int x, int y, bool bLeft = true);

private: // hidden functions
	void GetItemRect(int index, RECT* rc);
	void Paint();

private: // hidden data
	static bool registered;

	MENUITEM menu_items[20];
	int menu_count;
	char caption[256];
	int index_sel;

	int caption_height;
	int item_height;
	int text_offset;
};
/*----------------------------------------------------------------------------------------------*/
#endif