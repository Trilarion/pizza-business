#include "wnd_pizza.h"
/*----------------------------------------------------------------------------------------------*/
#define IDC_PMDLG_OK		1
#define IDC_PMDLG_CANCEL	2
#define IDC_PMDLG_LISTBOX	101
#define IDC_PMDLG_NEW		102
#define IDC_PMDLG_EDIT		103
#define IDC_PMDLG_DELETE	104
#define IDC_PMDLG_AUTOPRICE	105
#define IDC_PMDLG_PERCENT	106
#define IDC_PMDLG_SPINCTRL	107

#define IDC_PEDLG_OK		1
#define IDC_PEDLG_CANCEL	2
#define IDC_PEDLG_PRICE		101
#define IDC_PEDLG_AUTOPRICE	102
#define IDC_PEDLG_COST		103
/*----------------------------------------------------------------------------------------------*/
CPizzaObject* CPizzasDlg::GetSelectedPizza()
{
	HWND hwndLB = GetDlgItem(this->GetHandle(), IDC_PMDLG_LISTBOX);

	int iSel = SendMessage(hwndLB, LB_GETCURSEL, NULL, NULL);
	CPizzaObject *pPizzaObject = (CPizzaObject*)SendMessage(hwndLB, LB_GETITEMDATA, iSel, NULL);
	
	if((LPARAM)pPizzaObject == LB_ERR)
		return 0;
	else
		return pPizzaObject;
}
/*----------------------------------------------------------------------------------------------*/
void CPizzasDlg::OnCommand(WPARAM wParam, LPARAM lParam)
{
	switch(LOWORD(wParam))
	{
		case 1:
		case 2:
			EndDialog(hWnd, FALSE);
			break;

		case IDC_PMDLG_AUTOPRICE:
			if(BST_CHECKED == SendDlgItemMessage(hWnd, IDC_PMDLG_AUTOPRICE, BM_GETCHECK, NULL, NULL))
				this->m_pRestaurant->EnableAutoPricing(true);
			else
				this->m_pRestaurant->EnableAutoPricing(false);
			this->RefreshPizzaListBox();
			break;

		case IDC_PMDLG_PERCENT:
			{
				// set the new auto-pricing percentage
				char cPrice[10];
				GetWindowText(GetDlgItem(hWnd, IDC_PMDLG_PERCENT), cPrice, 9);

				this->m_pRestaurant->SetAutoPricePercentage((short)atoi(cPrice));
				this->RefreshPizzaListBox();
			}	
			break;

		case IDC_PMDLG_EDIT:
			{
				// get the restaurant object from the listbox
				CPizzaObject *pPizza = this->GetSelectedPizza();
				if(pPizza)
				{
					CPizzaEditDlg dlg;
					dlg.SetRestaurant(this->m_pRestaurant);
					dlg.SetPizza(pPizza);
					if(dlg.DoModalDlg("PIZZA_EDIT", hWnd, this->m_hInstance) == TRUE)
						this->RefreshPizzaListBox();
				}
			}
			break;
	}
}
/*----------------------------------------------------------------------------------------------*/
void CPizzasDlg::OnInitDialog()
{
	SetWindowText(hWnd, "Restaurant's Pizzas");
	// set the auto-pricing editbox
	char percentage[10];
	itoa(this->m_pRestaurant->GetAutoPricePercentage(), percentage, 10);
	SetWindowText(GetDlgItem(hWnd, IDC_PMDLG_PERCENT), percentage);
	// enable the auto-princing checkbox?
	if(this->m_pRestaurant->IsAutoPricingEnabled() == true)
		SendDlgItemMessage(hWnd, IDC_PMDLG_AUTOPRICE, BM_SETCHECK, BST_CHECKED, NULL);
	else
		EnableWindow(GetDlgItem(hWnd, IDC_PMDLG_PERCENT), FALSE);

	this->RefreshPizzaListBox();
}
/*----------------------------------------------------------------------------------------------*/
void CPizzasDlg::RefreshPizzaListBox()
{
	HWND hwndLB = GetDlgItem(this->GetHandle(), IDC_PMDLG_LISTBOX);
	SendMessage(hwndLB, LB_RESETCONTENT, NULL, NULL);

	for(unsigned int c = 1 ; c <= this->m_pRestaurant->m_PizzaList.GetCount() ; c++)
	{
		CPizzaObject *pPizzaObject = this->m_pRestaurant->m_PizzaList.GetItem(c);
		// get the price of the pizza
		float pizza_price = pPizzaObject->GetPrice();
		// get the name of the pizza
		char pizza_name[256];
		CPizza *pPizza = pPizzaObject->GetPizza();
		pPizza->GetName(pizza_name);
		// adjust the price if auto-pricing is enabled
		if(this->m_pRestaurant->IsAutoPricingEnabled() == true)
		{
			float production_cost = pPizza->GetProductionCost();
			pizza_price = production_cost + (production_cost * (this->m_pRestaurant->GetAutoPricePercentage()*.1));
		}
		// add the item to the listbox
		char text[280];
		sprintf(text, "%s - $%.2f", pizza_name, pizza_price);
		int index = SendMessage(hwndLB, LB_ADDSTRING, NULL, (LPARAM)(LPCSTR)text);
		SendMessage(hwndLB, LB_SETITEMDATA, index, (LPARAM)pPizzaObject);
	}
}
/*----------------------------------------------------------------------------------------------*/
long CPizzasDlg::WndProc(UINT iMessage, WPARAM wParam, LPARAM lParam)
{
	switch(iMessage)
	{
		case WM_INITDIALOG:
			this->OnInitDialog();
			break;

		case WM_COMMAND:
			this->OnCommand(wParam, lParam);
			break;

	}
	return 0;
}
/*-----------------------------------------------------------------------------------------------+
|																								 |
|	CPizzaEditDlg																				 |
|																								 |
+-----------------------------------------------------------------------------------------------*/
void CPizzaEditDlg::OnCommand(WPARAM wParam, LPARAM lParam)
{
	switch(LOWORD(wParam))
	{
		case IDC_PEDLG_OK:
			{
				// set the new price of the pizza
				char text[11];
				GetWindowText(GetDlgItem(hWnd, IDC_PEDLG_PRICE), text, 10);
				this->m_pPizza->SetPrice(atof(text));
				EndDialog(hWnd, TRUE);
			}
			break;

		case IDC_PEDLG_CANCEL:
			EndDialog(hWnd, FALSE);
			break;
	}
}
/*----------------------------------------------------------------------------------------------*/
void CPizzaEditDlg::OnInitDialog()
{
	if(!this->m_pPizza || !this->m_pRestaurant) return;

	// set the caption to the name of the pizza
	char pizza_name[256];
	this->m_pPizza->GetPizza()->GetName(pizza_name);
	SetWindowText(hWnd, pizza_name);

	// set the price of the pizza in the editbox
	char price[10];
	float pizza_price = this->m_pPizza->GetPrice();
	sprintf(price, "%.2f", pizza_price);
	SetWindowText(GetDlgItem(hWnd, IDC_PEDLG_PRICE), price);

	// make the autoprice text visible; disable the editbox
	// if the autoprice is enabled for the restaurant
	if(this->m_pRestaurant->IsAutoPricingEnabled() == true)
	{	
		EnableWindow(GetDlgItem(hWnd, IDC_PEDLG_PRICE), FALSE);
		ShowWindow(GetDlgItem(hWnd, IDC_PEDLG_AUTOPRICE), SW_NORMAL);
	}

	// set the production cost text
	char cost[10];
	float production_cost = this->m_pPizza->GetPizza()->GetProductionCost();
	sprintf(cost, "$%.2f", production_cost);
	SetWindowText(GetDlgItem(hWnd, IDC_PEDLG_COST), cost);
}
/*----------------------------------------------------------------------------------------------*/
long CPizzaEditDlg::WndProc(UINT iMessage, WPARAM wParam, LPARAM lParam)
{
	switch(iMessage)
	{
		case WM_INITDIALOG:
			this->OnInitDialog();
			break;

		case WM_COMMAND:
			this->OnCommand(wParam, lParam);
			break;

	}
	return 0;
}
/*----------------------------------------------------------------------------------------------*/