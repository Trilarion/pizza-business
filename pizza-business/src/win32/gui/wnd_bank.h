#ifndef _JS_WND_BANK_H
#define _JS_WND_BANK_H
/*----------------------------------------------------------------------------------------------*/
#include "window.h"
#include "telnet.h"
#include "..\..\datarest.h"
/*----------------------------------------------------------------------------------------------*/
class CBankDlg : public CDialog
{
public:
	CBankDlg(CRestaurant *pRestaurant);

public:
	long WndProc(UINT iMessage, WPARAM wParam, LPARAM lParam);

private: // window message handlers
	void OnCommand(WPARAM wParam, LPARAM lParam);
	void OnInitDialog();

private:
	void RefreshLoanBox();

private: // internal processing
	void InitBankServices(CTelnet &telnet);
	// called from InitBankServices
	void InitLoanProcess(CTelnet &telnet);
	void InitPayLoanProcess(CTelnet &telnet);
	void PrintWelcomeText(CTelnet &telnet, bool bConnect);

private:
	CRestaurant *m_pRestaurant;
};
/*----------------------------------------------------------------------------------------------*/
#endif