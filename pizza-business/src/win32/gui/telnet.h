#ifndef _JS_TELNET_H
#define _JS_TELNET_H
/*----------------------------------------------------------------------------------------------*/
#include "window.h"
#include "list.h"

using namespace ListH;
/*----------------------------------------------------------------------------------------------*/
class CTelnet : public CWindow
{
public:
	void Register(HINSTANCE hInstance);
	static bool bRegistered;

public:
	CTelnet(HWND hParent, DWORD dwExStyle, DWORD dwStyle, int id, int x, int y,
		int width, int height, HINSTANCE hInstance);
	~CTelnet();

public:
	long WndProc(UINT iMessage, WPARAM wParam, LPARAM lParam);

private:
	void OnChar(char character);
	void OnPaint();
	void OnTimer(UINT wTimerID);

public:	// Telnet functions
	void Telnet_Clear();
	void Telnet_NewLine();
	void Telnet_Print(char* text);
	char* Telnet_Prompt(char* prompt);

	bool IsWaitingInput() { return bInputCatch; }

private: // Telnet
	static char szClassName[41];

	List<char> m_TextList;
	short m_curIndex;

	short text_height;
	bool m_bCurShow;

	// input
	bool bInputCatch;
	char* input_text;
	short input_index;

	int cursor_x, cursor_y;
	HFONT hFont, oldFont;
};
/*----------------------------------------------------------------------------------------------*/
#endif