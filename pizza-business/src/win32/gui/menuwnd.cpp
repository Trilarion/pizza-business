#include "menuwnd.h"
/*----------------------------------------------------------------------------------------------*/
CMenuWindow::CMenuWindow(HINSTANCE hInstance, bool bEnableResume) :
m_pBtnResume(0), m_pBtnSaveGame(0)
{
	m_bEnableResume = bEnableResume;
	this->m_hInstance = hInstance;
}
/*----------------------------------------------------------------------------------------------*/
CMenuWindow::~CMenuWindow()
{
	SelectObject(memDC, oldBmp);
	DeleteObject(hBmp);

	// delete the buttons
	delete m_pBtnNewGame;
	delete m_pBtnLoadGame;

	if(m_pBtnSaveGame)
		delete m_pBtnSaveGame;

	delete m_pBtnQuit;

	if(m_pBtnResume)
		delete m_pBtnResume;

	DestroyWindow(hWnd);
}
/*----------------------------------------------------------------------------------------------*/
char CMenuWindow::szClassName[] = "pbMainMenu";
bool CMenuWindow::registered = false;
/*----------------------------------------------------------------------------------------------*/
INT CMenuWindow::DoModalDialog(HWND hParent)
{
	if(this->registered == false)
		this->Register();

	// center the window
	RECT rc, rcParent;
	GetWindowRect(hParent, &rcParent);
	short wnd_width = rcParent.right-rcParent.left;
	short wnd_height = rcParent.bottom-rcParent.top;
	rc.right = 400; rc.bottom = 300;
	rc.left = rcParent.left + (wnd_width / 2) - (rc.right / 2);
	rc.top = rcParent.top + (wnd_height / 2.5) - (rc.bottom / 2);

	hWnd = ::CreateWindow(szClassName, szClassName,
		WS_POPUPWINDOW,//|WS_VISIBLE|WS_BORDER, 
		rc.left, rc.top, rc.right, rc.bottom,
		hParent, NULL, this->m_hInstance, (LPSTR)this);

	// create the buttons
	m_pBtnNewGame = new CHoverButton(hWnd, IDC_NEWGAME, WS_CHILD|WS_VISIBLE|BS_OWNERDRAW,
		"New Game", m_hInstance, 105, 102, 125, 25);

	m_pBtnLoadGame = new CHoverButton(hWnd, IDC_LOADGAME, WS_CHILD|WS_VISIBLE|BS_OWNERDRAW,
		"Load Game", m_hInstance, 173,138, 130, 25);

	if(this->m_bEnableResume == true)
		m_pBtnSaveGame = new CHoverButton(hWnd, IDC_SAVEGAME, WS_CHILD|WS_VISIBLE|BS_OWNERDRAW,
			"Save Game", m_hInstance, 230, 172, 125, 25);

	if(this->m_bEnableResume == true)
		m_pBtnResume = new CHoverButton(hWnd, IDC_RESUME, WS_CHILD|WS_VISIBLE|BS_OWNERDRAW,
			"Save Game", m_hInstance, 292, 210, 94, 25);

	m_pBtnQuit = new CHoverButton(hWnd, IDC_EXIT, WS_CHILD|WS_VISIBLE|BS_OWNERDRAW,
		"Quit Game", m_hInstance, 315, 260, 56, 30);

	hBmp = (HBITMAP)LoadImage(this->m_hInstance, "menu.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	HDC hdc = GetDC(hWnd);
	memDC = CreateCompatibleDC(hdc);
	ReleaseDC(hWnd, hdc);
	oldBmp = (HBITMAP)SelectObject(memDC, hBmp);
	
	Show(SW_SHOWNORMAL);
	Update();

	return this->DoModal();
}
/*----------------------------------------------------------------------------------------------*/
void CMenuWindow::OnCommand(WPARAM wParam, LPARAM lParam)
{
	this->EndModel(LOWORD(wParam));
}
/*----------------------------------------------------------------------------------------------*/
void CMenuWindow::OnDrawItem(LPDRAWITEMSTRUCT lpdis)
{
	int x, y;

	switch(lpdis->CtlID)
	{
	case IDC_NEWGAME:
		{
			x = 0;
			if(this->m_pBtnNewGame->IsHover() == FALSE)
				y = 301;
			else
				y = 327;
			BitBlt(lpdis->hDC, 0, 0, lpdis->rcItem.right, lpdis->rcItem.bottom, memDC,
				x, y, SRCCOPY);
		}
		break;

	case IDC_LOADGAME:
		{
			x = 126;
			if(this->m_pBtnLoadGame->IsHover() == FALSE)
				y = 301;
			else
				y = 327;
			BitBlt(lpdis->hDC, 0, 0, lpdis->rcItem.right, lpdis->rcItem.bottom, memDC,
				x, y, SRCCOPY);
		}
		break;

	case IDC_SAVEGAME:
		{
			x = 257;
			if(this->m_pBtnSaveGame->IsHover() == FALSE)
				y = 301;
			else
				y = 327;
			BitBlt(lpdis->hDC, 0, 0, lpdis->rcItem.right, lpdis->rcItem.bottom, memDC,
				x, y, SRCCOPY);
		}
		break;

	case IDC_RESUME:
		{
			if(this->m_pBtnResume == NULL)
				break;

			y = 353;
			if(this->m_pBtnResume->IsHover() == FALSE)
				x = 114;
			else
				x = 209;
			BitBlt(lpdis->hDC, 0, 0, lpdis->rcItem.right, lpdis->rcItem.bottom, memDC,
				x, y, SRCCOPY);
		}
		break;

	case IDC_EXIT:
		{
			y = 353;
			if(this->m_pBtnQuit->IsHover() == FALSE)
				x = 0;
			else
				x = 57;
			BitBlt(lpdis->hDC, 0, 0, lpdis->rcItem.right, lpdis->rcItem.bottom, memDC,
				x, y, SRCCOPY);

		}
		break;
	}
}
/*----------------------------------------------------------------------------------------------*/
void CMenuWindow::OnPaint()
{
	PAINTSTRUCT ps;
	HDC hdc = BeginPaint(hWnd, &ps);
	SetMapMode(hdc, MM_TEXT);

	BitBlt(hdc, 0, 0, 400, 300, memDC, 0, 0, SRCCOPY);

	EndPaint(hWnd, &ps);
}
/*----------------------------------------------------------------------------------------------*/
void CMenuWindow::Register()
{
	WNDCLASS wndclass;
	wndclass.style		   = CS_HREDRAW | CS_VREDRAW;
	wndclass.lpfnWndProc   = ::WndProc;
	wndclass.cbClsExtra    = 0;
	wndclass.cbWndExtra    = sizeof(CMenuWindow *);
	wndclass.hInstance	   = this->m_hInstance;
	wndclass.hIcon         = LoadIcon(0, IDI_APPLICATION);
	wndclass.hCursor       = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
	wndclass.lpszMenuName   = NULL;
	wndclass.lpszClassName = szClassName;
	if(!RegisterClass(&wndclass))
		exit(FALSE);

	registered = true;
}
/*----------------------------------------------------------------------------------------------*/
long CMenuWindow::WndProc(UINT iMessage, WPARAM wParam, LPARAM lParam)
{
	switch(iMessage)
	{
		case WM_PAINT:
			this->OnPaint();
			break;

		case WM_DRAWITEM:
			this->OnDrawItem((LPDRAWITEMSTRUCT)lParam);
			break;

		//case WM_LBUTTONDOWN:
		//	this->EndModel(FALSE);
		//	break;

		case WM_COMMAND:
			this->OnCommand(wParam, lParam);
			break;

		case WM_DESTROY:
			break;

		default:
			return DefWindowProc(hWnd, iMessage, wParam, lParam);
	}
	return 0;
}
/*----------------------------------------------------------------------------------------------*/