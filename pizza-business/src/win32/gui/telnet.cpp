#include "telnet.h"
#include <stdio.h>
/*----------------------------------------------------------------------------------------------*/
#define ID_TIMER_CURSOR		1001
/*----------------------------------------------------------------------------------------------*/
bool CTelnet::bRegistered = false;
char CTelnet::szClassName[] = "TelnetWnd";
/*----------------------------------------------------------------------------------------------*/
CTelnet::CTelnet(HWND hParent, DWORD dwExStyle, DWORD dwStyle, int id, int x, int y,
				 int width, int height, HINSTANCE hInstance) :
m_curIndex(1), cursor_x(5), cursor_y(5), m_bCurShow(true), bInputCatch(false)
{
	this->Register(hInstance);

	hWnd = ::CreateWindowEx(dwExStyle, szClassName, szClassName,
		dwStyle,
		x, y, width, height,
		hParent, (HMENU)id, hInstance, (LPSTR)this);

	if(!hWnd)
		DWORD error_code = GetLastError();

	this->m_TextList.SetDeleteFlag(false);

	// create the font
	HDC hdc = GetDC(hWnd);
	int nHeight = -MulDiv(12, GetDeviceCaps(hdc, LOGPIXELSY), 72);
	hFont = CreateFont(16, 0,0,0,0,0,0,0,0,0,0,0,0, "Courier");

	oldFont = (HFONT)SelectObject(hdc, hFont);
	// calculate the height of the text
	SIZE size;
	GetTextExtentPoint(hdc, "A", 1, &size);
	text_height = size.cy + 2;

	ReleaseDC(hWnd, hdc);

	// set the timer for the blinking cursor
	SetTimer(hWnd, ID_TIMER_CURSOR, 500, NULL);

	// create the first line
	this->Telnet_NewLine();
}
/*----------------------------------------------------------------------------------------------*/
CTelnet::~CTelnet()
{
	KillTimer(hWnd, ID_TIMER_CURSOR);

	HDC hdc = GetDC(hWnd);
	SelectObject(hdc, oldFont);
	ReleaseDC(hWnd, hdc);

	DeleteObject(hFont);
	DestroyWindow(hWnd);
}
/*----------------------------------------------------------------------------------------------*/
void CTelnet::Register(HINSTANCE hInstance)
{
	if(bRegistered == true)
		return;

	WNDCLASS wndclass;
	wndclass.style		   = CS_HREDRAW|CS_VREDRAW;
	wndclass.lpfnWndProc   = ::WndProc;
	wndclass.cbClsExtra    = 0;
	wndclass.cbWndExtra    = sizeof(CTelnet *);
	wndclass.hInstance	   = hInstance;
	wndclass.hIcon         = LoadIcon(0, IDI_APPLICATION);
	wndclass.hCursor       = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
	wndclass.lpszMenuName  = NULL;
	wndclass.lpszClassName = szClassName;
	if(!RegisterClass(&wndclass))
		exit(FALSE);

	bRegistered = true;
}
/*----------------------------------------------------------------------------------------------*/
void CTelnet::OnChar(char character)
{
	char* text = this->m_TextList.GetItem(this->m_curIndex);

	if(character == 0x0D) // carriage return
	{
		this->Telnet_NewLine();
		InvalidateRect(hWnd, NULL, FALSE);
		return;
	}
	else if(character == 0x08) // backspace
	{
		short backspace_limit = 0;
		
		if(this->bInputCatch == true)
			backspace_limit = input_index;
		
		if(strlen(text) > backspace_limit)
			text[strlen(text)-1] = '\0';
	}
	else if(character == 0x0A) // linefeed
	{

	}
	else if(character == 0x1B) // escape
	{

	}
	else if(character == 0x09) // tab
	{

	}
	else
	{
		// be sure the length is not too long
		if(strlen(text) < 255)
		{
			char string[2];
			sprintf(string, "%c", character);
			strcat(text, string);
		}
	}

	// redraw the current line of text
	RECT rcDraw, rcClient;
	GetClientRect(hWnd, &rcClient);
	SetRect(&rcDraw, rcClient.left, this->cursor_y,
		rcClient.right, this->cursor_y+this->text_height + 2);
	InvalidateRect(hWnd, &rcDraw, FALSE);
}
/*----------------------------------------------------------------------------------------------*/
void CTelnet::OnPaint()
{
	PAINTSTRUCT ps;
	HDC hdc = BeginPaint(hWnd, &ps);
	
	SetMapMode(hdc, MM_TEXT);
	SetBkMode(hdc, TRANSPARENT);

	RECT rcClient;
	GetClientRect(hWnd, &rcClient);

	// color the background in a black color
	HBRUSH hBrBk = CreateSolidBrush(RGB(0,0,0));
	FillRect(hdc, &rcClient, hBrBk);
	DeleteObject(hBrBk);

	// draw the text to the screen in a green color
	// and set the font
	COLORREF org_color = SetTextColor(hdc, RGB(0,236,80));
	oldFont = (HFONT)SelectObject(hdc, hFont);

	int x_pos = 5, y_pos = 5;
	for(unsigned int c = 1 ; c <= this->m_TextList.GetCount() ; c++)
	{
		char* text = this->m_TextList.GetItem(c);

		// calculate the cursor x, y-position
		SIZE size;
		GetTextExtentPoint(hdc, text, strlen(text), &size);
		cursor_x = 5 + size.cx;

		y_pos = 5 + (c - 1) * text_height;
		TextOut(hdc, x_pos, y_pos, text, strlen(text));
	}

	// restore the old text color
	SetTextColor(hdc, org_color);
	SelectObject(hdc, oldFont);

	// draw the cursor with a vertical green line
	if(this->m_bCurShow == true)
	{
		HPEN hCursorPen = CreatePen(0, 0, RGB(0,255,0));
		HPEN oldPen = (HPEN)SelectObject(hdc, hCursorPen);
		MoveToEx(hdc, cursor_x, cursor_y, NULL);
		LineTo(hdc, cursor_x, cursor_y+text_height);
		SelectObject(hdc, oldPen);
		DeleteObject(hCursorPen);
	}

	EndPaint(hWnd, &ps);
}
/*----------------------------------------------------------------------------------------------*/
void CTelnet::OnTimer(UINT wTimerID)
{
	switch(wTimerID)
	{
		case ID_TIMER_CURSOR:
			{
				m_bCurShow = (m_bCurShow? false : true);

				// redraw the area around the cursor
				RECT rcDraw;
				SetRect(&rcDraw, this->cursor_x, this->cursor_y,
					this->cursor_x+2, this->cursor_y+this->text_height);
				InvalidateRect(hWnd, &rcDraw, 0);
			}
			break;
	}
}
/*----------------------------------------------------------------------------------------------*/
void CTelnet::Telnet_Clear()
{
	this->m_TextList.DeleteAllItems();

	this->Telnet_NewLine();
	InvalidateRect(hWnd, NULL, FALSE);
}
/*----------------------------------------------------------------------------------------------*/
void CTelnet::Telnet_NewLine()
{
	if(this->bInputCatch == true)
		this->bInputCatch = false;

	if(this->m_TextList.GetCount() >= 25)
	{
		// delete the first one
		char *text = this->m_TextList.GetItem(1);
		this->m_TextList.DeleteItem(1);
		delete [] text;
	}

	// create a copy of the string
	// and add it to the end of the list
	char *new_text = new char[256];
	strcpy(new_text, "");
	m_curIndex = this->m_TextList.AddItem(new_text);

	// reset the cursor position
	cursor_x = 5;
	cursor_y = 5 + (this->m_curIndex-1) * text_height;

	InvalidateRect(hWnd, NULL, FALSE);
}
/*----------------------------------------------------------------------------------------------*/
void CTelnet::Telnet_Print(char* text)
{
	char* line_text = this->m_TextList.GetItem(this->m_curIndex);
	strcat(line_text, text);

	InvalidateRect(hWnd, NULL, FALSE);
}
/*----------------------------------------------------------------------------------------------*/
char* CTelnet::Telnet_Prompt(char* prompt)
{
	this->Telnet_Print(prompt);

	char *text = this->m_TextList.GetItem(this->m_curIndex);
	input_index = strlen(text);

	MSG msg;
	for(this->bInputCatch = true ; this->bInputCatch ; WaitMessage())
	{
		while(PeekMessage(&msg, 0, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return this->input_text = &text[input_index];
}
/*----------------------------------------------------------------------------------------------*/
long CTelnet::WndProc(UINT iMessage, WPARAM wParam, LPARAM lParam)
{
	switch(iMessage)
	{
		case WM_PAINT:
			this->OnPaint();
			break;

		case WM_CHAR:
			this->OnChar((TCHAR)wParam);
			break;

		case WM_TIMER:
			this->OnTimer((UINT)wParam);
			break;

		case WM_CLOSE:
			if(this->bInputCatch == false)
				DestroyWindow(hWnd);
			break;

		case WM_DESTROY:
			break;

		default:
			return DefWindowProc(hWnd, iMessage, wParam, lParam);
	}
	return 0;
}
/*----------------------------------------------------------------------------------------------*/