#include <stdio.h>
#include "menu.h"
/*----------------------------------------------------------------------------------------------*/
bool CMenu::registered = false;
/*----------------------------------------------------------------------------------------------*/
CMenu::CMenu(HWND hParent, HINSTANCE hInstance)
{
	if(this->registered == false)
		this->Register(hInstance);

	hWnd = CreateWindowEx(0, "Menu_JS", "Menu_JS",
		WS_BORDER|WS_POPUP,
		10, 10, 200, 50,
		0, (HMENU)NULL, hInstance, (LPSTR)this);

	menu_count = 0;
	caption_height = 14;
	item_height = 22;
	text_offset = 4;
}
/*----------------------------------------------------------------------------------------------*/
CMenu::~CMenu()
{
	DestroyWindow(hWnd);
}
/*----------------------------------------------------------------------------------------------*/
void CMenu::GetItemRect(int index, RECT* rc)
{
	GetClientRect(hWnd, rc);
	rc->top = caption_height + (index * item_height);
	rc->bottom = rc->top + item_height;
}
/*----------------------------------------------------------------------------------------------*/
void CMenu::Paint()
{
	RECT rc;
	GetClientRect(hWnd, &rc);

  	PAINTSTRUCT ps;
  	HDC hdc = BeginPaint(hWnd, &ps);
	HDC memDC = CreateCompatibleDC(hdc);
	HBITMAP hBmp = CreateCompatibleBitmap(hdc, rc.right+5, rc.bottom+5);
	HBITMAP originalBmp = (HBITMAP)SelectObject(memDC, hBmp);

  	SetMapMode(memDC, MM_TEXT);
	SetBkMode(memDC, TRANSPARENT);

	HBRUSH hbrSelection = CreateSolidBrush(RGB(0,64,128));
	HBRUSH hbrBackground = CreateSolidBrush(RGB(255,255,255));

	int fontSize = -MulDiv(4, GetDeviceCaps(memDC, LOGPIXELSY), 72);
	HFONT hCaptionFont = CreateFont(fontSize, 0, 0, 0, FW_BOLD,
		0, 0, 0, 0, 0, 0, PROOF_QUALITY, 0, "Helv");

	fontSize = -MulDiv(10, GetDeviceCaps(memDC, LOGPIXELSY), 72);
	HFONT hTextFont = CreateFont(fontSize, 0, 0, 0, FW_BOLD,
		0, 0, 0, 0, 0, 0, PROOF_QUALITY, 0, "Helv");

	// draw caption background
	rc.bottom = rc.top + caption_height;
	FillRect(memDC, &rc, hbrSelection);
	// draw caption text
	HFONT oldFont = (HFONT)SelectObject(memDC, hCaptionFont);
	SetTextColor(memDC, RGB(255,255,255));
	TextOut(memDC, text_offset, 0, caption, strlen(caption));

	// draw the commands (options)
	SelectObject(memDC, hTextFont);
	SetTextColor(memDC, RGB(0,64,128));
	for(int c = 0 ; c < menu_count ; c++)
	{
		this->GetItemRect(c, &rc);
		
		if(c == index_sel)
		{
			SetTextColor(memDC, RGB(255,255,255));
			FillRect(memDC, &rc, hbrSelection);
			rc.left += text_offset;
			DrawText(memDC, menu_items[c].text, strlen(menu_items[c].text), &rc, DT_VCENTER|DT_SINGLELINE);
			SetTextColor(memDC, RGB(0,64,128));
		}
		else
		{
			FillRect(memDC, &rc, hbrBackground);
			rc.left += text_offset;
			DrawText(memDC, menu_items[c].text, strlen(menu_items[c].text), &rc, DT_VCENTER|DT_SINGLELINE);
		}
		rc.left -= text_offset;
		// top
		MoveToEx(memDC, rc.left, rc.top, NULL);
		LineTo(memDC, rc.right, rc.top);
		// bottom
		MoveToEx(memDC, rc.left, rc.bottom, NULL);
		LineTo(memDC, rc.right, rc.bottom);
	}
	SelectObject(memDC, oldFont);
	// delete gdi objects
	DeleteObject(hbrSelection);
	DeleteObject(hbrBackground);
	DeleteObject(hCaptionFont);
	DeleteObject(hTextFont);
	
	GetClientRect(hWnd, &rc);
	BitBlt(hdc, 0, 0, rc.right, rc.bottom, memDC, 0, 0, SRCCOPY);
	SelectObject(memDC, originalBmp);
	DeleteObject(hBmp);
	DeleteDC(memDC);
  	EndPaint(hWnd, &ps);
}
/*----------------------------------------------------------------------------------------------*/
void CMenu::Register(HINSTANCE hInstance)
{
		WNDCLASS wndclass;
		wndclass.style	   	   = CS_HREDRAW | CS_VREDRAW;
		wndclass.lpfnWndProc   = ::WndProc;
		wndclass.cbClsExtra    = 0;
		wndclass.cbWndExtra    = sizeof(CMenu *);
		wndclass.hInstance	   = hInstance;
		wndclass.hIcon         = LoadIcon(0, IDI_APPLICATION);
		wndclass.hCursor       = LoadCursor(NULL, IDC_ARROW);
		wndclass.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
		wndclass.lpszMenuName  = NULL;
		wndclass.lpszClassName = "Menu_JS";
		if(!RegisterClass(&wndclass))
			exit(FALSE);
	}
/*----------------------------------------------------------------------------------------------*/
void CMenu::ShowMenu(int x, int y, bool bLeft)
{
	int width = 0;
	SIZE size;
	HDC hdc = GetDC(hWnd);

	int fontSize = -MulDiv(10, GetDeviceCaps(hdc, LOGPIXELSY), 72);
	HFONT hTextFont = CreateFont(fontSize, 0, 0, 0, FW_BOLD,
		0, 0, 0, 0, 0, 0, PROOF_QUALITY, 0, "Helv");
	HFONT oldFont = (HFONT)SelectObject(hdc, hTextFont);

	for(int c = 0 ; c < menu_count ; c++)
	{
		GetTextExtentPoint32(hdc, menu_items[c].text, strlen(menu_items[c].text), &size);
		if(size.cx+(text_offset*2) > width)
			width = size.cx+(text_offset*2);
	}

	SelectObject(hdc, oldFont);
	DeleteObject(hTextFont);
	ReleaseDC(hWnd, hdc);

	int height = caption_height + (menu_count * item_height) + 1;
	SetWindowPos(hWnd, NULL, (bLeft?x:x-width), y, width, height, SWP_NOZORDER);
	ShowWindow(hWnd, SW_SHOW);
	SetCapture(hWnd);
}
/*----------------------------------------------------------------------------------------------*/
long CMenu::WndProc(UINT iMessage, WPARAM wParam, LPARAM lParam)
{
	switch(iMessage)
	{
		case WM_ACTIVATE:
			if(LOWORD(wParam) == WA_INACTIVE)
			{
				ShowWindow(hWnd, SW_HIDE);
				ReleaseCapture();
			}
			break;

		case WM_MOUSEMOVE:
			{
				int xPos = LOWORD(lParam);
				int yPos = HIWORD(lParam);
				POINT pt;
				pt.x = xPos;
				pt.y = yPos;
				RECT rc;
				for(int c = 0 ; c < menu_count ; c++)
				{
					this->GetItemRect(c, &rc);
					if(PtInRect(&rc, pt) == TRUE)
					{
						if(index_sel != c)
						{
							index_sel = c;
							InvalidateRect(hWnd, 0, FALSE);
						}
						break;
					}
					else
					{
						if(index_sel != -1)
						{
							index_sel = -1;
							InvalidateRect(hWnd, 0, FALSE);
						}
						else
							index_sel = -1;
					}
				}
			}
			break;

		case WM_LBUTTONDOWN:
			{
				int xPos = LOWORD(lParam);
				int yPos = HIWORD(lParam);

				POINT pt;
				pt.x = xPos;
				pt.y = yPos;
				RECT rc;
				for(int c = 0 ; c < menu_count ; c++)
				{
					this->GetItemRect(c, &rc);
					if(PtInRect(&rc, pt) == TRUE)
					{
						ShowWindow(hWnd, SW_HIDE);
						SendMessage(GetParent(hWnd), WM_COMMAND, MAKEWPARAM(menu_items[c].command, 0), NULL);
						return TRUE;
					}
				}
				ShowWindow(hWnd, SW_HIDE);
			}
			break;

		case WM_RBUTTONDOWN:
			ShowWindow(hWnd, SW_HIDE);
			break;

		case WM_PAINT:
			Paint();
			break;

		case WM_DESTROY:
			break;

		default:
			return DefWindowProc(hWnd, iMessage, wParam, lParam);
	}
	return 0;
}
/*----------------------------------------------------------------------------------------------*/