#ifndef _JS_WND_BUY_H
#define _JS_WND_BUY_H
/*----------------------------------------------------------------------------------------------*/
#include "window.h"
#include "..\..\datarest.h"
/*----------------------------------------------------------------------------------------------*/
class CBuySellDlg : public CDialog
{
public:
	long WndProc(UINT iMessage, WPARAM wParam, LPARAM lParam);

public:
	void SetAdsList(List<CAdCategory> &list) { m_pAdsList = &list; }
	void SetChairPrice(float price) { m_chair_price = price; }
	void SetIngList(List<CIngCategory> &list) { m_pIngList = &list; }
	void SetOvenList(List<COven> &list) { m_pOvenList = &list; }
	void SetTableList(List<CTable> &list) { m_pTableList = &list; }
	void SetRestaurant(CRestaurant *pRestaurant) { m_pRestaurant = pRestaurant; }

private:
	void OnCommand(WPARAM wParam, LPARAM lParam);
	void OnInitDialog();
	void OnNotify(LPNMHDR lpHdr);

private:
	LONG GetSelItemData();
	short GetTabIndex();
	void RefreshCaption();
	void RefreshListBox();

	// called internally from OnCommand()
	void OnCommand_Sell();

	// called internally from RefreshListBox()
	void LB_DumpAdvertisements();
	void LB_DumpChairs();
	void LB_DumpIngredients();
	void LB_DumpOvens();
	void LB_DumpTables();

private:
	CRestaurant *m_pRestaurant;
	float m_chair_price;
	List<CTable> *m_pTableList;
	List<COven> *m_pOvenList;
	List<CAdCategory> *m_pAdsList;
	List<CIngCategory> *m_pIngList;
};
/*----------------------------------------------------------------------------------------------*/
class CCatalogDlg : public CDialog
{
public:
	void SetRestaurant(CRestaurant *pRestaurant) { m_pRestaurant = pRestaurant; }

public:
	long WndProc(UINT iMessage, WPARAM wParam, LPARAM lParam);

protected:
	LPARAM GetSelItem();
	unsigned int GetQuantity();
	virtual void OnInitListBox() {}
	virtual void OnBuySelected() {}

protected:
	CRestaurant *m_pRestaurant;

private:
	void OnCommand(WPARAM wParam, LPARAM lParam);
	void OnInitDialog();
};
/*----------------------------------------------------------------------------------------------*/
class CAdsCatDlg : public CCatalogDlg
{
public:
	CAdsCatDlg(List<CAdCategory> &list) {
		m_pAdsList = &list;
	}

protected:
	void OnInitListBox();
	void OnBuySelected();

private:
	List<CAdCategory> *m_pAdsList;
};
/*----------------------------------------------------------------------------------------------*/
class CChairCatDlg : public CCatalogDlg
{
public:
	CChairCatDlg(float chair_price) {
		m_chair_price = chair_price;
	}

protected:
	void OnInitListBox();
	void OnBuySelected();

private:
	float m_chair_price;
};
/*----------------------------------------------------------------------------------------------*/
class CIngCatDlg : public CCatalogDlg
{
public:
	CIngCatDlg(List<CIngCategory> &list) {
		m_pIngList = &list;
	}

protected:
	void OnInitListBox();
	void OnBuySelected();

private:
	List<CIngCategory> *m_pIngList;
};
/*----------------------------------------------------------------------------------------------*/
class COvenCatDlg : public CCatalogDlg
{
public:
	COvenCatDlg(List<COven> &list) {
		m_pOvenList = &list;
	}

protected:
	void OnInitListBox();
	void OnBuySelected();

private:
	List<COven> *m_pOvenList;
};
/*----------------------------------------------------------------------------------------------*/
class CTableCatDlg : public CCatalogDlg
{
public:
	CTableCatDlg(List<CTable> &list) {
		m_pTableList = &list;
	}

protected:
	void OnInitListBox();
	void OnBuySelected();

private:
	List<CTable> *m_pTableList;
};
/*----------------------------------------------------------------------------------------------*/
class CSellEditDlg : public CDialog
{
public:
	CSellEditDlg() : m_Number(0) {
		strcpy(m_Desc, "");
	}

	void SetDescription(char* desc);

	void SetNumber(int number) { m_Number = number; }
	int GetNumber() { return m_Number; }

public:
	long WndProc(UINT iMessage, WPARAM wParam, LPARAM lParam);

private:
	void OnCommand(WPARAM wParam, LPARAM lParam);
	void OnInitDialog();

private:
	char m_Desc[256];
	int m_Number;
};
/*----------------------------------------------------------------------------------------------*/
class CSellListDlg : public CDialog
{
public:
	long WndProc(UINT iMessage, WPARAM wParam, LPARAM lParam);

private:
	void OnCommand(WPARAM wParam, LPARAM lParam);
	void OnInitDialog();
};
/*----------------------------------------------------------------------------------------------*/
#endif