#include "wnd_bank.h"
/*----------------------------------------------------------------------------------------------*/
#define IDC_BANK_LIST		101
#define IDC_BANK_CONNECT	102
#define IDC_BANK_DEBT		103
/*----------------------------------------------------------------------------------------------*/
CBankDlg::CBankDlg(CRestaurant *pRestaurant)
{
	this->m_pRestaurant = pRestaurant;
}
/*----------------------------------------------------------------------------------------------*/
void CBankDlg::InitBankServices(CTelnet &telnet)
{
	bool bFirstTime = true;
	bool bLogOut = false;
	do
	{
		telnet.Telnet_Clear();
		this->PrintWelcomeText(telnet, bFirstTime);
		telnet.Telnet_NewLine();

		telnet.Telnet_Print("State your business:");
		telnet.Telnet_NewLine();
		telnet.Telnet_Print("1. Apply for loan");
		telnet.Telnet_NewLine();
		telnet.Telnet_Print("2. Pay off loans");
		telnet.Telnet_NewLine();
		telnet.Telnet_Print("3. Log out");
		telnet.Telnet_NewLine();

		char* input = telnet.Telnet_Prompt("> ");
		telnet.Telnet_NewLine();

		switch(input[0])
		{
			case '1':
				this->InitLoanProcess(telnet);
				bFirstTime = false;
				break;

			case '2':
				this->InitPayLoanProcess(telnet);
				bFirstTime = false;
				break;

			case '3':
				bLogOut = true;
				break;
		}
	} while(bLogOut == false);
}
/*----------------------------------------------------------------------------------------------*/
void CBankDlg::InitLoanProcess(CTelnet &telnet)
{
	// print the number of loans for the restaurant
	char loan_count[256];
	sprintf(loan_count, "The restaurant currently has %d outstanding loans.", 
		this->m_pRestaurant->m_pBankAccount->m_LoanList.GetCount());
	telnet.Telnet_Print(loan_count);
	telnet.Telnet_NewLine();

	// TODO: print debt of the restaurant

	char *input = telnet.Telnet_Prompt("Enter the amount: $");
	
	double amount = atof(input);
	if(amount > 0)
		this->m_pRestaurant->m_pBankAccount->CreateLoan(amount, 0.03f); // 3% interest rate
}
/*----------------------------------------------------------------------------------------------*/
void CBankDlg::InitPayLoanProcess(CTelnet &telnet)
{
	CBankAccount *pAccount = this->m_pRestaurant->m_pBankAccount;
	// print the number of loans for the restaurant
	// and the amount of the restaurant's debt
	char text[256];
	sprintf(text, "You have a total of %d loans at a debt of $%.2f.",
		pAccount->GetActiveLoanCount(), pAccount->GetAccountBalance());

	telnet.Telnet_Print(text);
	telnet.Telnet_NewLine();

	// print the amount of funds on hand for the restaurant
	sprintf(text, "The restaurant currently has $%.2f.", this->m_pRestaurant->GetBudget());

	telnet.Telnet_Print(text);
	telnet.Telnet_NewLine();

	telnet.Telnet_Print("Enter the amount to pay:");
	telnet.Telnet_NewLine();
	char* input = telnet.Telnet_Prompt("> ");
	telnet.Telnet_NewLine();

	double amount = atof(input);
	if(amount < 1)
		return;
	
	if(amount <= this->m_pRestaurant->GetBudget())
		this->m_pRestaurant->m_pBankAccount->PayOffLoans(amount);
}
/*----------------------------------------------------------------------------------------------*/
void CBankDlg::OnInitDialog()
{
	this->RefreshLoanBox();
}
/*----------------------------------------------------------------------------------------------*/
void CBankDlg::OnCommand(WPARAM wParam, LPARAM lParam)
{
	switch(LOWORD(wParam))
	{
	case 1:
	case 2:
		EndDialog(hWnd, LOWORD(wParam));
		break;

	case IDC_BANK_CONNECT:
		{
			EnableWindow(hWnd, FALSE);

			short t_width = 500;
			short t_height = 400;
	
			RECT rcDesktop;
			GetWindowRect(GetDesktopWindow(), &rcDesktop);

			CTelnet telnet(hWnd, NULL, WS_OVERLAPPEDWINDOW|WS_VISIBLE, NULL,
				rcDesktop.right/2-t_width/2, rcDesktop.bottom/2-t_height/2, 
				t_width, t_height, this->m_hInstance);

			this->InitBankServices(telnet);

			EnableWindow(hWnd, TRUE);

			this->RefreshLoanBox();
		}
		break;
	}
}
/*----------------------------------------------------------------------------------------------*/
void CBankDlg::PrintWelcomeText(CTelnet &telnet, bool bConnect)
{
	// Tony, you can customize the stuff in this function
	if(bConnect == true)
	{
		telnet.Telnet_Print("Dialing 1.900.123.4567...");
		telnet.Telnet_NewLine();
		telnet.Telnet_Print("Connected.");
		telnet.Telnet_NewLine();
		telnet.Telnet_NewLine();
	}

	telnet.Telnet_Print("Welcome to Johnny's Bank International.");
	telnet.Telnet_NewLine();

	char text[40];
	sprintf(text, "Logged into account #: %d", this->m_pRestaurant->GetId());
	telnet.Telnet_Print(text);
	telnet.Telnet_NewLine();
}
/*----------------------------------------------------------------------------------------------*/
void CBankDlg::RefreshLoanBox()
{
	HWND hwndLB = GetDlgItem(this->hWnd, 101);

	SendMessage(hwndLB, LB_RESETCONTENT, NULL, NULL);

	// add an entry to the listbox for each loan
	CBankAccount *pAccount = this->m_pRestaurant->m_pBankAccount;
	for(unsigned int c = 1 ; c <= pAccount->m_LoanList.GetCount() ; c++)
	{
		CBankLoan *pLoan = pAccount->m_LoanList.GetItem(c);
	
		char text[256];
		sprintf(text, "Loan #%d, Amount: $%.2f, Balance: $%.2f (%s)", c, pLoan->GetInitialAmount(),
			pLoan->GetBalance(), (pLoan->IsInEffect()?"Active":"InActive"));

		int index = SendMessage(hwndLB, LB_ADDSTRING, NULL, (LPARAM)(LPCSTR)text);
		SendMessage(hwndLB, LB_SETITEMDATA, index, NULL);
	}

	// update the total debt label too
	char text[256];
	sprintf(text, "Total debt: $%.2f", pAccount->GetAccountBalance());
	SetWindowText(GetDlgItem(hWnd, IDC_BANK_DEBT), text);
}
/*----------------------------------------------------------------------------------------------*/
long CBankDlg::WndProc(UINT iMessage, WPARAM wParam, LPARAM lParam)
{
	switch(iMessage)
	{
		case WM_INITDIALOG:
			this->OnInitDialog();
			break;

		case WM_COMMAND:
			this->OnCommand(wParam, lParam);
			break;
	}
	return 0;
}
/*----------------------------------------------------------------------------------------------*/
