#include "winiface.h"
/*----------------------------------------------------------------------------------------------*/
CWinInterface::CWinInterface(HINSTANCE hInstance) :
m_pMainWindow(0)
{
	this->m_hInstance = hInstance;
}
/*----------------------------------------------------------------------------------------------*/
CWinInterface::~CWinInterface()
{
	if(m_pMainWindow)
		delete m_pMainWindow;
}
/*----------------------------------------------------------------------------------------------*/
int CWinInterface::Run()
{
	// create the main window; set its application pointer
	this->m_pMainWindow = new CMainWindow(this->m_hInstance, this->GetApplication());
	//this->m_pMainWindow->SetApplication(this->GetApplication());

	// start the application message loop
	MSG msg;
	while(GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
	return msg.wParam;
}
/*----------------------------------------------------------------------------------------------*/