#ifndef _JS_WINIFACE_H
#define _JS_WINIFACE_H
/*----------------------------------------------------------------------------------------------*/
#include "..\gfxiface.h"
#include "gui\mainwnd.h"
/*----------------------------------------------------------------------------------------------*/
class CWinInterface : public CGfxInterface
{
public:
	CWinInterface(HINSTANCE hInstance);
	~CWinInterface();

public:
	int Run();

private:
	HINSTANCE m_hInstance;
	CMainWindow *m_pMainWindow;
};
/*----------------------------------------------------------------------------------------------*/
#endif