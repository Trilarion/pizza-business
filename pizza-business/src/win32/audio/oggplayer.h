#ifndef _JS_OGGPLAYER_H
#define _JS_OGGPLAYER_H
/*----------------------------------------------------------------------------------------------*/
#include <windows.h>
#include <mmsystem.h>
#include <stdio.h>

#include <vorbis/codec.h>
#include <vorbis/vorbisfile.h>
/*----------------------------------------------------------------------------------------------*/
enum status_player { status_nofile, status_playing, status_stopped };
/*----------------------------------------------------------------------------------------------*/
static CRITICAL_SECTION waveCriticalSection;
WAVEHDR* allocateBlocks(int size, int count);
void freeBlocks(WAVEHDR* blockArray);
void CALLBACK waveOutProc(HWAVEOUT hWaveOut, UINT uMsg, DWORD dwInstance, DWORD dwParam1, DWORD dwParam2);

DWORD WINAPI OggPlayerThreadProc(LPVOID lpParameter);
/*----------------------------------------------------------------------------------------------*/
class COggPlayer
{
public: // construction and destruction
	COggPlayer();
	~COggPlayer();

public:
	DWORD ThreadProc();
	void OnNotify(UINT iMessage);

public: 
	// file operation functions
	bool OpenFile(char* filename);
	void CloseFile();

	// standard player functions
	bool Pause();
	bool Play(bool bLoop = false);
	bool Stop();

private:
	bool IsFileOpen();
	bool IsLooping() { return m_bLoop; }
	bool IsPlaying();

	void SetFilename(char *filename) { strcpy(m_Filename, filename); }
	void SetPlayerStatus(status_player status) { m_Status = status; }

private: // internal OggPlayer stuff
	// handles
	HWAVEOUT waveout;

	WAVEHDR* waveBlocks;
	int waveFreeBlockCount;
	int waveCurrentBlock;

	// functions
	void writeAudio(HWAVEOUT hWaveOut, LPSTR data, int size);

private:
	FILE *m_File;
	char m_Filename[MAX_PATH];

	status_player m_Status;

	HANDLE m_hThread;
	DWORD m_ThreadId;

	bool m_bStop;
	bool m_bLoop;
};
/*----------------------------------------------------------------------------------------------*/
#endif