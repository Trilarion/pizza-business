#include "table.h"
/*-----------------------------------------------------------------------------------------------+
|																								 |
|	Recordset implementation																	 |
|																								 |
+-----------------------------------------------------------------------------------------------*/
void Recordset::AddField(Field *pField, short iField)
{
	// add a recorditem to each recordset's cell list
	for(unsigned int c = 1 ; c <= this->m_Records.GetCount() ; c++)
	{
		Record *pRecord = this->m_Records.GetItem(c);
		if(pRecord)
		{
			RecordItem *pItem = new RecordItem(pField, "");
			pRecord->m_Cells.InsertItem(pItem, iField);
		}
	}
}
/*----------------------------------------------------------------------------------------------*/
void Recordset::DeleteField(short iField)
{
	for(unsigned int c = 1 ; c <= this->m_Records.GetCount() ; c++)
	{
		Record *pRecord = this->m_Records.GetItem(c);
		pRecord->m_Cells.DeleteItem(iField);
	}
}
/*----------------------------------------------------------------------------------------------*/
short Recordset::AddRecord(short iRow)
{
	Record *pRecord = new Record;
	int index = this->m_Records.InsertItem(pRecord, iRow);

	// add a cell for each field
	for(int c = 1 ; c <= this->m_pTable->GetFieldCount() ; c++)
	{
		Field *pField = this->m_pTable->GetField(c);
		
		RecordItem *pItem = new RecordItem(pField, "");
		pRecord->m_Cells.AddItem(pItem);
	}
	return index;
}
/*----------------------------------------------------------------------------------------------*/
void Recordset::SetText(short iRow, short iField, char* text)
{
	Record *pRecord = this->m_Records.GetItem(iRow);
	if(pRecord)
	{
		RecordItem *pItem = pRecord->m_Cells.GetItem(iField);
		if(pItem)
			pItem->SetText(text);
	}
}
/*-----------------------------------------------------------------------------------------------+
|																								 |
|	Table implementation																		 |
|																								 |
+-----------------------------------------------------------------------------------------------*/
void Table::ClearTable()
{
	this->m_Fields.DeleteAllItems();
	this->m_Recordset.ClearRecords();
}
/*----------------------------------------------------------------------------------------------*/
Field* Table::CreateField(char* text, short width, long flags, short iField)
{
	// create field object and add it to the list
	Field *pField = new Field(this, text, width, flags);
	m_Fields.InsertItem(pField, iField);
	
	this->m_Recordset.AddField(pField, iField);
	return pField;
}
/*----------------------------------------------------------------------------------------------*/
void Table::DeleteField(short iField)
{
	this->m_Fields.DeleteItem(iField);
	this->m_Recordset.DeleteField(iField);
}
/*----------------------------------------------------------------------------------------------*/
