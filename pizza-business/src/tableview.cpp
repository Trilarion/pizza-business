#include <iostream>
#include <iomanip>
#include "tableview.h"
#include "conutils.h"
using namespace std;
/*-----------------------------------------------------------------------------------------------+
|																								 |
|	CTableView implementation																	 |
|																								 |
+-----------------------------------------------------------------------------------------------*/
void CTableView::DrawHorzLine(Table &table)
{
	short c = 0;
	short wReport = 0;
	// calculate the width of the table
	for(c = 1 ; c <= table.GetFieldCount() ; c++)
	{
		Field *pField = table.GetField(c);
		wReport += pField->GetWidth();
	}

	// draw a horz line
	for(c = 0 ; c < wReport ; c++)
		cout << "-";
	cout << endl;
}
/*----------------------------------------------------------------------------------------------*/
void CTableView::DrawRecord(Record *pRecord)
{
	if(!pRecord) return;

	for(short c = 1 ; c <= pRecord->m_Cells.GetCount() ; c++)
	{
		RecordItem *pItem = pRecord->m_Cells.GetItem(c);

		char text[256];
		pItem->GetText(text);

		Field *pField = pItem->GetField();
		cout << resetiosflags(ios::left|ios::right);
		cout << setw(pField->GetWidth()) << setiosflags(pField->GetFlags()) 
			 << text;
	}
	cout << endl;
}
/*----------------------------------------------------------------------------------------------*/
void CTableView::DisplayTable(Table &table)
{
	DrawTableHeader(table);

	for(short c = 1 ; c <= table.m_Recordset.GetRecordCount() ; c++)
	{
		Record *pRecord = table.m_Recordset.GetRecord(c);
		DrawRecord(pRecord);
	}

	this->DrawHorzLine(table);
}
/*----------------------------------------------------------------------------------------------*/
void CTableView::DrawTableHeader(Table &table)
{
	this->DrawHorzLine(table);
	// draw the header (columns)
	for(short c = 1 ; c <= table.GetFieldCount() ; c++)
	{
		Field *pField = table.GetField(c);

		char text[256];
		pField->GetText(text);

		cout << resetiosflags(ios::left|ios::right);
		cout << setw(pField->GetWidth()) << setiosflags(pField->GetFlags())
			 << text;
	}
	cout << endl;

	this->DrawHorzLine(table);
}
/*----------------------------------------------------------------------------------------------*/
