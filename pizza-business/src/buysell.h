#ifndef _JS_BUYSELL_H
#define _JS_BUYSELL_H
/*----------------------------------------------------------------------------------------------*/
#include <cstring>
#include "list.h"
using namespace ListH;
/*----------------------------------------------------------------------------------------------*/
class CAdvertisement;
class CIngredient;
/*----------------------------------------------------------------------------------------------*/
class CAdCategory
{
public:
	void GetName(char* buffer) { strcpy(buffer, m_Name); }
	void SetName(char* name) { strcpy(m_Name, name); }

public:
	List<CAdvertisement> AdList;

private:
	char m_Name[256];
};
/*----------------------------------------------------------------------------------------------*/
class CAdvertisement
{
public:
	CAdvertisement()
	{
		SetDescription("");
		SetPrice(0.0f);
		SetId(0);
	}
	~CAdvertisement() {}

public:
	void GetDescription(char *buffer) { strcpy(buffer, m_Description); }
	void SetDescription(char *desc) { strcpy(m_Description, desc); }

	short GetFactor() { return m_Factor; }
	void SetFactor(short factor) { m_Factor = factor; }

	short GetId() { return m_Id; }
	void SetId(short id) { m_Id = id; }

	float GetPrice() { return m_Price; }
	void  SetPrice(float price) { m_Price = price; }

private:
	char m_Description[1024];
	float m_Price;
	short m_Factor;
	short m_Id;
};
/*----------------------------------------------------------------------------------------------*/
class CIngCategory
{
public:
	void GetName(char* buffer) { strcpy(buffer, m_Name); }
	void SetName(char* name) { strcpy(m_Name, name); }

public:
	List<CIngredient> IngList;

private:
	char m_Name[256];
};
/*----------------------------------------------------------------------------------------------*/
class CIngredient
{
public:
	CIngredient(CIngCategory *pIngCategory)
	{
		this->m_pCategory = pIngCategory;

		this->SetName("");
		this->SetDescription("");
		this->SetVenderID(0);
		this->SetId(0);
	}
	~CIngredient() {}

public:
	CIngCategory* GetCategory() { return m_pCategory; }

	void GetName(char* buffer) { strcpy(buffer, m_Name); }
	void SetName(char* name) { strcpy(m_Name, name); }

	void GetDescription(char* buffer) { strcpy(buffer, m_Description); }
	void SetDescription(char* desc) { strcpy(m_Description, desc); }

	short GetId() { return m_Id; }
	void SetId(short id) { m_Id = id; }

	float GetPrice() { return m_Price; }
	void SetPrice(float price) { m_Price = price; }

	short GetQuality() { return m_Quality; }
	void SetQuality(short quality) { m_Quality = quality; }

	short GetWeight() { return m_Weight; }
	void SetWeight(short weight) { m_Weight = weight; }

	short GetVenderID() { return m_VenderID; }
	void SetVenderID(short id) { m_VenderID = id; }

private:
	char m_Name[256];
	char m_Description[1024];
	float m_Price;
	short m_Quality;
	short m_Weight;
	short m_VenderID;
	short m_Id;

	CIngCategory *m_pCategory;
};
/*----------------------------------------------------------------------------------------------*/
struct AdItem {
	CAdvertisement *pAd;
	unsigned int quantity;
};
/*----------------------------------------------------------------------------------------------*/
// NOTE: the following structures are used for organizational purposes of the
// ingredients in the CRestaurant class
struct IngItem {
	short life_left;
};

struct Ingredient {
	CIngredient *pIngredient;
	List<IngItem> IngList;
};

struct IngCat {
	CIngCategory *pCategory;
	List<Ingredient> ingredients;
};
/*----------------------------------------------------------------------------------------------*/
#endif

