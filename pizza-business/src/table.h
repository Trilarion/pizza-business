#ifndef _JS_TABLE_H
#define _JS_TABLE_H
/*------------------------------------------------------------------------------------------------

	Author: Johnny Salazar
	Date: May 30, 2002
	Description:
		The objects included in this small library of table creation is designed similar
		but very different than Microsoft's DAO or ADO. It is still a work in progress, but
		it has worked well so far. All interfaces are meant to be hidden. I intend to make
		everything accessible through one Table object.

------------------------------------------------------------------------------------------------*/
#include <cstring>

#include "list.h"
using namespace ListH;
/*----------------------------------------------------------------------------------------------*/
class Field;
class Record;
class RecordItem;
class Recordset;
class Table;
/*----------------------------------------------------------------------------------------------*/
class Field
{
public:
	Field(Table *pTable, char* text, short width, long flags)
	{
		this->m_pTable = pTable;
		this->SetText(text);
		this->SetWidth(width);
		this->SetFlags(flags);
	}
	~Field() {}

public:
	long GetFlags() { return m_Flags; }
	void SetFlags(long flags) { m_Flags = flags; }

	void GetText(char* buffer) { strcpy(buffer, m_Text); }
	void SetText(char* text) { strcpy(m_Text, text); }

	short GetWidth() { return m_Width; }
	void SetWidth(short width) { m_Width = width; }

private:
	char m_Text[256];
	short m_Width;
	Table *m_pTable;
	long m_Flags;
};
/*----------------------------------------------------------------------------------------------*/
class Record
{
public:
	Record() {}
	~Record() {}

public:
	List<RecordItem> m_Cells;
};
/*----------------------------------------------------------------------------------------------*/
class RecordItem
{
public:
	RecordItem(Field *pField, char* text)
	{
		this->m_pField = pField;
		this->SetText(text);
	}
	~RecordItem() {}

public:
	Field *GetField() { return m_pField; }

	void GetText(char* buffer) { strcpy(buffer, m_Text); }
	void SetText(char* text) { strcpy(m_Text, text); }

private:
	char m_Text[256];
	Field* m_pField;
};
/*----------------------------------------------------------------------------------------------*/
class Recordset
{
friend class Table;

public:
	Recordset() {}
	Recordset(Table *pTable)
	{
		m_pTable = pTable;
	}
	~Recordset() {}

public:
	short AddRecord(short iRow = 0);
	void ClearRecords() { m_Records.DeleteAllItems(); }
	Record* GetRecord(short iRow) { return m_Records.GetItem(iRow); }
	short GetRecordCount() { return m_Records.GetCount(); }
	Table* GetTable() { return m_pTable; }

	void SetText(short iRow, short iField, char* text);

private:
	void AddField(Field *pField, short iField = 0);
	void DeleteField(short iField);

	void SetTable(Table *pTable) { m_pTable = pTable; }

private:
	List<Record> m_Records;
	Table* m_pTable;
};
/*----------------------------------------------------------------------------------------------*/
class Table
{
public:
	Table()
	{
		m_Recordset.SetTable(this);
	}
	~Table() {}

public: // functions
	void ClearTable();
	Field* CreateField(char* text, short width, long flags, short iField = 0);
	void DeleteField(short iField);
	Field* GetField(short iField) { return m_Fields.GetItem(iField); }
	short GetFieldCount() { return m_Fields.GetCount(); }

public: // data
	Recordset m_Recordset;

private:
	List<Field> m_Fields;
};
/*----------------------------------------------------------------------------------------------*/
#endif