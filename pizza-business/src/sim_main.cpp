#include <cmath>
#include <vector>
#include <queue>
#include "list.h"
#include "sim_main.h"
#include "datarest.h"
#include "datapool.h"
#include <cstdlib>
#include <ctime>
#include <algorithm>
#include <fstream>
#include <iostream>
/*----------------------------------------------------------------------------------------------*/
const int MAX_STATS = 7;
const int NUM_OF_MINUTES = (24 * 60);
ofstream debugSim;
/*----------------------------------------------------------------------------------------------*/
using namespace std;
/*-----------------------------------------------------------------------------------------------+
|																								 |
|	CSimulator implementation																	 |
|																								 |
+-----------------------------------------------------------------------------------------------*/
CSimulator::CSimulator()
{
	moneyFromPizzas = 0;
	customerID = 0;
	currentMinute = 1;
}
/*----------------------------------------------------------------------------------------------*/
CSimulator::~CSimulator()
{}
/*----------------------------------------------------------------------------------------------*/
void CSimulator::SimulateTurn()
{
    int c;
    int totalCustomers;
	SimOrder tempOrder;

	srand(time(0));

#ifdef DEBUG_SIM
    debugSim << "DEBUG SIMULATION" << endl;
    debugSim << "BEGIN DAY..." << endl;
	debugSim << "Number of minutes to be simulated: " << NUM_OF_MINUTES << endl;
#endif
    
	// NOTE: for development purposes, the game will be saved before the simulation begins
	// this may be a feature to leave in the final version, but perhaps for every 2-5 turns
	// like an autosave feature
	this->SaveGame("lastturn.ini");

	
	// NOTE: as of now, there can only be 1 restaurant in the list
	// In place for when the player can run more than one restaurant
	COwner *pOwner = this->GetHumanPlayer();
	for(c = 1 ; c <= pOwner->GetRestaurantCount() ; c++)
	{
		CRestaurant *pRestaurant = pOwner->GetRestaurant(c);

		moneyFromPizzas = 0;
		
		currentMinute = 1;	// Initialize the clock
		customerID = 0;		// Initialize the customerID counter

		// Clear all the lists, vectors, and queues
        waiterVec.clear();
        cookVec.clear();
		managerVec.clear();
		ovens.clear();
		chairPool.clear();
		customerPool.clear();
		while (!pizzasToBePrepared.empty())
			pizzasToBePrepared.pop();
		while (!pizzasToBeCooked.empty())
			pizzasToBeCooked.pop();
		while (!finishedPizzas.empty())
			finishedPizzas.pop();

		
		// Create vectors of the employees currently working at the restaurant...
		FillEmployeeVectors(pRestaurant);

		// Create "oven slots" based on the ovens currently owned by the restaurant...
		FillOvenVector(pRestaurant);
		
		// Determine the number of customers that will enter the restaurant for today
		totalCustomers = DetNumOfCusts(pRestaurant);
		
		// Resize the customer vector to the # of customers that will visit today
		customerPool.resize(totalCustomers);

		// Resize the chair vector to the # of chairs available to customers
        chairPool.resize(pRestaurant->GetChairCount());
#ifdef DEBUG_SIM
		debugSim << "The total number of chairs in the restaurant is: " << chairPool.size() << endl;
#endif

		// Generate random arrival times for each customer...
		SetArrivalTimes();

		// Begin Stats...
		// Fill the structure
		DayStats *pStats = new DayStats;
		pStats->customers_enter = 0;
		pStats->customers_longwait = 0;
		pStats->customers_nospace = 0;
		pStats->pizzas_sold = 0;
		pStats->popularity = pRestaurant->GetCurrentPopularity();
		pStats->turn_id = this->GetTurn();
		pStats->money_made = pRestaurant->GetBudget();

		// add stats structure to beginning of stats list
		pRestaurant->m_StatsList.InsertItem(pStats, 1);

		// if the list gets bigger than MAX_STATS delete the last one
		int stats_count = pRestaurant->m_StatsList.GetCount();
		if(stats_count > MAX_STATS)
			pRestaurant->m_StatsList.DeleteItem(stats_count);
		// End Stats.

		// Set the stats for the number of customers that enter the restaurant
		pStats->customers_enter = totalCustomers;

		// Randomize the order of the ovens for the day...
		random_shuffle(ovens.begin(), ovens.end());
		/*--------------------------------------------------------------------------------*/
		// Begin time loop
		/*--------------------------------------------------------------------------------*/
		for (currentMinute = 1; currentMinute <= NUM_OF_MINUTES; currentMinute++)
		{
			// Change the order of the waiters and cooks in the vectors, so that the first cook or waiter ...
			// ... in the list doesn't get an "unfair" advantage (done every ten minutes)
			if (currentMinute % 10 == 0)
			{
				random_shuffle(waiterVec.begin(), waiterVec.end());
				random_shuffle(cookVec.begin(), cookVec.end());
			}

			// Manages customers who are scheduled to arrive this minute
			GenerateCustomerArrivals(pStats);

			// Updates the statuses of the waiters
			UpdateWaiterStatuses(pRestaurant, pStats);

			// Updates the statuses of the cooks
			UpdateCookStatuses(pRestaurant);

			// Updates the statuses of the ovens
			UpdateOvens();
			
            // Updates the statuses of the customers in the restaurant
			UpdateCustomerStatuses(pStats);
		}
		/*--------------------------------------------------------------------------*/
		// End main time loop
		/*--------------------------------------------------------------------------*/

		// do the daily bank stuff
		if(this->DoEndofTurnBankStuff() == 1) // equals 1 if restaurant was removed
			continue;

		// daily restaurant stuff
		pRestaurant->DoEndTurnStuff();

		// set the new budget in the stats
		pStats->money_made = pRestaurant->GetBudget() - pStats->money_made;
	}

	// increment the turn count
	this->SetTurn((GetTurn() + 1));
}
/*----------------------------------------------------------------------------------------------*/
void CSimulator::GenerateCustomerArrivals(DayStats *pStats)
{
	std::vector<SimCustomer>::iterator i;

	// Begin customer arrivals...
	for (i = customerPool.begin(); i != customerPool.end(); i++)
	{
	    // If a customer has an arrival time of NOW...
 		if (i->GetArrivalTime() == currentMinute)
		{
			// Attempt to assign the customer to a chair.
			// If a customer's assignment fails, there was no room...
			if (!AssignCustomerToChair(i))
            {
    		     pStats->customers_nospace++;
                 // TODO: Lower the restaurant's popularity rating...
				 pStats->popularity--;
             }    
		}
	}
}
/*----------------------------------------------------------------------------------------------*/
// NOTE: THIS FUNCTION IS MARKED TO BE CHANGED
// THIS ALGORITHM WILL BE UPDATED/CHANGED TO BETTER DETERMINE THE CUSTOMERS THAT WILL VISIT A RESTAURANT
int CSimulator::DetNumOfCusts(CRestaurant *restaurant)
{
    // 20 customers will visit each day, no matter the restaurant's popularity rating
    int total = 20; 
	int i, j;

	// Add a number of customers equal to the restaurant's popularity rating (the closest whole number)
	if (restaurant->GetCurrentPopularity() >= 0)
		total += (int)floor(restaurant->GetCurrentPopularity());
		
	// TODO:  Add a number of customers consistent with the advertising purchased for the turn...

	if (restaurant->m_StatsList.GetCount() == 0)
	{
		// Then there is no "previous day," so the advertisements bought will affect the number
		// ... of customers in a constant way
		for (i = 1; i <= restaurant->m_AdsList.GetCount(); i++)
		{
			j = restaurant->m_AdsList.GetItem(i)->quantity;
			switch (restaurant->m_AdsList.GetItem(i)->pAd->GetFactor())
			{
			case 0:
				total += (3 * j);
				break;
			case 1:
				total += (5 * j);
				break;
			case 2:
				total += (7 * j);
				break;
			case 3:
				total += (9 * j);
				break;
			case 4:
				total += (11 * j);
				break;
			case 5:
				total += (13 * j);
				break;
			case 6:
				total += (15 * j);
				break;
			case 7:
				total += (17 * j);
				break;
			case 8:
				total += (19 * j);
				break;
			case 9:
				total += (20 * j);
				break;
			case 10:
				total += (25 * j);
				break;
			}
		}
	}
	else	// If this is not the first day, base the number of extra consumers on a
			// percentage of the previous day's customers
	{
		for (i = 1; i <= restaurant->m_AdsList.GetCount(); i++)
		{
			j = restaurant->m_AdsList.GetItem(i)->quantity;
			total += (j * ( (restaurant->m_StatsList.GetItem(1)->customers_enter) *
						(restaurant->m_AdsList.GetItem(i)->pAd->GetFactor() * .1) ) );
		}	
	}
#ifdef DEBUG_SIM
    debugSim << "The restaurant's current popularity is: " << restaurant->GetCurrentPopularity() << endl;
    debugSim << "Total number of potential customers for the day: " << total << endl;
#endif
	return total;
}
/*----------------------------------------------------------------------------------------------*/
void CSimulator::SetArrivalTimes()
{
	int i;
	// For each customer in the customerPool, set a random arrival time
	for (i = 0; i < customerPool.size(); i++)
	{
		// Generate random arrival times from minute 1 to 60 minutes before the restaurant "closes"
		customerPool[i].SetArrivalTime(1 + (rand() % (NUM_OF_MINUTES - 60)));
#ifdef DEBUG_SIM
		debugSim << "Customer " << i << " will arrive at: " << customerPool[i].GetArrivalTime() << endl;
#endif
		
	}
}
/*----------------------------------------------------------------------------------------------*/
void CSimulator::FillEmployeeVectors(CRestaurant* restaurant)
{
    int i;
	List<CEmployee> waiterList, cookList, manager;

	// Put the employees into lists based on their job...
    restaurant->GetEmployees(&waiterList, TITLE_WAITER);
    restaurant->GetEmployees(&cookList, TITLE_CHEF);
    restaurant->GetEmployees(&manager, TITLE_MANAGER);

    // Put the waiters, cooks, and manager into vectors so that random_shuffle can be used.
	waiterVec.resize(waiterList.GetCount());
    for (i = 1; i <= waiterList.GetCount(); i++)
    {
        waiterVec[i-1].actualEmployee = waiterList.GetItem(i);
    }
	cookVec.resize(cookList.GetCount());
    for (i = 1; i <= cookList.GetCount(); i++)
    {
        cookVec[i-1].actualEmployee = cookList.GetItem(i);
    }
	managerVec.resize(manager.GetCount());
	for (i = 1; i <= manager.GetCount(); i++)
	{
		managerVec[i-1].actualEmployee = manager.GetItem(i);
	}
	// Set the statuses of the waiters, cooks, and managers to NOT_BUSY
    for (i = 0; i < waiterVec.size(); i++)
	{
        waiterVec[i].SetCurrentStatus(NOT_BUSY);
		waiterVec[i].id = i;
	}
    for (i = 0; i < cookVec.size(); i++)
	{
		cookVec[i].SetCurrentStatus(NOT_BUSY);
		cookVec[i].id = i;
	}
	for (i = 0; i < managerVec.size(); i++)
	{
		managerVec[i].SetCurrentStatus(NOT_BUSY);
		managerVec[i].id = i;
	}

#ifdef DEBUG_SIM
debugSim << "Filling employee lists..." << endl;
debugSim << "Number of waiters: " << waiterVec.size() << endl;
debugSim << "Number of cooks: " << cookVec.size() << endl;
debugSim << "Number of managers: " << manager.GetCount() << endl;
#endif
}
/*----------------------------------------------------------------------------------------------*/
bool CSimulator::AssignCustomerToChair(vector<SimCustomer>::iterator i)
{
    bool isThereSpace = false;
	int j;
 
	// Iterate through the pool of chairs...
	for (j = 0; j < chairPool.size(); j++)
	{
	    // If there is an empty chair, have the customer occupy it
	    if (!chairPool[j].IsChairOccupied())
		{
#ifdef DEBUG_SIM
			debugSim << "Customer has occupied chair " << j << endl;
#endif
            chairPool[j].SetChairOccupied(true);
            chairPool[j].SetOccupant(&(*i));
			// Set this boolean flag to true - there was space for the customer
			isThereSpace = true;
			(*i).SetCustomerID(customerID);
            (*i).SetStatusEnd(currentMinute + 60); // Customer will wait 60 mins for order to be taken
			customerID++;
			break;
		}
    } 
	// If there was space for the customer, return true
    if (isThereSpace)
        return true;
	// ... otherwise return false
    else
        return false;
}
/*----------------------------------------------------------------------------------------------*/
void CSimulator::FillOvenVector(CRestaurant* restaurant)
{
	int i, j;
	COven* myOven;

	// Iterate through the list of ovens in CRestaurant...
	for (i = 1; i <= (restaurant->m_OvenList.GetCount()); i++)
	{
		myOven = restaurant->m_OvenList.GetItem(i);
		// Put a number of "oven slots" into the ovens vector equal...
		// to that of the number of pizzas that the oven supports
		for (j = 0; j < myOven->GetPizzaSupport(); j++)
		{
			// Generate a SimOvenSlot with a cook time equal to that of the oven, and put...
			// it into the oven vector
			ovens.push_back(SimOvenSlot(myOven->GetCookTime()));
		}
	}
#ifdef DEBUG_SIM
	debugSim << "There are " << ovens.size() << " \"oven slots\" in the restaurant" << endl;
#endif
}
/*----------------------------------------------------------------------------------------------*/
SimOrder CSimulator::GenerateOrder(std::vector<SimChair>::iterator chair, CRestaurant *pRestaurant)
{
	int numberOfPizzas;
	int randomNumber;
	SimOrder myOrder;
	CPizzaObject myPizza;
	CPizza customPizza;
	int i, j;
	int num;

	srand(time(0));
	
	// If the restaurant is setup to allow the customers to create their own pizzas...
	if (pRestaurant->Toppings_IsSelectable())
	{
		// Then generate customer-chosen pizza at random
		// NOTE:  THIS MAY BE CHANGED TO BETTER CORRELATE WITH REAL STATISTICS OF PEOPLE'S FAVORITE TOPPINGS
		
		// For each category of ingredients...
		for (i = 1; i <= pRestaurant->m_IngList.GetCount(); i++)
		{
			// Choose a number of ingredients to take from that particular category.
			num = 1 + (rand() % pRestaurant->m_IngList.GetItem(i)->ingredients.GetCount());	
			// For each ingredient to be chosen from that category...
			for (j = 0; j < num; j++)
			{
				// Choose an ingredient from the category, and add it to the pizza
				customPizza.AddIngredient((pRestaurant->m_IngList).GetItem(i)->ingredients.GetItem(1 + (rand() % pRestaurant->m_IngList.GetItem(i)->ingredients.GetCount()))->pIngredient);
			}
		}
		// Generate a CPizzaObject from the CPizza object that was created...
		CPizzaObject myPizza2(&customPizza);
		// And set the order's pizza to it
		myOrder.SetPizza(&myPizza2);
		// If the restaurant has the ingredients to make this pizza...
		if (pRestaurant->IsPizzaSupported(&myPizza2))
		{
			// Set the order's information, including the ID of the customer who ordered it...
			myOrder.SetOrdererID((*chair).GetOccupant()->GetCustomerID());
			// and the chair of the customer who ordered it.
			myOrder.SetChairOfOrderer(chair);
		}
		// If the restaurant does NOT have the ingredients to make this pizza...
		else
		{
			// Set the orderer ID of the order to -1, which shows that the order cannot be made and so cannot be taken
			myOrder.SetOrdererID(-1);
		}
		return myOrder;
	}
	// If the restaurant is not setup to have customers choose their own toppings...
	// ...choose one of the premade pizzas available
	else
	{
		// Get the total number of pre-made pizzas available
		numberOfPizzas = pRestaurant->m_PizzaList.GetCount();
		// Choose a pizza at random...
		randomNumber = (1 + (rand() % numberOfPizzas));
		myPizza = (*pRestaurant->m_PizzaList.GetItem(randomNumber));
		myOrder.SetPizza(&myPizza);

		// If the pizza is able to made, then the information is set...
		if (pRestaurant->IsPizzaSupported(&myPizza))
		{
			myOrder.SetOrdererID((*chair).GetOccupant()->GetCustomerID());
			myOrder.SetChairOfOrderer(chair);
		}
		// Otherwise the flag is set, so that the order will not be taken
		else
		{
			myOrder.SetOrdererID(-1);
		}
		return myOrder;
	// IMPORTANT!!!! NOTE: When the function is called, check to ensure that the order created has a customerID that is not -1
	}
}
/*----------------------------------------------------------------------------------------------*/
void CSimulator::UpdateWaiterStatuses(CRestaurant *pRestaurant, DayStats *pStats)
{
	int j;
	SimOrder tempOrder;
	std::vector<SimChair>::iterator k;
	
	// Go through the list of waiters...
    for (j = 0; j < waiterVec.size(); j++)
    {
		// Check the waiter's current status
		switch (waiterVec[j].GetCurrentStatus())
		{
		/* BEGIN CASE NOT_BUSY ------------------------------------*/
		case NOT_BUSY:
			// If the waiter is not buys...
			// Have the waiter check chair to see if there are customers WAITING_TO_ORDER
            for (k = chairPool.begin(); k != chairPool.end(); k++)
            {
				// If a chair is occupied, and the customer sitting there is waiting to order...
			    if ((*k).IsChairOccupied() == true && 
                    ((*k).GetOccupant())->GetCurrentStatus() == WAITING_TO_ORDER)
                {
					// Have the waiter begin to take the customer's order.
					waiterVec[j].SetCurrentStatus(TAKING_ORDER);
					waiterVec[j].SetCurrentCustomerChair(k);
					// Have the customer begin waiting for his pizza
					// NOTE: it is necessary to change the customer's status at this point...
					// otherwise another waiter will take this customer's order again possibly.
					((*k).GetOccupant())->SetCurrentStatus(WAITING_FOR_PIZZA);
					
			  		// TODO: Change the competency ranges, so that there's more posssibilities
					// If the waiter's competency is >= 70, the ordering will only take one minute
                    // If the waiter's competency is >= 40 but < 70, the ordering will take two minutes 
					if ( (waiterVec[j].actualEmployee->GetCompetency()) >= 70 )
                    {
	                    waiterVec[j].SetStatusEnd(currentMinute + 1);
                    }
                    else if ( (waiterVec[j].actualEmployee->GetCompetency()) < 70 )
                    {
		                waiterVec[j].SetStatusEnd(currentMinute + 2);
		            }
#ifdef DEBUG_SIM
					debugSim << "Waiter " << waiterVec[j].id << " is now taking an order from the customer located at a table occupied by customer number " << (*k).GetOccupant()->GetCustomerID() << endl;
					debugSim << "Waiter " << waiterVec[j].id << " will be finished taking the order at " << waiterVec[j].GetStatusEnd() << endl;
#endif
					break;
				}
			}
			// If the waiter's status is still not busy, that means that he has checked...
			// for a customer waiting order and has found none.  Therefore, he can check to see...
			// if there's a pizza waiting to be returned to a customer.
			if (waiterVec[j].GetCurrentStatus() == NOT_BUSY)
			{
				// If there is a finished pizza to be returned...
				if (!finishedPizzas.empty())
				{
					// Set the current order of the waiter to this finished pizza...
					waiterVec[j].SetCurrentOrder(finishedPizzas.front());
					if ( waiterVec[j].actualEmployee->GetCompetency() > 
						((m_DataPool.GetMaxCompetency() + m_DataPool.GetMinCompetency()) / 2) )
						waiterVec[j].SetStatusEnd(currentMinute + 1);
					else
						waiterVec[j].SetStatusEnd(currentMinute + 2);
					// And change the waiter's current status	
					waiterVec[j].SetCurrentStatus(TAKING_PIZZA_TO_CUST);
					// Also set the current chair of the waiter's customer to the chair at which the customer who ordered...
					// the finished pizza is sitting
					waiterVec[j].SetCurrentCustomerChair(waiterVec[j].GetCurrentOrder().GetChairOfOrderer());
					// Remove the finished pizza from the queue...
					finishedPizzas.pop();
#ifdef DEBUG_SIM
					debugSim << "Waiter " << waiterVec[j].id << " is now taking a pizza to a customer" << endl;
#endif
				}
			}
			break;
			/* END CASE NOT_BUSY ------------------------------------*/
			case TAKING_ORDER:
				// If the waiter's is currently taking an order...
				// AND the waiter is to be finished this minute
				if (waiterVec[j].GetStatusEnd() == currentMinute)
				{
					// Update the waiter's "personal" stats...
					waiterVec[j].actualEmployee->SetStatistic(PIZZAS_ORDERED, waiterVec[j].actualEmployee->GetStatistic(PIZZAS_ORDERED) + 1);
					// And actually generate the order.
					tempOrder = GenerateOrder(waiterVec[j].GetCurrentCustomerChair(), pRestaurant);
					// If the ordererID of the order is not -1, that means the order can be made...
					if (tempOrder.GetOrdererID() != -1)
					{
						// The order is put into a queue for the cooks to prepare
						pizzasToBePrepared.push(tempOrder);
						// The customer begins waiting for the pizza...
						waiterVec[j].GetCurrentCustomerChair()->GetOccupant()->SetCurrentStatus(WAITING_FOR_PIZZA);
						(waiterVec[j].GetCurrentCustomerChair()->GetOccupant())->SetStatusEnd(currentMinute + 120);
#ifdef DEBUG_SIM
						debugSim << "Waiter " << waiterVec[j].id << "has finished taking the customer " << waiterVec[j].GetCurrentCustomerChair()->GetOccupant()->GetCustomerID() << " order and the order has been generated."  << endl;
#endif
					}
					// If the ordererID of the order IS -1, the order could not be made.
					else
					{
						// The customer leaves,  because they don't have the pizza he wants
						// NOTE:  THERE SHOULD BE A STAT FOR THE NUMBER OF CUSTOMERS WHO LEAVE BECAUSE THEIR PIZZA CANNOT BE MADE
#ifdef DEBUG_SIM
						debugSim << "Waiter " << waiterVec[j].id << "has finished taking an order, but it has not been generated because it cannot be made." << endl;
#endif
					}
					waiterVec[j].SetCurrentStatus(NOT_BUSY);
				}
			break;
			case TAKING_PIZZA_TO_CUST:
				if (waiterVec[j].GetStatusEnd() == currentMinute)
				{
					// Update the waiter's "personal" stats...
					waiterVec[j].actualEmployee->SetStatistic(PIZZAS_SERVED, waiterVec[j].actualEmployee->GetStatistic(PIZZAS_SERVED) + 1);
					if (waiterVec[j].GetCurrentCustomerChair()->IsChairOccupied())
					{
						// Give the restaurant the money for the pizza
						if (pRestaurant->BuyPizza(waiterVec[j].GetCurrentOrder().GetPizza()))
						{
#ifdef DEBUG_SIM
						debugSim << "Waiter " << waiterVec[j].id << " has taken a pizza to customer " << waiterVec[j].GetCurrentCustomerChair()->GetOccupant()->GetCustomerID() << " and the pizza has been payed for" << endl;
#endif
							moneyFromPizzas += waiterVec[j].GetCurrentOrder().GetPizza()->GetPrice();
							waiterVec[j].GetCurrentCustomerChair()->GetOccupant()->SetCurrentStatus(EATING_PIZZA);
							waiterVec[j].GetCurrentCustomerChair()->GetOccupant()->SetStatusEnd(currentMinute + 20);
							pStats->pizzas_sold++;
						}
						else
						{
							// The customer leaves, pop rating goes down...
#ifdef DEBUG_SIM
							debugSim << "Waiter " << waiterVec[j].id << " has taken a pizza to customer " << waiterVec[j].GetCurrentCustomerChair()->GetOccupant()->GetCustomerID() << " but there was a problem, and the customer has not payed for the pizza" << endl;
#endif
						}
						waiterVec[j].SetCurrentStatus(NOT_BUSY);
					}
					else
					{
						waiterVec[j].SetCurrentStatus(NOT_BUSY);
						// Note: the restaurant gets no money from the pizza, because...
						// ... the customer left.
#ifdef DEBUG_SIM
							debugSim << "Waiter " << waiterVec[j].id << " has taken a pizza to a customer, but the customer has left already" << endl;
#endif
					}
				}
			break;
				
			}
	}
}
/*----------------------------------------------------------------------------------------------*/
void CSimulator::UpdateCookStatuses(CRestaurant *pRestaurant)
{
	int j;
	for (j = 0; j < cookVec.size(); j++)
	{
		switch(cookVec[j].GetCurrentStatus())
		{
		case NOT_BUSY:
			// Check the PIZZAS_TO_BE_PREPARED queue
			if (!pizzasToBePrepared.empty())
			{
				// BEGIN PREPARING A CERTAIN PIZZA
				cookVec[j].SetCurrentOrder(pizzasToBePrepared.front());
				pizzasToBePrepared.pop();
				cookVec[j].SetCurrentStatus(PREPARING_PIZZA);
				if (cookVec[j].actualEmployee->GetCompetency() > 
					((m_DataPool.GetMaxCompetency() + m_DataPool.GetMinCompetency()) / 2))
					cookVec[j].SetStatusEnd(currentMinute + 10);
				else
					cookVec[j].SetStatusEnd(currentMinute + 15);
#ifdef DEBUG_SIM
				debugSim << "Cook " << cookVec[j].id << " has begun preparing a pizza.  He will be done at " << cookVec[j].GetStatusEnd() << endl;
#endif
			}
			break;
		case PREPARING_PIZZA:
			if (cookVec[j].GetStatusEnd() == currentMinute)
			{
				// Update the cook's "personal" stats...
				cookVec[j].actualEmployee->SetStatistic(PIZZAS_COOKED, cookVec[j].actualEmployee->GetStatistic(PIZZAS_COOKED) + 1);
				pRestaurant->CookPizza(cookVec[j].GetCurrentOrder().GetPizza());
#ifdef DEBUG_SIM
				debugSim << "Cook " << cookVec[j].id << " has finished preparing a pizza" << endl;
#endif
				// WRONG: finishedPizzas.push(cookVec[j].GetCurrentOrder());
				// Put the pizza in the TO_BE_COOKD queue
				pizzasToBeCooked.push(cookVec[j].GetCurrentOrder());
				cookVec[j].SetCurrentStatus(NOT_BUSY);
			}
			break;
		}
	}
}
/*----------------------------------------------------------------------------------------------*/
void CSimulator::UpdateCustomerStatuses(DayStats *pStats)
{
	int i;
	for (i = 0; i < chairPool.size(); i++)
	{
		if (chairPool[i].IsChairOccupied())
		{
			switch(chairPool[i].GetOccupant()->GetCurrentStatus())
			{
			case WAITING_TO_ORDER:
				if (chairPool[i].GetOccupant()->GetStatusEnd() == currentMinute)
				{
					// The customer leaves
					chairPool[i].SetChairOccupied(false);
					pStats->customers_longwait++;
					// Lower popularity rating of restaurant
					pStats->popularity--;
				}
				break;
			case ORDERING:
				// Taken care of by the waiter's TAKING_ORDER case...
				break;
			case WAITING_FOR_PIZZA:
				if (chairPool[i].GetOccupant()->GetStatusEnd() == currentMinute)
				{
					// The customer leaves
#ifdef DEBUG_SIM
					debugSim << "Customer " << chairPool[i].GetOccupant()->GetCustomerID() << " has left because he waited too long for his pizza" << endl;
#endif
					chairPool[i].SetChairOccupied(false);
					pStats->customers_longwait++;
					// Lower popularity rating of restaurant
					pStats->popularity--;
				}
				break;
			case EATING_PIZZA:
				if (chairPool[i].GetOccupant()->GetStatusEnd() == currentMinute)
				{
					// The customer leaves
					chairPool[i].SetChairOccupied(false);
					// Raise popularity rating of restaurant
					pStats->popularity += 1;
				}
			}
		}
	}
}
/*----------------------------------------------------------------------------------------------*/
void CSimulator::UpdateOvens()
{
	int i;
	// Check to see if there's room in an oven to put the pizza
	for (i = 0; i < ovens.size(); i++)
	{
		if (!pizzasToBeCooked.empty())
		{
			if (!(ovens[i].IsOccupied()))
			{
				ovens[i].SetOrderCooking(pizzasToBeCooked.front());
				pizzasToBeCooked.pop();
				ovens[i].SetOccupancy(true);
				ovens[i].SetCookingEndTime(currentMinute + ovens[i].GetCookTime());
#ifdef DEBUG_SIM
				debugSim << "Oven " << i << " is now cooking a pizza, which will be completed at " << ovens[i].GetCookingEndTime() << endl;
#endif
				break;
			}
		}
	}
	// Check to see if there's pizzas that are done this minute
	for (i = 0; i < ovens.size(); i++)
	{
		if (ovens[i].GetCookingEndTime() == currentMinute)
		{
			finishedPizzas.push(ovens[i].GetOrderCooking());
			ovens[i].SetOccupancy(false);
#ifdef DEBUG_SIM
			debugSim << "A pizza has finished cooking in oven " << i << endl;
#endif
		}
	}
}
/*-----------------------------------------------------------------------------------------------+
|																								 |
|	SimCustomer implementation																		 |
|																								 |
+-----------------------------------------------------------------------------------------------*/
SimCustomer::SimCustomer()
{
    currentAction = WAITING_TO_ORDER;
}
/*----------------------------------------------------------------------------------------------*/
void SimCustomer::SetArrivalTime(int time)
{
    arrivalTime = time;
}
/*----------------------------------------------------------------------------------------------*/
int SimCustomer::GetArrivalTime()
{
	return arrivalTime;
}
/*----------------------------------------------------------------------------------------------*/
void SimCustomer::SetCustomerID(int id)
{
	customerID = id;
}
/*----------------------------------------------------------------------------------------------*/
int SimCustomer::GetCustomerID()
{
	return customerID;
}
/*----------------------------------------------------------------------------------------------*/
STATUS SimCustomer::GetCurrentStatus()
{
    return currentAction;
}
/*----------------------------------------------------------------------------------------------*/
void SimCustomer::SetCurrentStatus(STATUS newAction)
{
    currentAction = newAction;
}
/*----------------------------------------------------------------------------------------------*/
void SimCustomer::SetStatusEnd(int time)
{
    currActionEndsAt = time;
}
/*----------------------------------------------------------------------------------------------*/
int SimCustomer::GetStatusEnd()
{
    return currActionEndsAt;
}
/*----------------------------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------------------------+
|																								 |
|	SimChair implementation																		 |
|																								 |
+-----------------------------------------------------------------------------------------------*/
SimChair::SimChair()
{
    occupied = false;
}
/*----------------------------------------------------------------------------------------------*/
SimChair::~SimChair()
{}
/*----------------------------------------------------------------------------------------------*/
SimCustomer* SimChair::GetOccupant()
{
    return occupant;
}
/*----------------------------------------------------------------------------------------------*/
void SimChair::SetOccupant(SimCustomer* newOccupant)
{
    occupant = newOccupant;
}
/*----------------------------------------------------------------------------------------------*/
bool SimChair::IsChairOccupied()
{
    return occupied;
}
/*----------------------------------------------------------------------------------------------*/
void SimChair::SetChairOccupied(bool newOccupied)
{
    occupied = newOccupied;
}
/*-----------------------------------------------------------------------------------------------+
|																								 |
|	SimOvenSlot implementation																	 |
|																								 |
+-----------------------------------------------------------------------------------------------*/
SimOvenSlot::SimOvenSlot()
{
	myCookTime = 0;
}
/*----------------------------------------------------------------------------------------------*/
SimOvenSlot::SimOvenSlot(int cookTime)
{
	myCookTime = cookTime;
	isOccupied = false;
}
/*----------------------------------------------------------------------------------------------*/
SimOvenSlot::~SimOvenSlot() {}
/*----------------------------------------------------------------------------------------------*/
void SimOvenSlot::SetCookTime(int cookTime)
{
	myCookTime = cookTime;
}
/*----------------------------------------------------------------------------------------------*/
int SimOvenSlot::GetCookTime()
{
	return myCookTime;
}
/*----------------------------------------------------------------------------------------------*/
bool SimOvenSlot::IsOccupied()
{
	return isOccupied;
}
/*----------------------------------------------------------------------------------------------*/
void SimOvenSlot::SetOccupancy(bool occupied)
{
	isOccupied = occupied;
}
/*----------------------------------------------------------------------------------------------*/
void SimOvenSlot::SetOrderCooking(SimOrder order)
{
	orderCooking = order;
}
/*----------------------------------------------------------------------------------------------*/
SimOrder SimOvenSlot::GetOrderCooking()
{
	return orderCooking;
}
/*----------------------------------------------------------------------------------------------*/
void SimOvenSlot::SetCookingEndTime(int endTime)
{
	cookingEndTime = endTime;
}
/*----------------------------------------------------------------------------------------------*/
int SimOvenSlot::GetCookingEndTime()
{
	return cookingEndTime;
}
/*-----------------------------------------------------------------------------------------------+
|																								 |
|	SimOrder implementation																	 |
|																								 |
+-----------------------------------------------------------------------------------------------*/
SimOrder::SimOrder() {}
/*----------------------------------------------------------------------------------------------*/
SimOrder::~SimOrder() {}
/*----------------------------------------------------------------------------------------------*/
int SimOrder::GetOrdererID()
{
	return ordererID;
}
/*----------------------------------------------------------------------------------------------*/
void SimOrder::SetOrdererID(int id)
{
	ordererID = id;
}
/*----------------------------------------------------------------------------------------------*/
std::vector<SimChair>::iterator SimOrder::GetChairOfOrderer()
{
	return chair;
}
/*----------------------------------------------------------------------------------------------*/
void SimOrder::SetChairOfOrderer(std::vector<SimChair>::iterator chairTemp)
{
	chair = chairTemp;
}
/*----------------------------------------------------------------------------------------------*/
CPizzaObject* SimOrder::GetPizza()
{
	return &pizza;
}
/*----------------------------------------------------------------------------------------------*/
void SimOrder::SetPizza(CPizzaObject *newPizza)
{
	pizza = (*newPizza);
}
/*-----------------------------------------------------------------------------------------------+
|																								 |
|	SimEmployee implementation																	 |
|																								 |
+-----------------------------------------------------------------------------------------------*/
SimEmployee::SimEmployee() {}
/*----------------------------------------------------------------------------------------------*/
SimEmployee::~SimEmployee() {}
/*----------------------------------------------------------------------------------------------*/
STATUS SimEmployee::GetCurrentStatus()
{
	return currentStatus;
}
/*----------------------------------------------------------------------------------------------*/
void SimEmployee::SetCurrentStatus(STATUS newStatus)
{
	currentStatus = newStatus;
}
/*----------------------------------------------------------------------------------------------*/
int SimEmployee::GetStatusEnd()
{
	return statusEnd;
}
/*----------------------------------------------------------------------------------------------*/
void SimEmployee::SetStatusEnd(int end)
{
	statusEnd = end;
}
/*----------------------------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------------------------+
|																								 |
|	SimWaiter implementation																	 |
|																								 |
+-----------------------------------------------------------------------------------------------*/
SimWaiter::SimWaiter() {}
/*----------------------------------------------------------------------------------------------*/
SimWaiter::~SimWaiter() {}
/*----------------------------------------------------------------------------------------------*/
std::vector<SimChair>::iterator SimWaiter::GetCurrentCustomerChair()
{
	return currentCustomerChair;
}
/*----------------------------------------------------------------------------------------------*/
void SimWaiter::SetCurrentCustomerChair(std::vector<SimChair>::iterator customerChair)
{
	currentCustomerChair = customerChair;
}
/*----------------------------------------------------------------------------------------------*/
SimOrder SimWaiter::GetCurrentOrder()
{
	return currentOrder;
}
/*----------------------------------------------------------------------------------------------*/
void SimWaiter::SetCurrentOrder(SimOrder newOrder)
{
	currentOrder = newOrder;
}
/*-----------------------------------------------------------------------------------------------+
|																								 |
|	SimCook implementation																	 |
|																								 |
+-----------------------------------------------------------------------------------------------*/
SimCook::SimCook() {}
/*----------------------------------------------------------------------------------------------*/
SimCook::~SimCook() {}
/*----------------------------------------------------------------------------------------------*/
SimOrder SimCook::GetCurrentOrder()
{
	return currentOrder;
}
/*----------------------------------------------------------------------------------------------*/
void SimCook::SetCurrentOrder(SimOrder newOrder)
{
	currentOrder = newOrder;
}
/*----------------------------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------------------------+
|																								 |
|	SimManager implementation																	 |
|																								 |
+-----------------------------------------------------------------------------------------------*/
SimManager::SimManager() {}
/*----------------------------------------------------------------------------------------------*/
SimManager::~SimManager() {}
/*----------------------------------------------------------------------------------------------*/
std::vector<SimChair>::iterator SimManager::GetCurrentCustomerChair()
{
	return currentCustomerChair;
}
/*----------------------------------------------------------------------------------------------*/
void SimManager::SetCurrentCustomerChair(std::vector<SimChair>::iterator customerChair)
{
	currentCustomerChair = customerChair;
}
/*----------------------------------------------------------------------------------------------*/
SimOrder SimManager::GetCurrentOrder()
{
	return currentOrder;
}
/*----------------------------------------------------------------------------------------------*/
void SimManager::SetCurrentOrder(SimOrder newOrder)
{
	currentOrder = newOrder;
}
/*----------------------------------------------------------------------------------------------*/