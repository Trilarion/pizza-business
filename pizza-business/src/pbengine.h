#ifndef _JS_PBENGINE_H
#define _JS_PBENGINE_H
/*----------------------------------------------------------------------------------------------*/
#include "datarest.h"
#include "datapool.h"
#include "bank.h"
/*----------------------------------------------------------------------------------------------*/
class CGameEngine
{
public: // construction/destruction
	CGameEngine();
	~CGameEngine() {}
	bool Initialize();

public: // data
	List<COwner> m_OwnerList;
	CDataPool m_DataPool;

	CBank m_Bank;

public: // functions
	int DoEndofTurnBankStuff();

	bool GetError(char *buffer) { strcpy(buffer, error_msg); return error; }
	COwner* GetHumanPlayer();
	COwner* GetPlayer(unsigned int index) { return m_OwnerList.GetItem(index); }
	unsigned int GetPlayerCount() { return m_OwnerList.GetCount(); }
	
	unsigned int GetTurn() { return m_GameTurn; }
	void SetTurn(unsigned int turn) { m_GameTurn = turn; }
	bool IsGameInSession() { return bInSession; }

	bool RestIdAvailable(unsigned short id);
	unsigned short GetNextAvailableRestID();

public: // engine functions
	void StartNewGame();
	bool LoadGame(char* filename);
	void SaveGame(char* filename);

private:
	short AddPlayer(COwner *pOwner, bool create_restaurant = true);
	void RemoveRestaurant(CRestaurant *pRestaurant);

private:
	char* GetCommaLimit(char* lpstrText, char* return_buffer);

private:
	unsigned int m_GameTurn;
	bool bInSession;

private:
	void SetError(char *message);
	bool error;
	char error_msg[256];
};
/*----------------------------------------------------------------------------------------------*/
#endif