#ifndef _JS_MAIN_H
#define _JS_MAIN_H
/*----------------------------------------------------------------------------------------------*/
#include "interface.h"
#include "logger.h"
#include "sim_main.h"
/*----------------------------------------------------------------------------------------------*/
class CApplication
{
public:
	CApplication() : m_LogFile("corelog.log"), m_pInterface(0) {}

public:
	CLogFile m_LogFile;												// the shared logging system
	CSimulator m_Simulator;											// the simulator & game engine
	CInterface *m_pInterface;										// a pointer to the generic interface
};
/*----------------------------------------------------------------------------------------------*/
#endif