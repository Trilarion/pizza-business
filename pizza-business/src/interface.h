#ifndef _JS_INTERFACE_H
#define _JS_INTERFACE_H
/*----------------------------------------------------------------------------------------------*/
#include <cstring>
#include "datarest.h"

class CApplication;
/*----------------------------------------------------------------------------------------------*/
class CInterface
{
public:
	CInterface();

public:
	virtual int Run() { return -1; };							// called from main function

public:
	// current restaurant
	CRestaurant* GetCurrentRestaurant() { return m_pCurRestaurant; }
	void SetCurrentRestaurant(CRestaurant *pRestaurant) { m_pCurRestaurant = pRestaurant; }

	CApplication* GetApplication() { return m_pApplication; }
	void SetApplication(CApplication *pApp) { m_pApplication = pApp; }

private:
	CRestaurant *m_pCurRestaurant;

protected:
	CApplication *m_pApplication;
};
/*----------------------------------------------------------------------------------------------*/
#endif