#ifndef _JS_PIZZAS_H
#define _JS_PIZZAS_H
/*----------------------------------------------------------------------------------------------*/
#include "buysell.h"
/*----------------------------------------------------------------------------------------------*/
class CPizza
{
public: // construction/destruction
	CPizza();

public: // functions
	// ingredients
	bool AddIngredient(CIngredient *pIngredient);
	CIngredient* GetIngredient(unsigned int index);
	unsigned int GetIngredientCount() { return m_IngList.GetCount(); }
	void RemoveAllIngredients();

	// pricing
	float GetProductionCost();

	// name
	void GetName(char *buffer) { strcpy(buffer, m_Name); }
	void SetName(char *name) { strcpy(m_Name, name); }

private: // data
	List<CIngredient> m_IngList;
	char m_Name[256];
};
/*----------------------------------------------------------------------------------------------*/
class CPizzaObject
{
public:
	CPizzaObject() {
		m_pPizza = 0;
		this->SetPrice(0.0f);
	};
	CPizzaObject(CPizza *pPizza) {
		m_pPizza = pPizza;
		this->SetPrice(0.0f);
	};

public:
	CPizza* GetPizza() { return m_pPizza; }

	float GetPrice() { return m_Price; }
	void SetPrice(float price) { m_Price = price; }

private:
	float m_Price;
	CPizza *m_pPizza;
};
/*----------------------------------------------------------------------------------------------*/
#endif