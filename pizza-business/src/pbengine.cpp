#include "stdio.h"
#include "pbengine.h"
/*----------------------------------------------------------------------------------------------*/
#include "inifiles.h"
/*----------------------------------------------------------------------------------------------*/
CGameEngine::CGameEngine() : m_GameTurn(0), error(false), bInSession(false)
{
	strcpy(this->error_msg, "");
}
/*----------------------------------------------------------------------------------------------*/
short CGameEngine::AddPlayer(COwner *pOwner, bool create_restaurant)
{
	// add owner to the list
	this->m_OwnerList.AddItem(pOwner);

	if(create_restaurant == true)
	{
		// each player starts with one restaurant
		CRestaurant *pRestaurant = new CRestaurant(pOwner);
		pOwner->AddRestaurant(pRestaurant);
		// assign the restaurant an identification number
		pRestaurant->SetId(this->GetNextAvailableRestID());

		// initialize the ingredient list
		pRestaurant->InitIngredientList(&this->m_DataPool.m_IngList);
		// initialize the pizza list
		pRestaurant->InitPizzaList(&this->m_DataPool.m_PizzaList);

		// create an account at the bank
		// for the new restaurant
		pRestaurant->m_pBankAccount = this->m_Bank.CreateAccount(pRestaurant);
	}

	// return the number of players in the game
	return this->m_OwnerList.GetCount();
}
/*----------------------------------------------------------------------------------------------*/
int CGameEngine::DoEndofTurnBankStuff()
{
	// time to collect!
	this->m_Bank.CollectLoanMoney();

	// see if any accounts have expired. if one has expired, remove the account
	// and remove its corresponding restaurant from the game
	for(unsigned int c = 1 ; c <= this->m_Bank.GetAccountCount() ; c++)
	{
		CBankAccount *pAccount = this->m_Bank.GetAccount(c);
		if(pAccount->IsExpired())
		{
			CRestaurant *pRestaurant = pAccount->GetRestaurant();
			this->m_Bank.DeleteAccount(pAccount->GetRestaurant());
			this->RemoveRestaurant(pRestaurant);
			return 1;
		}
	}
	return 0;
}
/*----------------------------------------------------------------------------------------------*/
char* CGameEngine::GetCommaLimit(char* lpstrText, char* return_buffer)
{
	char* rtn_text = 0;
	// parse through the string looking for the
	// comma or the null
	for(short i = 0 ; i <= strlen(lpstrText) ; i++)
	{
		switch(lpstrText[i])
		{
		case '\0':
		case '\n':
			if(i == 0) return 0;
			return_buffer[i] = '\0';
			rtn_text = &lpstrText[i];
			return rtn_text;

		case ',':
			if(i == 0) return 0;
			return_buffer[i] = '\0';
			rtn_text = &lpstrText[i+1];
			return rtn_text;

		default:
			return_buffer[i] = lpstrText[i];
			break;
		}
	}

	return 0;
}
/*----------------------------------------------------------------------------------------------*/
COwner* CGameEngine::GetHumanPlayer()
{
	const short index_human = 1;
	return this->m_OwnerList.GetItem(index_human);
}
/*----------------------------------------------------------------------------------------------*/
bool CGameEngine::Initialize()
{
	// initialize the data pool
	bool bResult = this->m_DataPool.Initialize();
	if(!bResult) {
		this->SetError("Error initializing DataPool. Be sure \"entities.pbd\" exists "
			"in the same directory as the executable.");
	}
	return bResult;
}
/*----------------------------------------------------------------------------------------------*/
bool CGameEngine::LoadGame(char* filename)
{
	char *buffer_moved = 0;	// used for the parsing of "comma spliced" strings
	unsigned int c = 0;	// generic counter

	// open the file
	CIniFile ini(filename);
	// be sure the file was opened successful
	// if not exit from function to eliminate future errors
	if(ini.IsFileValid() == false)
		return false;

	// delete all the players
	this->m_OwnerList.DeleteAllItems();

	// get/set the turn
	unsigned int turn = ini.GetInteger("Game", "turn", 0);
	this->SetTurn(turn);
	unsigned int player_count = ini.GetInteger("Game", "player_count", 0);
	// get/set each owner
	for(unsigned int p = 1 ; p <= player_count ; p++)
	{
		char owner_section[256];
		sprintf(owner_section, "Owner.%d", p);
		// create a new player, add him to the list
		COwner *pOwner = new COwner;
		this->AddPlayer(pOwner, false);
		// get/set owner name
		char owner_name[256];
		ini.GetString(owner_section, "name", "", owner_name, 255);
		pOwner->SetName(owner_name);
		// get/set owner budget (savings account)
		double owner_budget = ini.GetDouble(owner_section, "budget", 0.0f);
		pOwner->SetBudget(owner_budget);

		unsigned int restaurant_count = ini.GetInteger(owner_section, "restaurant_count", 0);
		// get/set each restaurant
		for(unsigned int r = 1 ; r <= restaurant_count ; r++)
		{
			// create the restaurant; add it to the owner's list
			CRestaurant *pRestaurant = new CRestaurant(pOwner);
			pOwner->AddRestaurant(pRestaurant);
			// set the identification number of restaurant
			pRestaurant->SetId(this->GetNextAvailableRestID());
			// initilize the ingredient list
			pRestaurant->InitIngredientList(&m_DataPool.m_IngList);
			// initialize the pizza list
			pRestaurant->InitPizzaList(&m_DataPool.m_PizzaList);

			// create an account at the bank
			// for the new restaurant
			pRestaurant->m_pBankAccount = this->m_Bank.CreateAccount(pRestaurant);

			/* -----------  get the data information ----------------------*/
			char data_section[256];
			sprintf(data_section, "Restaurant.%d.Data", r);
			
			// get/set the restaurant name
			char rest_name[256];
			ini.GetString(data_section, "name", "", rest_name, 255);
			pRestaurant->SetName(rest_name);

			// get/set the restaurant budget
			double budget = ini.GetDouble(data_section, "budget", 0);
			pRestaurant->SetBudget(budget);

			/* -----------  get the employee information ------------------*/
			char employee_section[256];
			sprintf(employee_section, "Restaurant.%d.Employees", r);
			unsigned int employee_count = ini.GetInteger(employee_section, "count", 0);
			for(c = 0 ; c < employee_count ; c++)
			{
				char key[256];
				sprintf(key, "%d", c);

				char emp_buffer[1024];
				ini.GetString(employee_section, key, "", emp_buffer, 1023);

				buffer_moved = emp_buffer;
				// get the data
				char name[256], age[4], comp[4], title[4], salary[8], days[8];
				buffer_moved = this->GetCommaLimit(buffer_moved, name);
				buffer_moved = this->GetCommaLimit(buffer_moved, age);
				buffer_moved = this->GetCommaLimit(buffer_moved, comp);
				buffer_moved = this->GetCommaLimit(buffer_moved, title);
				buffer_moved = this->GetCommaLimit(buffer_moved, salary);
				buffer_moved = this->GetCommaLimit(buffer_moved, days);
				// create a new employe object, set its data
				CEmployee *pEmployee = new CEmployee;
				pEmployee->SetName(name);
				pEmployee->SetAge(atoi(age));
				pEmployee->SetCompetency(atoi(comp));
				pEmployee->SetTitle((Emp_Title)atoi(title));
				pEmployee->SetSalary(atof(salary));
				pEmployee->SetDaysEmployeed(atoi(days));
				pEmployee->m_EmployedAt = pRestaurant;
				// add employee to the restaurant list and the datapool list
				m_DataPool.m_CanList.AddItem(pEmployee);
				pRestaurant->m_EmpList.AddItem(pEmployee);
			}

			/* -----------  get the advertisment information --------------*/
			char ads_section[256];
			sprintf(ads_section, "Restaurant.%d.Ads", r);
			for(c = 1 ; c <= m_DataPool.m_AdList.GetCount() ; c++)
			{
				CAdCategory *pCat = m_DataPool.m_AdList.GetItem(c);
				for(unsigned int i = 1 ; i <= pCat->AdList.GetCount() ; i++)
				{
					CAdvertisement *pAd = pCat->AdList.GetItem(i);

					char key[256];
					sprintf(key, "%d", pAd->GetId());

					char ads_buffer[1024];
					ini.GetString(ads_section, key, "", ads_buffer, 1023);

					buffer_moved = ads_buffer;
					while(1)
					{
						char str_ads_quantity[4];
						buffer_moved = this->GetCommaLimit(buffer_moved, str_ads_quantity);
						if(buffer_moved == 0)
							break;

						short ads_quantity = atoi(str_ads_quantity);
						
						pRestaurant->AddAdvertismentToList(pAd, ads_quantity);
					}
				}
			}

			/* -----------  get the furniture information -----------------*/
			char furniture_section[256];
			sprintf(furniture_section, "Restaurant.%d.Furniture", r);
			// get/set the ovens
			char oven_buffer[1024];
			ini.GetString(furniture_section, "ovens", "", oven_buffer, 1023);
			buffer_moved = oven_buffer;
			while(1)
			{
				char str_oven_id[4];
				buffer_moved = this->GetCommaLimit(buffer_moved, str_oven_id);
				if(buffer_moved == 0)
					break;

				short oven_id = atoi(str_oven_id);
				
				COven *pOven = m_DataPool.GetOvenByID(oven_id);
				if(pOven)
					pRestaurant->m_OvenList.AddItem(pOven);
			}

			// get/set the tables
			char table_buffer[1024];
			ini.GetString(furniture_section, "tables", "", table_buffer, 1023);
			buffer_moved = table_buffer;
			while(1)
			{
				char str_table_id[4];
				buffer_moved = this->GetCommaLimit(buffer_moved, str_table_id);
				if(buffer_moved == 0)
					break;

				short table_id = atoi(str_table_id);
				
				CTable *pTable = m_DataPool.GetTableByID(table_id);
				if(pTable)
					pRestaurant->m_TableList.AddItem(pTable);
			}

			// get/set the restaurant chair count
			unsigned int chairs = ini.GetInteger(furniture_section, "num_chairs", 0);
			pRestaurant->m_NumChairs = chairs;

			/* -----------  get the ingredients information ---------------*/
			char ingredient_section[256];
			sprintf(ingredient_section, "Restaurant.%d.Ingredients", r);
			// step through each category
			for(c = 1 ; c <= m_DataPool.m_IngList.GetCount() ; c++)
			{
				CIngCategory *pCat = m_DataPool.m_IngList.GetItem(c);
				// step through each ingredient in category
				for(unsigned int i = 1 ; i <= pCat->IngList.GetCount() ; i++)
				{
					CIngredient *pIngredient = pCat->IngList.GetItem(i);
					
					char key[256];
					sprintf(key, "%d", pIngredient->GetId());

					char ing_buffer[65535];
					ini.GetString(ingredient_section, key, "", ing_buffer, 1023);

					buffer_moved = ing_buffer;
					while(1)
					{
						char str_ing_quality[4];
						buffer_moved = this->GetCommaLimit(buffer_moved, str_ing_quality);
						if(buffer_moved == 0)
							break;

						short ing_quality = atoi(str_ing_quality);
						
						Ingredient *pIng = pRestaurant->AddIngredientToList(pIngredient, 1);
						// reset the quality because the function will set it to the default
						IngItem *pItem = pIng->IngList.GetItem(1);
						pItem->life_left = ing_quality;
					}
				}
			}
		} // end of restaurant from loop
	} // end of the owner loop

	bInSession = true;
	return true;
}
/*----------------------------------------------------------------------------------------------*/
void CGameEngine::RemoveRestaurant(CRestaurant *pRestaurant)
{
	for(unsigned int c = 1 ; c <= this->m_OwnerList.GetCount() ; c++)
	{
		COwner *pOwner = this->m_OwnerList.GetItem(c);
		
		for(unsigned int r = 1 ; r <= pOwner->m_StoreList.GetCount() ; r++)
		{
			CRestaurant *pRest = pOwner->m_StoreList.GetItem(r);
			if(pRest->GetId() == pRestaurant->GetId())
			{
				pOwner->m_StoreList.DeleteItem(r);
				return;
			}
		}
	}
}
/*----------------------------------------------------------------------------------------------*/
void CGameEngine::SaveGame(char* filename)
{
	int c = 0;	// generic counter
	// write out the ini file from top to the bottom
	ofstream outfile;
	outfile.open(filename);
	
	/* -----------  set the game information --------------------------*/
	outfile << "[Game]" << endl;
	// write the turn count
	outfile << "turn=" << this->GetTurn() << endl;
	// write the number of players
	outfile << "player_count=" << this->GetPlayerCount() << endl;

	// setup through each owner in the list and write the data
	// to a file in the "ini" file format
	for(unsigned int iOwner = 1 ; iOwner <= this->GetPlayerCount() ; iOwner++)
	{
		/* -----------  set the owner information ---------------------*/
		COwner *pOwner = this->GetPlayer(iOwner);
		outfile << endl << "[Owner." << iOwner << "]" << endl;
		// write the name
		char owner_name[256];
		pOwner->GetName(owner_name);
		outfile << "name=" << owner_name << endl;
		// write the budget
		outfile << "budget=" << pOwner->GetBudget() << endl;
		// write the restaurant count
		outfile << "restaurant_count=" << pOwner->GetRestaurantCount() << endl;

		/* -----------  set the information for each restaurant -------*/
		for(unsigned int r = 1 ; r <= pOwner->GetRestaurantCount() ; r++)
		{
			CRestaurant *pRestaurant = pOwner->GetRestaurant(r);
			
			/* -----------  set the data for each restaurant ----------*/
			outfile << endl << "[Restaurant." << r << ".Data]" <<  endl;
			// write the name of restaurant
			char rest_name[256];
			pRestaurant->GetName(rest_name);
			outfile << "name=" << rest_name << endl;
			// write the budget
			outfile << "budget=" << pRestaurant->GetBudget() << endl;
			// write the open state
			outfile << "shop_open=" << (pRestaurant->IsShopOpen()?"1":"0") << endl;

			/* -----------  set the staff for each restaurant --------*/
			outfile << endl << "[Restaurant." << r << ".Employees]" <<  endl;
			// write the employee count
			outfile << "count=" << pRestaurant->m_EmpList.GetCount() << endl;
			// write each employee to the file
			List<CEmployee> *pEmployeeList = &pRestaurant->m_EmpList;
			for(unsigned int c = 1 ; c <= pEmployeeList->GetCount() ; c++)
			{
				CEmployee *pEmployee = pEmployeeList->GetItem(c);
				outfile << c - 1 << endl;
				// write the employee name
				char emp_name[256];
				pEmployee->GetName(emp_name);
				outfile << emp_name << endl;
				// write the employee age
				outfile << "," << pEmployee->GetAge() << endl;
				// write the employee competency
				outfile << "," << pEmployee->GetCompetency() << endl;
				// write the employee salary
				outfile << "," << pEmployee->GetSalary() << endl;
				// write the employee # days worked
				outfile << "," << pEmployee->GetDaysEmployed() << endl;
			}

			/* -----------  set the ads for each restaurant -----------*/
			outfile << endl << "[Restaurant." << r << ".Ads]" <<  endl;
			// write the number of ads for each ad purchased
			for(c = 1 ; c <= pRestaurant->m_AdsList.GetCount() ; c++)
			{
				AdItem *pItem = pRestaurant->m_AdsList.GetItem(c);
				outfile << pItem->pAd->GetId() << "=" << pItem->quantity << endl;
			}

			/* ----------- write the furniture information -------------*/
			outfile << endl << "[Restaurant." << r << ".Furniture]" << endl;
			// write the ovens
			outfile << "ovens=";
			for(c = 1 ; c <= pRestaurant->m_OvenList.GetCount() ; c++)
			{
				COven *pOven = pRestaurant->m_OvenList.GetItem(c);
				outfile << pOven->GetID();

				// add a comma only if it is not the last item
				if(c != pRestaurant->m_OvenList.GetCount())
					outfile << ",";
			}
			outfile << endl;
			// write the tables
			outfile << "tables=";
			for(c = 1 ; c <= pRestaurant->m_TableList.GetCount() ; c++)
			{
				CTable *pTable = pRestaurant->m_TableList.GetItem(c);
				outfile << pTable->GetID();

				// add a comma only if it is not the last item
				if(c != pRestaurant->m_TableList.GetCount())
					outfile << ",";
			}
			outfile << endl;
			// write the chair count
			outfile << "num_chairs=" << pRestaurant->GetChairCount() << endl;

			/* ----------- write the ingredients information -----------*/
			outfile << endl << "[Restaurant." << r << ".Ingredients]" << endl;
			for(c = 1 ; c <= pRestaurant->m_IngList.GetCount() ; c++)
			{
				IngCat *pCat = pRestaurant->m_IngList.GetItem(c);
				for(unsigned int i = 1 ; i <= pCat->pCategory->IngList.GetCount() ; i++)
				{
					Ingredient *pIngredient = pCat->ingredients.GetItem(i);
					outfile << pIngredient->pIngredient->GetId() << "=";
					
					for(unsigned int ii = 1 ; ii <= pIngredient->IngList.GetCount() ; ii++)
					{
						IngItem *pItem = pIngredient->IngList.GetItem(ii);
						outfile << pItem->life_left;

						if(ii != pIngredient->IngList.GetCount())
							outfile << ",";
					}
					outfile << endl;
				}
			}

		} // end of restaurant for loop
	} // end of owner for loop

	// done, close the file
	outfile.close();
}
/*----------------------------------------------------------------------------------------------*/
void CGameEngine::SetError(char *message)
{
	this->error = true;
	strcpy(this->error_msg, message);
}
/*----------------------------------------------------------------------------------------------*/
void CGameEngine::StartNewGame()
{
	// there is always a player; create one
	// the human player is always index #1 in the list

	// delete all the players
	this->m_OwnerList.DeleteAllItems();

	// delete all accounts at the bank
	this->m_Bank.DeleteAllAccounts();

	// add one human player
	COwner *pOwner = new COwner;
	this->AddPlayer(pOwner);

	// reset the game turn
	this->SetTurn(0);

	bInSession = true;
}
/*----------------------------------------------------------------------------------------------*/
unsigned short CGameEngine::GetNextAvailableRestID()
{
	for(unsigned short c = 0 ; c < 256 ; c++)
	{
		if(this->RestIdAvailable(c) == true)
			return c;
	}

	return 0;
}
/*----------------------------------------------------------------------------------------------*/
bool CGameEngine::RestIdAvailable(unsigned short id)
{
	// iterate through all the players' restaurants
	for(unsigned short c = 1 ; c <= this->m_OwnerList.GetCount() ; c++)
	{
		COwner *pOwner = this->m_OwnerList.GetItem(c);
		for(unsigned short r = 1 ; r <= pOwner->GetRestaurantCount() ; r++)
		{
			CRestaurant *pRestaurant = pOwner->GetRestaurant(r);
			if(pRestaurant->GetId() != id)
				return true;
		}
	}
	return false;
}
/*----------------------------------------------------------------------------------------------*/