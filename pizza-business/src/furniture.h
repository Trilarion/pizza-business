#ifndef _JS_FURNITURE_H
#define _JS_FURNITURE_H
/*----------------------------------------------------------------------------------------------*/
#include <cstring>
/*----------------------------------------------------------------------------------------------*/
enum Furniture_Type { FT_MISC, FT_TABLE, FT_OVEN, FT_CHAIR };
/*----------------------------------------------------------------------------------------------*/
class CFurniture
{
public:
	short GetID() { return m_id; }
	void SetID(short id) { m_id = id; }

	void GetName(char* buffer) { strcpy(buffer, m_Name); }
	void SetName(char* name) { strcpy(m_Name, name); }

	float GetPrice() { return m_Price; }
	void SetPrice(float price) { m_Price = price; }

private:
	char m_Name[256];
	float m_Price;
	short m_id;
};
/*----------------------------------------------------------------------------------------------*/
class COven : public CFurniture
{
public:
	COven();

public: // functions
	short GetPizzaSupport() { return m_Pizza_support; }
	void SetPizzaSupport(short num_pizzas) { m_Pizza_support = num_pizzas; }

	short GetCookTime() { return m_Cooking_time; }
	void SetCookTime(short cook_time) { m_Cooking_time = cook_time; }

private: // data
	short m_Pizza_support;
	short m_Cooking_time;
};
/*----------------------------------------------------------------------------------------------*/
class CTable : public CFurniture
{
public:
	CTable();

public: // functions
	short GetChairsSupported() { return m_ChairSupport; }
	void SetChairsSupported(short num_chairs) { m_ChairSupport = num_chairs; }

private: // data
	short m_ChairSupport;
};
/*----------------------------------------------------------------------------------------------*/
#endif
