#include <iostream>
#include <fstream>
#include <cstring>
#include <cstdlib>
#include "conutils.h"
using namespace std;
/*----------------------------------------------------------------------------------------------*/
void Console_Clear()
{
	// i think this works
#ifdef UNIX
	system("clear");
#endif

#ifdef WIN32
	system("cls");
#endif
}
/*----------------------------------------------------------------------------------------------*/
void Console_HorzLine(int length)
{
	for(int c = 1 ; c <= length ; c++)
		cout << "-";
	cout << endl;
}
/*----------------------------------------------------------------------------------------------*/
void Console_NewLine()
{
	cout << endl;
}
/*----------------------------------------------------------------------------------------------*/
void PromptUser(char* prompt, char* buffer, int size)
{
	cout << prompt;

	strcpy(buffer, "");

	char input[256];
	cin.getline(input, 256);

	strncpy(buffer, input, size);
}
/*----------------------------------------------------------------------------------------------*/
