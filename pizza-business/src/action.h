#ifndef _JS_ACTION_H
#define _JS_ACTION_H
enum STATUS
{
	// Customer action states
	WAITING_TO_ORDER, ORDERING, WAITING_FOR_PIZZA, EATING_PIZZA, 
	// General employee action states
	NOT_BUSY,
	// Waiter action states
	TAKING_ORDER, TAKING_PIZZA_TO_CUST, 
	// Cook action states
	PREPARING_PIZZA
};
#endif
