/*
    Pizza Business 0.99 - A game in which you can create your own pizza business.
    Copyright (C) 2002 Tony Granberg                                 
                                          
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or   
    (at your option) any later version.                              
                                       
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of 
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
    GNU General Public License for more details.                 
                                                
    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software      
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/*-----------------------------------------------------------------------------------------------+
|																								 |
|	Win32 port: Pizza Business - early console version											 |
|	Start: May 22, 2002																			 |
|	By: Johnny Salazar, and...                                                                   |
|																								 |
+-----------------------------------------------------------------------------------------------*/
#define APP_NAME		"Pizza Business"
#define APP_VERSION		0.99
/*----------------------------------------------------------------------------------------------*/
#include <fstream>
#include "main.h"

#ifdef _WINDOWS
	#define WIN32_LEAN_AND_MEAN
	#include "win32\winiface.h"
#else
	#include "coniface.h"
#endif
/*----------------------------------------------------------------------------------------------*/
extern ofstream debugSim;										// declared in sim_main.cpp
/*----------------------------------------------------------------------------------------------*/
int InitEngine(CApplication &application, char* savedgame_filename)
{
	int run_result = 0;

	// initialize the game engine
	if(!application.m_Simulator.Initialize()) {
		char error_msg[256];
		application.m_Simulator.GetError(error_msg);
		application.m_LogFile.WriteError(error_msg);
		return -1;
	}

	// give the interface a pointer to the application
	if(application.m_pInterface) {
		application.m_pInterface->SetApplication(&application);
	}
	else
		return -1;

	// simulation debugging stuff
    #ifdef DEBUG_SIM
		debugSim.open("debug_sim.txt");
    #endif

	if(savedgame_filename != 0)
	{
		// log the action
		char text[2480];
		sprintf(text, "Loading new game: %s", savedgame_filename);
		application.m_LogFile.Write(text);
		// load the game
		application.m_Simulator.LoadGame(savedgame_filename);
	}

	if(application.m_pInterface)
	{
		application.m_LogFile.Write("Initiating interface.");
		application.m_LogFile.WriteLineBreak();
		run_result = application.m_pInterface->Run();					// start the interface
		application.m_LogFile.WriteLineBreak();
		application.m_LogFile.Write("Interface is done.");
	}
	else
		application.m_LogFile.WriteError("Unable to initialize user interface.");

	// simulation debugging stuff
    #ifdef DEBUG_SIM
		debugSim.close();
    #endif

	application.m_LogFile.Write("Application exited.");

	return run_result;
}
/*----------------------------------------------------------------------------------------------*/
#ifdef _WINDOWS
int PASCAL WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int nCmdShow)
{
	CApplication Application;
	Application.m_LogFile.Write("Application started.");

	bool loadgame = false;
	char loadgame_filename[256];
	
	// NOTE: the only arguments we are interested in
	// is a previously saved game
	if(strlen(lpszCmdLine) > 0)
	{
		loadgame = true;
		strcpy(loadgame_filename, lpszCmdLine);
	}

	// create the interface
	CWinInterface iface(hInstance);
	// set the global interface pointer
	Application.m_pInterface = &iface;
	
	int bResult = InitEngine(Application, loadgame ? loadgame_filename : 0);
	if(bResult != 0)
		MessageBox(0, "There was an error during the intialization of the game engine. Check \"corelog.log\""
			"in the game directory for the error message", "Error: Closing program", MB_OK);
	return bResult;
}
/*----------------------------------------------------------------------------------------------*/
#else
int main(int argc, char **argv)
{
	CApplication Application;
	Application.m_LogFile.Write("Application started.");

	bool loadgame = false;
	char loadgame_filename[256];

	// check parameter stuff
    if(argc >= 2) 
	{
        for(int i = 0 ; i <= argc - 1 ; i++) 
		{
			if(argv[i][0] == '-' || argv[i][0] == '/') // option found
			{
				char* arguement = &argv[i][1];

				if(strcmp(arguement, "h") == 0) 
				{

					cout << "Usage: pbgame [options] [filename]... \n";
					cout << "-V:                      This shows the version number\n";
					cout << "-h:                      Displays this help message\n";
					cout << "(C) Copyright 2001-2002 Tony Granberg. All rights reserved.\n";
					return 0;
				} 
				else if(strcmp(arguement, "V") == 0)
				{
					cout << APP_NAME << " "<< APP_VERSION << endl;
					return 0;
				}
				else
				{
					cout << "Invalid argument: " << argv[i] << endl;
					return 0;
				}
			}
			else // filename found
			{
				// let's prepare to load it
				strcpy(loadgame_filename, argv[i]);
				loadgame = true;
			}
        }
    }

	// create the interface
	CConInterface iface;
	// set the global interface pointer
	Application.m_pInterface = &iface;

	int bResult = InitEngine(Application, loadgame ? loadgame_filename : 0);
	if(bResult != 0)
		cout << "There was an error during the intialization of the game engine." << endl
			 << "Check \"corelog.log\" in the game directory for the error message." << endl;
	return bResult;
}
/*----------------------------------------------------------------------------------------------*/
#endif

