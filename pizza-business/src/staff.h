#ifndef _JS_STAFF_H
#define _JS_STAFF_H
/*----------------------------------------------------------------------------------------------*/
#include <vector>
/*----------------------------------------------------------------------------------------------*/
// NOTE: placement of the enumeration values need to stay in this order to insure that
// the proper calculations are made for the salary
enum Emp_Title { TITLE_UNEMPLOYED, TITLE_WAITER, TITLE_CHEF, TITLE_MANAGER, TITLE_IRRELEVANT };
/*----------------------------------------------------------------------------------------------*/
// NOTE: we can not include "sim_main.h" because that file includes "restdata.h" and that file
// includes this file. so we must declare SimChair here. don't believe me, try it.
class SimChair;	// declared and defined in sim_main.*
class CRestaurant;
/*----------------------------------------------------------------------------------------------*/
// chef stats id (0-9)
#define PIZZAS_COOKED	0
// waiter stats id (10-19)
#define PIZZAS_ORDERED	10
#define PIZZAS_SERVED	11
// manager stats id (20-?)
#define STAFF_HIRE		20
#define STAFF_FIRE		22
#define INGRED_EMPTY	23
/*----------------------------------------------------------------------------------------------*/
class CChef
{
public: // construction/destruction
	CChef() {}
	~CChef() {}

public: // functions
	virtual bool ClearStatistics();
	virtual unsigned int GetStats(int id);
	virtual bool SetStats(int id, unsigned int statistic);
	
private: // data
	struct tagSTATS {
		unsigned int pizzas_cooked;
	}stats;
};
/*----------------------------------------------------------------------------------------------*/
class CManager
{
public: // construction/destruction
	CManager() {}
	~CManager() {}

private: // data
	struct tagSTATS {
		
	}stats;
};
/*----------------------------------------------------------------------------------------------*/
class CWaiter
{
public: // construction/destruction
	CWaiter() {}
	~CWaiter() {}

public: // functions
	virtual bool ClearStatistics();
	virtual unsigned int GetStats(int id);
	virtual bool SetStats(int id, unsigned int statistic);

	std::vector<SimChair>::iterator GetCurrentCustomer() { return m_CurrentCustomer; }
	void SetCurrentCustomer(std::vector<SimChair>::iterator customer) { m_CurrentCustomer = customer; }

private: // data
	std::vector<SimChair>::iterator m_CurrentCustomer;

	struct tagSTATS {
		unsigned int pizzas_served;
		unsigned int pizzas_ordered;
	}stats;
};
/*----------------------------------------------------------------------------------------------*/
// NOTE: Each employee has access to the data and functions of each job position.
// To ensure that he has 'appropriate access' to the functions is determined by
// the title which is held in the 'm_Title' member variable.
class CEmployee : public CWaiter, public CManager, public CChef
{
public: // construction/destruction
	CEmployee();
	~CEmployee() {}

public: // data access
	short GetAge() { return m_Age; }
	void SetAge(short age) { m_Age = age; }

	int GetCompetency() { return m_Competency; }
	void SetCompetency(int competency) { m_Competency = competency; }

	unsigned int GetDaysEmployed() { return m_Days_employed; }
	void SetDaysEmployeed(unsigned int num_days) { m_Days_employed = num_days; }

	void GetDescription(char *buffer) { strcpy(buffer, m_Description); }
	void SetDescription(char *desc) { strcpy(m_Description, desc); }

	void GetName(char *buffer) { strcpy(buffer, m_Name); }
	void SetName(char *name) { strcpy(m_Name, name); }

	float GetSalary() { return m_Salary; }
	void SetSalary(float salary) { m_Salary = salary; }

	bool ClearStatistics();
	unsigned int GetStatistic(int id);
	bool SetStatistic(int id, unsigned int statistic);

	Emp_Title GetTitle() { return m_Title; }
	void SetTitle(Emp_Title title) { m_Title = title; }

public:
	CRestaurant *m_EmployedAt;

protected: // data
	// attributes
	char m_Name[256];
	float m_Salary;
	Emp_Title m_Title;
	short m_Age;
	char m_Description[1024];
	int m_Competency;
	// personel stats
	unsigned int m_Days_employed;
};
/*----------------------------------------------------------------------------------------------*/
#endif