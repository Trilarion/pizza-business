# Microsoft Developer Studio Project File - Name="pb_win32" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Application" 0x0101

CFG=pb_win32 - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "pb_win32.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "pb_win32.mak" CFG="pb_win32 - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "pb_win32 - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE "pb_win32 - Win32 Debug" (based on "Win32 (x86) Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "pb_win32 - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /YX /FD /c
# ADD CPP /nologo /W3 /GX /O2 /I ".\src\win32" /I ".\src\win32\audio" /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /FD /c
# SUBTRACT CPP /YX
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "NDEBUG"
# ADD RSC /l 0x409 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:windows /machine:I386
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib comctl32.lib ogg_static.lib vorbis_static.lib vorbisfile_static.lib winmm.lib /nologo /subsystem:windows /machine:I386 /out:"Bin/pbgame.exe" /libpath:".\lib\win32"

!ELSEIF  "$(CFG)" == "pb_win32 - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "pb_win32___Win32_Debug"
# PROP BASE Intermediate_Dir "pb_win32___Win32_Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Win32_Debug"
# PROP Intermediate_Dir "Win32_Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /YX /FD /GZ /c
# ADD CPP /nologo /W3 /Gm /GX /ZI /Od /I ".\src\win32" /I ".\src\win32\audio" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /FD /GZ /c
# SUBTRACT CPP /YX
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "_DEBUG"
# ADD RSC /l 0x409 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib comctl32.lib ogg_static.lib vorbis_static.lib vorbisfile_static.lib winmm.lib /nologo /subsystem:windows /debug /machine:I386 /out:"Bin/pbgame.exe" /pdbtype:sept /libpath:".\lib\win32"

!ENDIF 

# Begin Target

# Name "pb_win32 - Win32 Release"
# Name "pb_win32 - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Group "GUI Source"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\src\win32\gui\hoverbtn.cpp
# End Source File
# Begin Source File

SOURCE=.\src\win32\gui\mainwnd.cpp
# End Source File
# Begin Source File

SOURCE=.\src\win32\gui\menu.cpp
# End Source File
# Begin Source File

SOURCE=.\src\win32\gui\menuwnd.cpp
# End Source File
# Begin Source File

SOURCE=.\src\win32\gui\telnet.cpp
# End Source File
# Begin Source File

SOURCE=.\src\win32\gui\window.cpp
# End Source File
# Begin Source File

SOURCE=.\src\win32\gui\wnd_bank.cpp
# End Source File
# Begin Source File

SOURCE=.\src\win32\gui\wnd_buy.cpp
# End Source File
# Begin Source File

SOURCE=.\src\win32\gui\wnd_pizza.cpp
# End Source File
# Begin Source File

SOURCE=.\src\win32\gui\wnd_staff.cpp
# End Source File
# Begin Source File

SOURCE=.\src\win32\gui\wnd_stats.cpp
# End Source File
# End Group
# Begin Group "Audio Source"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\src\win32\audio\oggplayer.cpp
# End Source File
# End Group
# Begin Source File

SOURCE=.\src\bank.cpp
# End Source File
# Begin Source File

SOURCE=.\src\city.cpp
# End Source File
# Begin Source File

SOURCE=.\src\data.cpp
# End Source File
# Begin Source File

SOURCE=.\src\datapool.cpp
# End Source File
# Begin Source File

SOURCE=.\src\datarest.cpp
# End Source File
# Begin Source File

SOURCE=.\src\furniture.cpp
# End Source File
# Begin Source File

SOURCE=.\src\inifiles.cpp
# End Source File
# Begin Source File

SOURCE=.\src\interface.cpp
# End Source File
# Begin Source File

SOURCE=.\src\logger.cpp
# End Source File
# Begin Source File

SOURCE=.\src\main.cpp
# End Source File
# Begin Source File

SOURCE=.\res\pb_win32.rc
# End Source File
# Begin Source File

SOURCE=.\src\pbengine.cpp
# End Source File
# Begin Source File

SOURCE=.\src\pizzas.cpp
# End Source File
# Begin Source File

SOURCE=.\src\region.cpp
# End Source File
# Begin Source File

SOURCE=.\src\sim_main.cpp
# End Source File
# Begin Source File

SOURCE=.\src\staff.cpp
# End Source File
# Begin Source File

SOURCE=.\src\win32\winiface.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Group "GUI Headers"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\src\win32\gui\hoverbtn.h
# End Source File
# Begin Source File

SOURCE=.\src\win32\gui\mainwnd.h
# End Source File
# Begin Source File

SOURCE=.\src\win32\gui\menu.h
# End Source File
# Begin Source File

SOURCE=.\src\win32\gui\menuwnd.h
# End Source File
# Begin Source File

SOURCE=.\src\win32\gui\telnet.h
# End Source File
# Begin Source File

SOURCE=.\src\win32\gui\window.h
# End Source File
# Begin Source File

SOURCE=.\src\win32\gui\wnd_bank.h
# End Source File
# Begin Source File

SOURCE=.\src\win32\gui\wnd_buy.h
# End Source File
# Begin Source File

SOURCE=.\src\win32\gui\wnd_pizza.h
# End Source File
# Begin Source File

SOURCE=.\src\win32\gui\wnd_staff.h
# End Source File
# Begin Source File

SOURCE=.\src\win32\gui\wnd_stats.h
# End Source File
# End Group
# Begin Group "Audio Headers"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\src\win32\audio\oggplayer.h
# End Source File
# End Group
# Begin Source File

SOURCE=.\src\action.h
# End Source File
# Begin Source File

SOURCE=.\src\bank.h
# End Source File
# Begin Source File

SOURCE=.\src\buysell.h
# End Source File
# Begin Source File

SOURCE=.\src\city.h
# End Source File
# Begin Source File

SOURCE=.\src\data.h
# End Source File
# Begin Source File

SOURCE=.\src\datapool.h
# End Source File
# Begin Source File

SOURCE=.\src\datarest.h
# End Source File
# Begin Source File

SOURCE=.\src\furniture.h
# End Source File
# Begin Source File

SOURCE=.\src\gfxiface.h
# End Source File
# Begin Source File

SOURCE=.\src\inifiles.h
# End Source File
# Begin Source File

SOURCE=.\src\interface.h
# End Source File
# Begin Source File

SOURCE=.\src\list.h
# End Source File
# Begin Source File

SOURCE=.\src\logger.h
# End Source File
# Begin Source File

SOURCE=.\src\main.h
# End Source File
# Begin Source File

SOURCE=.\src\pbengine.h
# End Source File
# Begin Source File

SOURCE=.\src\pizzas.h
# End Source File
# Begin Source File

SOURCE=.\src\region.h
# End Source File
# Begin Source File

SOURCE=.\src\sim_main.h
# End Source File
# Begin Source File

SOURCE=.\src\staff.h
# End Source File
# Begin Source File

SOURCE=.\src\win32\winiface.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# Begin Source File

SOURCE=.\res\molah.ico
# End Source File
# End Group
# Begin Source File

SOURCE=.\notes.txt
# End Source File
# End Target
# End Project
