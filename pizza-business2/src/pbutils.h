#ifndef _JS_PBUTILS_H_
#define _JS_PBUTILS_H_
/*-------------------------------------------------------------------------------------------------------------*/
#include <string>
#include <sstream>
#include <iomanip>
/*-------------------------------------------------------------------------------------------------------------*/
/// \file pbutils.h
/// Contains various utility functions
/*-------------------------------------------------------------------------------------------------------------*/
/// Converts an object of type T to a std::string
/// \param item An object of type T which will be converted to a string
/// \return The converted item as a std::string
template <typename T>
std::string ToString(T item)
{
	std::stringstream myStream;

	myStream << item;

	std::string myStr = myStream.str();
	return myStr;
}
/*-------------------------------------------------------------------------------------------------------------*/
/// Converts an object of type T to an std::string of type T with the precision specified
/// \param item An object of type T which will be converted to a string
/// \param precision A unsigned integer which specifies the precision with which to convert
/// \return The converted item as a string
/// \note Usage should be as follows
/// \code 
/// double a = 5.678;
/// myString = ToString(a, 2);
/// // A now equals 5.68 \endcode
template <typename T>
std::string ToString(T item, unsigned short precision)
{
	std::stringstream myStream;

	myStream << std::setiosflags(std::ios::fixed) << std::setprecision(precision) << item;

	std::string myStr = myStream.str();

	return myStr;
}
/*-------------------------------------------------------------------------------------------------------------*/
// Converts a value of any type T to a wide string.  Usage is same as for ToString(T item)
/*template <typename T>
std::wstring ToWString(T item)
{
	std::wstringstream myStream;

	myStream << item;

	std::wstring myStr = myStream.str();

	return myStr;
}*/
/*-------------------------------------------------------------------------------------------------------------*/
// Converts a value of any type T to a wide string, with <precision> places after decimal pt.
// Usage is same as for ToString(T item, unsigned short precision)
/*template <typename T>
std::wstring ToWString(T item, unsigned short precicion)
{
	std::wstringstream myStream;

	myStream << std::setiosflags(std::ios::fixed) << std::setprecision(precision) << item;

	std::wstring myStr = myStream.str();

	return myStr;
}*/
/*-------------------------------------------------------------------------------------------------------------*/
/// Converts a std::string to any type t
/// \param input The string that is to be converted
/// \return Returns an object of type T that has been converted from the string
/// \note Usage should be as follows
/// \code
/// myType a = StringTo<myType>(myStringToConvert) \endcode
template <typename T>
T StringTo(const std::string &input)
{
	std::stringstream myStream;
	T output;

	myStream << input;

	myStream >> output;

	return output;
}
/*-------------------------------------------------------------------------------------------------------------*/
// Converts a wide string to any type T
// Usage of the function must be as follows
// myType a = WStringTo<myType>(myWStringToConvert)
/*template <typename T>
T WStringTo(const std::wstring &input)
{
	std::wstringstream myStream;
	T output;

	myStream << input;

	myStream >> output;

	return output;
}*/
/*-------------------------------------------------------------------------------------------------------------*/
#endif


