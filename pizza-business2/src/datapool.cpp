#include <fstream>
#include <cstdlib>
#include <cstdio>
#include <ctime>
#include "datapool.h"
using namespace std;
/*----------------------------------------------------------------------------------------------*/
short GenerateRandomNumber(short min, short max)
{
	if(min == max)
		return min;

	return (rand() % (max - min -1)) + min + 1;
}
/*-----------------------------------------------------------------------------------------------+
|																								 |
|	CRecruiter implementation																	 |
|																								 |
/*----------------------------------------------------------------------------------------------*/
void CDataPool::CreateIngredients(List<IngCategory> *pIngCatList)
{
	for(unsigned int c = 1 ; c <= pIngCatList->GetCount() ; c++)
	{
		IngCategory *pCat = pIngCatList->GetItem(c);
		
		// create a new ingredient category; set name; add to list
		CIngCategory *pNewCat = new CIngCategory;
		this->m_IngList.AddItem(pNewCat);
		pNewCat->SetName(pCat->name);

		// add ingredients to list
		for(unsigned int i = 1 ; i <= pCat->Ingredients.GetCount() ; i++)
		{
			IngData *pData = pCat->Ingredients.GetItem(i);

			// create new ingredient;
			CIngredient *pNewIng = new CIngredient(pNewCat);
			
			// set the data
			pNewIng->SetName(pData->name);
			pNewIng->SetPrice(pData->price);
			pNewIng->SetQuality(pData->quality);
			pNewIng->SetVenderID(pData->vender_id);
			pNewIng->SetWeight(pData->weight);
			pNewIng->SetId(pData->ingredient_id);

			// add it to the list
			pNewCat->IngList.AddItem(pNewIng);
		}
	}
}
/*----------------------------------------------------------------------------------------------*/
void CDataPool::CreateOvens(List<OvenData> *pOvenList)
{
	for(unsigned int c = 1 ; c <= pOvenList->GetCount() ; c++)
	{
		OvenData *pData = pOvenList->GetItem(c);

		// create new oven; add it to list
		COven *pOven = new COven;
		this->m_OvenList.AddItem(pOven);

		// set oven data
		pOven->SetName(pData->name);
		pOven->SetPrice(pData->price);
		pOven->SetPizzaSupport(pData->pizza_support);
		pOven->SetCookTime(pData->cooking_time);
		pOven->SetID(pData->oven_id);
	}
}
/*----------------------------------------------------------------------------------------------*/
void CDataPool::CreatePizzas(List<RecipeData> *pPizzaList)
{
	for(unsigned int c = 1 ; c <= pPizzaList->GetCount() ; c++)
	{
		// get the pizza
		RecipeData *pPizzaData = pPizzaList->GetItem(c);

		// create a CPizza object, add it to the list
		CPizza *pPizza = new CPizza;
		this->m_PizzaList.AddItem(pPizza);
		pPizza->SetName(pPizzaData->name);

		// add the ingredients to the pizza
		for(unsigned int i = 1 ; i <= pPizzaData->Ingredients.GetCount() ; i++)
		{
			short* pizzaID = pPizzaData->Ingredients.GetItem(i);
		
			CIngredient *pIngredient = this->GetIngredientByID(*pizzaID);
			pPizza->AddIngredient(pIngredient);
		}
	}
}
/*----------------------------------------------------------------------------------------------*/
void CDataPool::CreateTables(List<TableData> *pTableList)
{
	for(unsigned int c = 1 ; c <= pTableList->GetCount() ; c++)
	{
		// create the new table; add it to list
		CTable *pTable = new CTable;
		this->m_TableList.AddItem(pTable);

		// set table data
		TableData *pData = pTableList->GetItem(c);
		pTable->SetName(pData->name);
		pTable->SetChairsSupported(pData->chair_support);
		pTable->SetPrice(pData->price);
		pTable->SetID(pData->table_id);
	}
}
/*----------------------------------------------------------------------------------------------*/
CIngredient* CDataPool::GetIngredientByID(short id)
{
	// search through each category to look for an ingredient with
	// a matching ingredient ID
	// return a pointer to the CIngredient object if found or return NULL

	for(unsigned int c = 1 ; c <= this->m_IngList.GetCount() ; c++)
	{
		CIngCategory *pCat = this->m_IngList.GetItem(c);

		for(unsigned int i = 1 ; i <= pCat->IngList.GetCount() ; i++)
		{
			CIngredient *pIngredient = pCat->IngList.GetItem(i);	
			if(pIngredient)
			{
				if(pIngredient->GetId() == id)
					return pIngredient;
			}
		}
	}
	
	return 0;
}
/*----------------------------------------------------------------------------------------------*/
COven* CDataPool::GetOvenByID(short id)
{
	for(unsigned int c = 1 ; c <= this->m_OvenList.GetCount() ; c++)
	{
		COven *pOven = this->m_OvenList.GetItem(c);
		if(pOven->GetID() == id) return pOven;
	}

	return 0;
}
/*----------------------------------------------------------------------------------------------*/
CTable* CDataPool::GetTableByID(short id)
{
	for(unsigned int c = 1 ; c <= this->m_TableList.GetCount() ; c++)
	{
		CTable *pTable = this->m_TableList.GetItem(c);
		if(pTable->GetID() == id) return pTable;
	}

	return 0;
}
/*----------------------------------------------------------------------------------------------*/
bool CDataPool::Initialize()
{
	// load the PB_DATA from the file
	PBD_DATA pbd;
	pbd.dwFlags = DATA_ALL;
	if(!Entities_LoadFile("entities.pbd", pbd))
		return false;

	// create the employees
	this->CreateEmployees(pbd, 20);

	// create the ovens
	this->CreateOvens(&pbd.OvenList);

	// create the tables
	this->CreateTables(&pbd.TableList);

	// create the advertisements
	this->CreateAdvertisements(&pbd.AdCatList);

	// create the ingredients
	this->CreateIngredients(&pbd.IngCatList);

	// create the pizza recipes
	this->CreatePizzas(&pbd.RecipeList);

	// set the price of the chairs
	this->m_Chair_price = pbd.Chair.price;

	// set the min/max competency value
	this->m_Max_competency = this->CalculateCompetencyBonus(pbd.Competency.max, pbd.Age.max);
	this->m_Min_competency = this->CalculateCompetencyBonus(pbd.Competency.min, pbd.Age.min);

	return true;
}
/*-----------------------------------------------------------------------------------------------+
|																								 |
|	CMarketer implementation																	 |
|																								 |
+-----------------------------------------------------------------------------------------------*/
CMarketer::CMarketer()
{
	
}
/*----------------------------------------------------------------------------------------------*/
void CMarketer::CreateAdvertisements(List<AdCategory> *pAdCat)
{
	for(unsigned int c = 1 ; c <= pAdCat->GetCount() ; c++)
	{
		AdCategory *pCat = pAdCat->GetItem(c);
		// add the category to the list
		CAdCategory *pNewCat = new CAdCategory;
		this->m_AdList.AddItem(pNewCat);
		// set the name
		pNewCat->SetName(pCat->name);

		// add all the ads to the new category
		for(unsigned int i = 1 ; i <= pCat->Ads.GetCount() ; i++)
		{
			AdData *pData = pCat->Ads.GetItem(i);

			// create new ad; add it to the category's list
			CAdvertisement *pAd = new CAdvertisement;
			pAd->SetDescription(pData->name);
			pAd->SetPrice(pData->price);
			pAd->SetFactor(pData->factor);
			pAd->SetId(pData->ad_id);

			pNewCat->AdList.AddItem(pAd);
		}
	}
}
/*----------------------------------------------------------------------------------------------*/
CAdvertisement* CMarketer::GetAdvertisementByID(short id)
{
	unsigned int cat_count = m_AdList.GetCount();
	for(unsigned int c = 1 ; c <= cat_count ; c++)
	{
		CAdCategory *pAdCat = m_AdList.GetItem(c);
		unsigned int item_count = pAdCat->AdList.GetCount();
		for(unsigned int i = 1 ; i <= item_count ; i++)
		{
			CAdvertisement *pAd = pAdCat->AdList.GetItem(i);
			if(pAd->GetId() == id)
				return pAd;
		}
	}

	return 0;
}
/*-----------------------------------------------------------------------------------------------+
|																								 |
|	CRecruiter implementation																	 |
|																								 |
+-----------------------------------------------------------------------------------------------*/
CRecruiter::CRecruiter()
{
	
}
/*----------------------------------------------------------------------------------------------*/
CRecruiter::~CRecruiter()
{

}
/*----------------------------------------------------------------------------------------------*/
short CRecruiter::CalculateCompetencyBonus(short initial_competency, short age)
{
	const float relief = 0.1f;
	const short age_multi = 2;

	return initial_competency + relief * (age_multi * age);
}
/*----------------------------------------------------------------------------------------------*/
void CRecruiter::CreateEmployees(PBD_DATA & pbd, short max_people)
{
	srand(time(0));

	// create specified number of employees
	for(short c = 0 ; c < max_people ; c++)
	{
		char fname[256], lname[256], fullname[256];
		CEmployee *pEmployee = new CEmployee;
		this->m_CanList.AddItem(pEmployee);

		int index = 0;
		// let's get a firstname
		index = GenerateRandomNumber(1, pbd.FirstnameList.GetCount());
		strcpy(fname, ((CharName*)pbd.FirstnameList.GetItem(index))->name);
		// let's get a lastname
		index = GenerateRandomNumber(1, pbd.LastnameList.GetCount());
		strcpy(lname, ((CharName*)pbd.LastnameList.GetItem(index))->name);
		// combine names and set employee name in structure
		sprintf(fullname, "%s %s", fname, lname);
		pEmployee->SetName(fullname);

		// let's get a description
		index = GenerateRandomNumber(1, pbd.DescList.GetCount());
		CharDesc *pDesc = pbd.DescList.GetItem(index);
		strcpy(lname, pDesc->name);
		
		// let's get an age
		short age = GenerateRandomNumber(pbd.Age.min, pbd.Age.max);
		pEmployee->SetAge(age);

		// let's calculate a competency; also caluculate the age bonus
		short competency = GenerateRandomNumber(pbd.Competency.min, pbd.Competency.max);
		short total_competency = this->CalculateCompetencyBonus(competency, age);
		pEmployee->SetCompetency(total_competency);
	}
}
/*----------------------------------------------------------------------------------------------*/


