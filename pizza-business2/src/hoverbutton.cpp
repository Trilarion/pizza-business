#include "hoverbutton.h"
#include <wx/event.h>
#include <wx/wx.h>
#include <wx/dcclient.h>
/*###########################################################################*/
//
//	CHoverButton Implementation
//
/*###########################################################################*/
BEGIN_EVENT_TABLE(CHoverButton, wxControl)
	EVT_LEFT_DOWN(CHoverButton::OnPressed)
	EVT_LEFT_UP(CHoverButton::OnReleased)
	EVT_ENTER_WINDOW(CHoverButton::OnHover)
	EVT_LEAVE_WINDOW(CHoverButton::OnEndHover)
	EVT_PAINT(CHoverButton::OnPaint)
END_EVENT_TABLE()
DEFINE_EVENT_TYPE(wxREPAINT_BUTTONS)
/*------------------------------------------------------------------------*/
CHoverButton::CHoverButton(wxWindow *buttonParent, wxWindowID buttonID, const wxString& label, 
				const wxPoint& pos, const wxSize& size, long style, 
				const wxValidator& validator, const wxString& name) : 
wxControl(buttonParent, buttonID,/* label,*/ pos, size, wxNO_BORDER , validator, name)
{
	isHovered = false;
	isPressed = false;
}
/*------------------------------------------------------------------------*/
CHoverButton::~CHoverButton()
{
}
/*------------------------------------------------------------------------*/
bool CHoverButton::IsHovered()
{
	return isHovered;
}
/*------------------------------------------------------------------------*/
void CHoverButton::SetHovered(bool hovered)
{
	isHovered = hovered;
}
/*------------------------------------------------------------------------*/
void CHoverButton::OnHover(wxMouseEvent &e)
{
	// If the left mouse button is down as the cursor hovers over the button,
	// ... then the button should be put into its pressed state.  This is so
	// ... if the user pressed down on the button, moved the cursor off of 
	// ... the button (while still holding the left mouse button), and then 
	// ... moved the cursor back over the button, the proper behavior will occur.
	if (e.LeftIsDown())
	{
		OnPressed(e);
	}
	// Otherwise, it's just a normal hover
	else
		SetHovered(true);

	this->Refresh(false);
}
/*------------------------------------------------------------------------*/
void CHoverButton::OnEndHover(wxMouseEvent &e)
{
	SetHovered(false);
	SetPressed(false);	// This is so if the user moves the cursor off the button
						// ...while holding the left mouse button down, the pressed
						// ... state is canceled.
	this->Refresh(false);
}
/*------------------------------------------------------------------------*/
void CHoverButton::OnPressed(wxMouseEvent &e)
{
	SetPressed(true);
	this->Refresh(false);
	e.Skip();	// This is needed so that the parent window will be able to receive
				// ... a click event.  If the event is not "skipped" a click event
				// ... will never be generated.
}
/*------------------------------------------------------------------------*/
void CHoverButton::OnReleased(wxMouseEvent &e)
{
	SetPressed(false);
	this->Refresh(false);
	int id = this->GetId();
	wxCommandEvent event(wxEVT_COMMAND_BUTTON_CLICKED, id);
	//event.SetId(this->GetId());
	ProcessEvent(event);
	e.Skip();	// This is needed so that the parent window will be able to receive
				// ... a click event.  If the event is not "skipped" a click event
				// ... will never be generated.
}
/*------------------------------------------------------------------------*/
void CHoverButton::SetPressed(bool pressed)
{
	isPressed = pressed;
}
/*------------------------------------------------------------------------*/
bool CHoverButton::IsPressed()
{
	return isPressed;
}
/*------------------------------------------------------------------------*/
void CHoverButton::OnPaint(wxPaintEvent &e)
{
	wxPaintDC myPaint(this);

	wxCommandEvent eventCustom(wxREPAINT_BUTTONS);
	eventCustom.SetId(this->GetId());
    wxPostEvent(this->GetParent(), eventCustom);
	//e.skip();	// Necessary so that this button's parent window can take care of painting
}
/*------------------------------------------------------------------------*/



