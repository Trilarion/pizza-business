#include <fstream>
#include <string>
#include "logger.h"

using namespace std;
/*----------------------------------------------------------------------------------------------*/
CLogFile::CLogFile()
{
	this->SetFilename("");
	this->EnableLogging(true);
}
/*----------------------------------------------------------------------------------------------*/
CLogFile::CLogFile(char* filename, bool bClear)
{
	this->SetFilename(filename);
	this->EnableLogging(true);

	if(bClear == true)
		this->Clear();
}
/*----------------------------------------------------------------------------------------------*/
void CLogFile::Clear()
{
	// trunc old log file details
	// just open and close file
	ofstream outfile;
	outfile.open(this->m_Filename, ios::out|ios::trunc);
	outfile.close();
}
/*----------------------------------------------------------------------------------------------*/
void CLogFile::SetFilename(char* filename)
{
	strcpy(this->m_Filename, filename);
}
/*----------------------------------------------------------------------------------------------*/
void CLogFile::Write(char* text)
{
	// open the file, append the text, and close the file
	ofstream outfile;
	outfile.open(this->m_Filename, ios::out|ios::app);
	outfile << text << endl;
	outfile.close();
}
/*----------------------------------------------------------------------------------------------*/
void CLogFile::WriteError(char* text)
{
	ofstream outfile;
	outfile.open(this->m_Filename, ios::out|ios::app);
	outfile << "ERROR: " << text << endl;
	outfile.close();
}
/*----------------------------------------------------------------------------------------------*/
void CLogFile::WriteLineBreak()
{
	const short dash_count = 60;

	ofstream outfile;
	outfile.open(this->m_Filename, ios::out|ios::app);

	for(short c = 0 ; c < dash_count ; c++)
		outfile << "-";
	outfile << endl;
	
	outfile.close();
}
/*----------------------------------------------------------------------------------------------*/



