#ifndef _JS_GRAPH_H
#define _JS_GRAPH_H
/*----------------------------------------------------------------------------------------------*/
#include <wx/gdicmn.h>
#include <wx/dc.h>
#include <wx/event.h>
#include <wx/window.h>
#undef new
#include <vector>
#include <string>
#include "datarest.h"
/*----------------------------------------------------------------------------------------------*/
/// Line Graph coordinate structure
struct lgCoord
{
public:
	lgCoord() 
	{
		this->x = 0;
		this->actX = 0;
		this->y = 0;
	}
	lgCoord(double myX, double myY) 
	{
		this->actX = myX;
		this->y = myY;
	}
	double x;
	double actX;
	double y;
};
/*----------------------------------------------------------------------------------------------*/
class Graph
{
public:
	Graph();

public:
	void SetGraphHeight(int height);
	void SetGraphWidth(int width);
	void SetGraphPosition(wxPoint pos);
	void SetPadding(int top, int bottom, int left, int right);
	void SetXAxisLabel(std::string label);
	void SetYAxisLabel(std::string label);

	virtual void DrawGraph(wxDC &) = 0;

protected:
	int graphPixelWidth;
	int graphPixelHeight;
	wxPoint graphPixelPosition;
	int paddingBottom;
	int paddingTop;
	int paddingRight;
	int paddingLeft;
	std::string xLabel;
	std::string yLabel;
};
/*----------------------------------------------------------------------------------------------*/
class LineGraph : public Graph
{
public:
	LineGraph();
	void DrawGraph(wxDC &graphDC);
	void SetGraphPoints(std::vector<lgCoord> points);
    int GetNumPoints();

private:
	void CalcMaxXValue();
	void CalcMinXValue();
	void CalcMaxYValue();
	void CalcMinYValue();
	void RelativizeXValues();
	bool ContainsNegativeYValues();
	bool ContainsPositiveYValues();
	std::vector<lgCoord> graphPoints;
	double maxYValue;
	double minYValue;
	int maxXValue;
	int minXValue;
};
/*----------------------------------------------------------------------------------------------*/
#endif


