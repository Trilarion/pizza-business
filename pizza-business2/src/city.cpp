#include "city.h"
/*-----------------------------------------------------------------------------------------------+
|																								 |
|	CCity implementation																	 |
|																								 |
+-----------------------------------------------------------------------------------------------*/
void CCity::AddRegion(CRegion *newRegion)
{
	m_regions.AddItem(newRegion);
	data.num_regions++;
}
CRegion* CCity::GetRegionOfRestaurant(int restaurantID)
{
	bool regionFound = false;
	CRegion* tempRegion;
	for (unsigned int i = 1; i <= m_regions.GetCount() && regionFound == false; i++)
	{
		for (unsigned int j = 1; j <= m_regions.GetItem(i)->m_restaurants.GetCount(); j++)
		{
			if ((*m_regions.GetItem(i)->m_restaurants.GetItem(j)) == (unsigned short)restaurantID)
			{
				tempRegion = m_regions.GetItem(i);
				regionFound = true;
				break;
			}
		}
	}
	if (regionFound == true)
		return tempRegion;
	else
	// --temp--
	return 0;
	// --end--
}
bool CCity::LoadRegions(char *filename)
{
	// Johnny will have to code this
	
	// --temp--
	return false;
	// --end--
}

