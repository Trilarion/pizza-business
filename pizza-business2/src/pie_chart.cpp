#include "pie_chart.h"
#include <cmath>
/*----------------------------------------------------------------------------------------------*/
#define PI		3.14
/*----------------------------------------------------------------------------------------------*/
CPieChart::CPieChart() : m_Radius(1)
{
	
}
/*----------------------------------------------------------------------------------------------*/
CPieChart::~CPieChart()
{

}
/*----------------------------------------------------------------------------------------------*/
// add a slice to the pie chart's list
CPieSlice* CPieChart::AddSlice(char* key_name, long value)
{
    if (value > 0 )
    {
        CPieSliceEx *pNewItem = new CPieSliceEx;
	    pNewItem->pSlice = new CPieSlice(key_name, value);
	    this->m_Items.AddItem(pNewItem);

    	//this->CalculateSlicePos();
	    return pNewItem->pSlice;
    }
    return NULL;
}
/*----------------------------------------------------------------------------------------------*/
// calculates the coordinates of each slice based on a unit circle (radius = 1)
void CPieChart::CalculateSlicePos()
{
	unsigned int c = 0; // counter
	const short total_deg = 360;
	long total_values = 0;

	// get the sum of the pie slice values
	for(c = 1 ; c <= this->m_Items.GetCount() ; c++)
	{
		CPieSliceEx *pItem = this->m_Items.GetItem(c);
		total_values += pItem->pSlice->GetValue();
	}

	unsigned int position = 0;	// current position along the arc
	for(c = 1 ; c <= this->m_Items.GetCount() ; c++)
	{
		CPieSliceEx *pItem = this->m_Items.GetItem(c);

		// calculate the value on a base of 360
		pItem->arc_length = (pItem->pSlice->GetValue() * (long)total_deg) / total_values;
		// be sure the pie is a complete 360 degrees
		// so just override the previously arc_length calculation
		if(c == this->m_Items.GetCount())
			pItem->arc_length = 360 - position;

		// calculate the starting position
		pItem->x_pos1 = cos(position * PI / 180);
		pItem->y_pos1 = sin(position * PI / 180);

		position += pItem->arc_length;
		// calculate the ending position
		pItem->x_pos2 = cos(position * PI / 180);
		pItem->y_pos2 = sin(position * PI / 180);
	}
}
/*----------------------------------------------------------------------------------------------*/
BEGIN_EVENT_TABLE(CPieChartWnd, wxWindow)
	EVT_PAINT(CPieChartWnd::OnPaint)
END_EVENT_TABLE()
/*----------------------------------------------------------------------------------------------*/
// the radius is calculated in relation to the height of the window.
// recommended width of the window is twice the height to properly
// allocate space for the pie slice labels and its cooresponding color key.
CPieChartWnd::CPieChartWnd(wxWindow* parent, wxWindowID id, const wxPoint& pos,
						   const wxSize& size, long style) :
wxWindow(parent, id, pos, size, style)
{
	// set the color array
	m_Colors[0].Set(255,128,128);		// pink
	m_Colors[1].Set(255,255,128);		// light yellow
	m_Colors[2].Set(0,0,255);			// blue
	m_Colors[3].Set(128,128,64);		// tan
	m_Colors[4].Set(255,0,0);			// red
	m_Colors[5].Set(255,128,255);		// light purple
	m_Colors[6].Set(0,255,0);			// green

	// set the radius to half the height of the window
	this->CPieChart::SetRadius(size.GetHeight() / 2);
}
/*----------------------------------------------------------------------------------------------*/
void CPieChartWnd::OnPaint(wxPaintEvent& event)
{
	wxPaintDC dc(this);
	wxSize client_size = this->GetClientSize();

	this->CalculateSlicePos();

	//dc.DrawEllipse(0, 0, this->GetRadius()*2, this->GetRadius()*2);
	// draw each slice of the pie
	unsigned int position = 0, color_index = 0;
	for(unsigned int c = 1 ; c <= this->CPieChart::m_Items.GetCount() ; c++)
	{
		CPieSliceEx *pItem = this->CPieChart::m_Items.GetItem(c);

		// calculate the mid point of the pie chart (circle)
		unsigned int mid_x = client_size.GetHeight() / 2;
		unsigned int mid_y = client_size.GetHeight() / 2;
		// calculate the absolute (effective) coordinates
		int abs_x1 = 0, abs_y1 = 0, abs_x2 = 0, abs_y2 = 0;
		abs_x1 = mid_x + (pItem->x_pos1 * this->CPieChart::m_Radius);
		abs_y1 = ConvertY(pItem->y_pos1);

		abs_x2 = mid_x + (pItem->x_pos2 * this->CPieChart::m_Radius);
		abs_y2 = ConvertY(pItem->y_pos2);

		// create the brush
		wxBrush brush(m_Colors[color_index], wxSOLID);
		dc.SetBrush(brush);

		// draw the pie slice
		dc.DrawEllipticArc(mid_x - this->m_Radius, mid_y - this->m_Radius,
			mid_x + this->m_Radius, mid_y + this->m_Radius,
			position, position + pItem->arc_length);
		dc.DrawLine(mid_x, mid_y, abs_x1, abs_y1);
		dc.DrawLine(mid_x, mid_y, abs_x2, abs_y2);

		// draw the color key (rectangle)
		dc.DrawRectangle((client_size.GetWidth() / 3) * 2 - 40, c * dc.GetCharHeight() * 1.5, 30, dc.GetCharHeight());
		dc.SetBrush(wxNullBrush);

		// get the slice key_name text
		char key_name[16], legend_text[24];
		pItem->pSlice->GetKeyName(key_name);
		sprintf(legend_text, "%s (%d)", key_name, pItem->pSlice->GetValue());
		// draw the slice key_name text
		dc.DrawText(legend_text, (client_size.GetWidth() / 3) * 2, c * dc.GetCharHeight()*1.5);

		// change the color for the next slice
		if(color_index < 6)
			color_index++;
		else
			color_index = 0;

		// update the position for the next slice
		position += pItem->arc_length;
	}
    if (CPieChart::m_Items.GetCount() <= 0)
    {
        dc.DrawText("This chart will be shown once an expenditure is made", 10, 10);
    }
}
/*----------------------------------------------------------------------------------------------*/
unsigned int CPieChartWnd::ConvertY(float y_coord)
{
	if(y_coord == 0.0)
		return this->GetRadius();
	else if(y_coord < 0.0)
	{
		wxSize client_size = this->GetClientSize();
		unsigned int mid_y = client_size.GetHeight() / 2;
		return mid_y + ((y_coord * -1.0) * this->GetRadius());
	}
	else
		return this->GetRadius() - (y_coord * this->GetRadius());
}
/*----------------------------------------------------------------------------------------------*/



