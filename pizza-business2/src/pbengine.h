#ifndef _JS_PBENGINE_H
#define _JS_PBENGINE_H
/*----------------------------------------------------------------------------------------------*/
#include "datarest.h"
#include "datapool.h"
#include "bank.h"
/*----------------------------------------------------------------------------------------------*/
class CApplication;
/*----------------------------------------------------------------------------------------------*/
// addition by johnny
// begun on Jan 31, 2003
// saved game stuff

/*----------------------------------------------------------------------------------------------*/
class CGameEngine
{
public: // construction/destruction
	CGameEngine();
	~CGameEngine() {}
	bool Initialize();

private:
	//CApplication *m_pApplication;

public: // data
	List<COwner> m_OwnerList;
	CDataPool m_DataPool;

	CBank m_Bank;

public: // functions
	int DoEndofTurnBankStuff();

	bool GetError(char *buffer) { strcpy(buffer, error_msg); return error; }
	COwner* GetHumanPlayer();
	COwner* GetPlayer(unsigned int index) { return m_OwnerList.GetItem(index); }
	unsigned int GetPlayerCount();
	
	unsigned int GetTurn() { return m_Data.iGameTurn; }
	void SetTurn(unsigned int turn) { m_Data.iGameTurn = turn; }
	bool IsGameInSession() { return bInSession; }

	bool RestIdAvailable(unsigned short id);
	unsigned short GetNextAvailableRestID();

public: // engine functions
	void StartNewGame();

	void w_LoadGame(char* filename);
	bool w_SaveGame(char* filename);

private:
	short AddPlayer(COwner *pOwner, bool create_restaurant = true);
	void RemoveRestaurant(CRestaurant *pRestaurant);

private:
	char* GetCommaLimit(char* lpstrText, char* return_buffer);

private:
	struct {
		unsigned int iGameTurn;
		unsigned int iPlayerCount;
	} m_Data;
	bool bInSession;

private:
	void SetError(char *message);
	bool error;
	char error_msg[256];
};
/*----------------------------------------------------------------------------------------------*/
#endif

