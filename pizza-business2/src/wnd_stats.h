#ifndef _J_WND_STATS_H_
#define _J_WND_STATS_H_
/*------------------------------------------------------------------------*/
#include <wx/notebook.h>
#include <wx/dialog.h>
#include <wx/button.h>
#include <wx/statline.h>
#include <wx/stattext.h>
#include <wx/textctrl.h>
#include <wx/listctrl.h>
#include <wx/spinctrl.h>
#undef new
#include "datarest.h"
#include "pie_chart.h"
#include "graph.h"
//#include "wxchart/lbchart.h"
/*------------------------------------------------------------------------*/
class CDayStatsDlg : public wxDialog
{
public:
	CDayStatsDlg(wxWindow *parent, CRestaurant* pRestaurant);
	~CDayStatsDlg();

	void OnPaint(wxPaintEvent &e);
	void OnOKClicked(wxMouseEvent &e);

private:

	wxNotebook *m_pNoteBook;
	wxButton *m_pOkButton;
private:
	CRestaurant *m_pRestaurant;

private:
	DECLARE_EVENT_TABLE()
};
/*------------------------------------------------------------------------*/
class CStatsPage1 : public wxWindow
{
public:
	CStatsPage1(wxWindow *parent);
	~CStatsPage1();

	void Initialize(CRestaurant* pRestaurant);

public:
	void OnChecked_FilterChange(wxMouseEvent &e);

	void OnClicked_PrevStats(wxMouseEvent &e);
	void OnClicked_NextStats(wxMouseEvent &e);

private:
	void RefreshStatistics();
	void RefreshStaffList();

private:
	// controls dialy statistics (top half)
	wxStaticLine* line;
	wxStaticText* income;
	wxStaticText* pizzasSold;
	wxStaticText* custsVisits;
	wxStaticText* custsLeft;
	wxStaticText* popularity;
	wxStaticText* m_pDayText;
	wxStaticText* m_pDayTitle;
	wxBitmapButton *m_pPrevButton;
	wxBitmapButton *m_pNextButton;
	wxStaticBitmap *m_BmpStatsBk;

	wxStaticText* m_pStaffTitle;
	wxListCtrl *m_pListView;
	wxCheckBox *m_pCheckCook;
	wxCheckBox *m_pCheckWaiter;
	wxCheckBox *m_pCheckManager;

private:
	CRestaurant *m_pRestaurant;
	unsigned int m_CurStatIndex;
	unsigned short m_ColCount;

private:
	DECLARE_EVENT_TABLE()
};
/*------------------------------------------------------------------------*/
class CStatsPage2 : public wxWindow
{
public:
	CStatsPage2(wxWindow *parent);
	~CStatsPage2();

	void Initialize(CRestaurant* pRestaurant);

private:
	CRestaurant *m_pRestaurant;

private:
	CPieChartWnd *m_pChartIncome;

private:
	DECLARE_EVENT_TABLE()
};
/*------------------------------------------------------------------------*/
class CStatsPage3 : public wxWindow
{
public:
    CStatsPage3(wxWindow *parent);
    ~CStatsPage3();

    void Initialize(CRestaurant* pRestaurant);

protected:
    void OnPaint(wxPaintEvent &e);

private:
    CRestaurant *m_pRestaurant;
    LineGraph income;
    
    DECLARE_EVENT_TABLE()
};
/*------------------------------------------------------------------------*/
#endif

