#include "mainframe.h"
#include "wnd_staff.h"
#include "wnd_stats.h"
#include "wnd_pizza.h"
#include "wnd_buy.h"
#include "wnd_bank.h"
#include "pbutils.h"
#include <wx/colour.h>
/*###########################################################################*/
//
//	CMainPanel Implementation
//
/*###########################################################################*/
BEGIN_EVENT_TABLE(CMainPanel, wxPanel)
	EVT_BUTTON(BTN_STATS, CMainPanel::OnStats)
	EVT_BUTTON(BTN_STAFF, CMainPanel::OnStaff)
	EVT_BUTTON(BTN_BUYSELL, CMainPanel::OnBuySell)
	EVT_BUTTON(BTN_PIZZAS, CMainPanel::OnPizzas)
	EVT_BUTTON(BTN_ENDTURN, CMainPanel::OnEndTurn)
	EVT_BUTTON(BTN_MAINMENU, CMainPanel::OnMainMenu)
	EVT_BUTTON(BTN_BANK, CMainPanel::OnBank)
	EVT_PAINT(CMainPanel::OnPaint)
	REPAINT_BUTTONS(-1, CMainPanel::OnPaintButton)
END_EVENT_TABLE()
/*---------------------------------------------------------------------------*/
CMainPanel::CMainPanel(wxFrame* frame, int x, int y, int width, int height) :
wxPanel(frame, -1, wxPoint(x, y), wxSize(width, height)),
m_BmpBk("bground.bmp", wxBITMAP_TYPE_BMP), // load the background bitmap
m_BmpUI("mainwnd.bmp", wxBITMAP_TYPE_BMP),
m_BmpTemp("mainbk.bmp", wxBITMAP_TYPE_BMP),
m_bFullScreen(false)
{
	m_pCurRestaurant = NULL;

	// nullify the pointers
	m_pBtnStaff = NULL;
	m_pBtnStats = NULL;
	m_pBtnBuySell = NULL;
	m_pBtnPizzas = NULL;
	m_pBtnBank = NULL;
	m_pBtnEndTurn = NULL;
	m_pBtnMainMenu = NULL;

	// set dimensions of the nav bars along the window
	m_navbar_width = 128;
	m_navbar_height = 48;

	// set dimensions of the nav bar buttons
	m_button_width = 100; 
	m_button_height = 30;

	// get a pointer to the Application object
	this->m_pApplication = &wxGetApp();

	this->SetBackgroundColour(wxColour(0,0,0));
}
/*---------------------------------------------------------------------------*/
CMainPanel::~CMainPanel()
{
	// delete the buttons
	if(m_pBtnStaff)
		this->m_pBtnStaff->Destroy();

	if(m_pBtnStats)
		m_pBtnStats->Destroy();

	if(m_pBtnBuySell)
		m_pBtnBuySell->Destroy();

	if(m_pBtnPizzas)
		m_pBtnPizzas->Destroy();

	if(m_pBtnBank)
		m_pBtnBank->Destroy();

	if(m_pBtnEndTurn)
		m_pBtnEndTurn->Destroy();

	if(m_pBtnMainMenu)
		m_pBtnMainMenu->Destroy();
}
/*---------------------------------------------------------------------------*/
void CMainPanel::CreateNavButtons()
{
	const short btn_spacing = 10;
	short b = 0, y = 0;

	// create the five top buttons
	for(b = 0, y = btn_spacing * 3 ; b < 7 ; b++, y += btn_spacing + 30)
	{
		if(b == 5) y += btn_spacing * 2; // end turn button

		if(b == 6) // budget text area
		{
			/*y += btn_spacing * 3;
			// this is to calculate the rect for the budget text
			m_rcBudget.left = rcClient.right-(m_navbar_width/2)-(m_navbtn_width/2);
			m_rcBudget.top = y;
			m_rcBudget.right = m_rcBudget.left + m_navbtn_width;
			m_rcBudget.bottom = m_rcBudget.top + 100;*/
			continue;
		}

		CHoverButton *pButton = new CHoverButton(this, BTN_STATS + b, "",
			wxPoint(((CMainFrame*)this->GetParent())->m_FrameWidth-(m_navbar_width/2)-(m_button_width/2), y),
			wxSize(m_button_width, m_button_height));

		switch(b)
		{
		case 0:
			m_pBtnStats = pButton;
			break;

		case 1:
			m_pBtnStaff = pButton;
			break;

		case 2:
			m_pBtnBuySell = pButton;
			break;

		case 3:
			m_pBtnPizzas = pButton;
			break;

		case 4:
			m_pBtnBank = pButton;
			break;

		case 5:
			m_pBtnEndTurn = pButton;
			break;
		}
	}

	// create the menu button
	for(b = 0, y = 600 - 48-30 ; b < 1 ; b++, y -= btn_spacing + 30)
	{
		if(b == 0)
		{
			this->m_pBtnMainMenu = new CHoverButton(this, BTN_MAINMENU + b, "",
				wxPoint(((CMainFrame*)this->GetParent())->m_FrameWidth-(this->m_navbar_width/2)-(this->m_button_width/2), y),
				wxSize(this->m_button_width, this->m_button_height));
		}

	}
}
/*---------------------------------------------------------------------------*/
void CMainPanel::OnBuySell(wxCommandEvent& event)
{
	CDataPool *pDataPool = &this->m_pApplication->m_Simulator.m_DataPool;
	//CBuySellDlg dialog(this, this->m_pCurRestaurant, pDataPool->m_TableList,
	//	pDataPool->m_OvenList, pDataPool->m_IngList, pDataPool->m_AdList,
	//	pDataPool->m_Chair_price);
    CBuySellDlg dialog(this, this->m_pCurRestaurant, pDataPool);
	dialog.Centre();
	dialog.ShowModal();
}
/*---------------------------------------------------------------------------*/
void CMainPanel::OnEndTurn(wxCommandEvent& event)
{
	this->m_pApplication->m_Simulator.SimulateTurn();
	this->Refresh();
	this->m_pBtnBank->Refresh();
	this->m_pBtnBuySell->Refresh();
	this->m_pBtnEndTurn->Refresh();
	this->m_pBtnMainMenu->Refresh();
	this->m_pBtnPizzas->Refresh();
	this->m_pBtnStaff->Refresh();
	this->m_pBtnStats->Refresh();

	this->ShowStatsDialog();
}
/*---------------------------------------------------------------------------*/
void CMainPanel::OnMainMenu(wxCommandEvent& event)
{
	this->ShowMainMenu();
}
/*---------------------------------------------------------------------------*/
void CMainPanel::OnPizzas(wxCommandEvent& event)
{
	CPizzasDlg dlg(this, this->m_pCurRestaurant);
	dlg.Centre();
	dlg.ShowModal();
}
/*---------------------------------------------------------------------------*/
void CMainPanel::OnBank(wxCommandEvent& event)
{
	CBankDlg dlg(this, this->m_pCurRestaurant);
	dlg.ShowModal();
}
/*---------------------------------------------------------------------------*/
void CMainPanel::OnPaint(wxPaintEvent& event)
{
	wxPaintDC dc(this);
	wxSize frame_size = this->GetClientSize();

	if(this->m_pApplication->m_Simulator.IsGameInSession() == false)
	{
		// create the brush
		wxBrush hbrBground(m_BmpBk);

		// paint the background
		dc.SetBrush(hbrBground);
		dc.DrawRectangle(0, 0, frame_size.GetWidth(), frame_size.GetHeight());
		dc.SetBrush(wxNullBrush);
	}
	else // draw the main user interface
	{
		wxMemoryDC memDC1;
		memDC1.SelectObject(this->m_BmpUI);
		
		const int snav_width = 128;
		const int snav_height = 600;
		const int bnav_width = 800;
		const int bnav_height = 48;

		// draw the right side and then the left
		dc.Blit(800-snav_width, 0, snav_width, snav_height, &memDC1, 800-snav_width, 0);
		dc.Blit(0, 600-bnav_height, bnav_width, bnav_height, &memDC1, 0, 600-bnav_height);

		memDC1.SelectObject(wxNullBitmap);

		// Draw the budget text
		//char budget[15];
        std::string budget;
		CRestaurant *pRestaurant = this->m_pCurRestaurant;
		if(pRestaurant)
		{
			wxColour *myColor = new wxColour(0, 0, 0);
			double rest_budget = pRestaurant->GetBudget();
            budget = "Budget: $";
            budget += ToString(rest_budget, 2);
			//sprintf(budget, "Budget: $%.2f", rest_budget);
			dc.SetTextForeground(*myColor);
			dc.SetFont(wxFont(14, wxDEFAULT, wxNORMAL, wxNORMAL));
			//dc.DrawText(budget, 680, 330);
			dc.DrawText(budget.c_str(), 250, 600-bnav_height+8);
		}

		// draw the temprary background picture
		memDC1.SelectObject(this->m_BmpTemp);
		dc.Blit(0, 0, /*672*/800-snav_width, /*552*/600-bnav_height, &memDC1, 0, 0);
	}
}
/*---------------------------------------------------------------------------*/
void CMainPanel::OnPaintButton(wxEvent &e)
{
	CHoverButton *pButton = 0;
	int x, y;

	int id = e.GetId();
	switch(id)
	{
	case BTN_MAINMENU:
		y = 93;
		pButton = this->m_pBtnMainMenu;	
		break;
	case BTN_STATS:
		y = 186;
		pButton = this->m_pBtnStats;
		break;
	case BTN_STAFF:
		y = 155;
		pButton = this->m_pBtnStaff;
		break;
	case BTN_BUYSELL:
		y = 31;
		pButton = this->m_pBtnBuySell;
		break;
	case BTN_PIZZAS:
		y = 0;
		pButton = this->m_pBtnPizzas;
		break;
	case BTN_BANK:
		y = 124;
		pButton = this->m_pBtnBank;
		break;
	case BTN_ENDTURN:
		y = 62;
		pButton = this->m_pBtnEndTurn;
		break;
	}

	if(pButton)
	{
		if(pButton->IsHovered() == false)
		{
			if(pButton->IsPressed() == true)
				x = 0;
			else
				x = 202;
		}
		else
		{
			if(pButton->IsPressed() == true)
				x = 0;
			else
				x = 101;
		}

		// setup the device contexts
		wxClientDC dc(pButton);
		wxMemoryDC memDC1;
		memDC1.SelectObject(this->m_BmpUI);
		// draw the background
		wxPoint position = pButton->GetPosition();
		dc.Blit(0, 0, this->m_button_width, this->m_button_height, &memDC1, position.x, position.y);
		// draw the button image
		dc.Blit(0, 0, this->m_button_width, this->m_button_height, &memDC1, x, 217, wxAND);
		dc.Blit(0, 0, this->m_button_width, this->m_button_height, &memDC1, x, y, wxOR);
		// cleanup DCs
		memDC1.SelectObject(wxNullBitmap);
	}
}
/*---------------------------------------------------------------------------*/
void CMainPanel::OnStaff(wxCommandEvent& event)
{
	CStaffDlg dialog(this, this->m_pCurRestaurant,
		&this->m_pApplication->m_Simulator.m_DataPool.m_CanList);
	//dialog.SetPeopleList(&this->m_pApplication->m_Simulator.m_DataPool.m_CanList);

	dialog.Centre();
	dialog.ShowModal();
}
/*---------------------------------------------------------------------------*/
void CMainPanel::OnStats(wxCommandEvent& event)
{
	this->ShowStatsDialog();
}
/*---------------------------------------------------------------------------*/
void CMainPanel::ShowMainMenu()
{
	this->m_MainMenu = new CMainMenu(this, -1, "MainMenu", wxPoint(-1,-1),
		wxSize(400, 300), wxDIALOG_MODAL|wxSIMPLE_BORDER);
	
	// note: the return value indicates the user's response
	// to the dialog
	int iResult = this->m_MainMenu->ShowModal();
	switch(iResult)
	{
	case IDC_NEWGAME:
		// start a new game
		this->m_pApplication->m_Simulator.StartNewGame();
		// set the current restaurant
		m_pCurRestaurant = this->m_pApplication->m_Simulator.GetHumanPlayer()->GetRestaurant();
		break;

	case IDC_QUIT:
		this->GetParent()->Close(TRUE);
		break;
	}

	this->m_MainMenu->Close(TRUE);

	// update the background
	this->Refresh();
}
/*---------------------------------------------------------------------------*/
void CMainPanel::ShowStatsDialog()
{
	CDayStatsDlg dialog(this, this->m_pCurRestaurant);
	dialog.Centre();
	dialog.ShowModal();
}
/*###########################################################################*/
//
//	CMainFrame Implementation
//
/*###########################################################################*/
BEGIN_EVENT_TABLE(CMainFrame, wxFrame)
	EVT_BUTTON(BUTTON_TEST, CMainFrame::OnCommand)
END_EVENT_TABLE()
/*---------------------------------------------------------------------------*/
CMainFrame::CMainFrame(const wxString& title, const wxPoint& pos, const wxSize& size) :
wxFrame((wxFrame*)NULL, -1, title, pos, size, wxCAPTION|wxMINIMIZE_BOX|wxSYSTEM_MENU)
{
	// get a pointer to the Application object
	this->m_pApplication = &wxGetApp();

	this->m_FrameWidth = 800;
	this->m_FrameHeight = 600;

	this->SetBackgroundColour(wxColour(0,0,0));

	this->SetClientSize(wxSize(m_FrameWidth, m_FrameHeight));
	this->Centre();

	// create the panel
	this->m_Panel = new CMainPanel(this, 0, 0, -1, -1);

   	this->Show(TRUE);

  	this->m_Panel->ShowMainMenu();
	this->m_Panel->CreateNavButtons();
}
/*---------------------------------------------------------------------------*/
CMainFrame::~CMainFrame()
{
	this->m_Panel->Destroy();
}
/*---------------------------------------------------------------------------*/
void CMainFrame::OnCommand(wxCommandEvent& event)
{

}
/*---------------------------------------------------------------------------*/



