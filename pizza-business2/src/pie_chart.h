#ifndef _JS_PIE_CHART_H
#define _JS_PIE_CHART_H
/*----------------------------------------------------------------------------------------------*/
#include <wx/wx.h>
#include <cstring>
#include "list.h"
using namespace ListH;
/*----------------------------------------------------------------------------------------------*/
class CPieSlice
{
public:
	CPieSlice() {}
	CPieSlice(char* key_name, long value) {
		this->SetKeyName(key_name);
		this->SetValue(value);
	}
	CPieSlice(char* key_name) {
		this->SetKeyName(key_name);
		this->SetValue(0);
	}
	CPieSlice(long value) {
		this->SetKeyName("");
		this->SetValue(value);
	}

public:
	void GetKeyName(char* buffer) { strcpy(buffer, this->m_KeyName); }
	void SetKeyName(char* name) { strcpy(this->m_KeyName, name); }

	long GetValue() { return this->m_Value; }
	void SetValue(long new_value) { this->m_Value = new_value; }

private:
	char m_KeyName[16];
	long m_Value;
};
/*----------------------------------------------------------------------------------------------*/
struct CPieSliceEx
{
	CPieSlice *pSlice;
	short arc_length;
	// starting position on arc
	float x_pos1;
	float y_pos1;
	// ending position on arc
	float x_pos2;
	float y_pos2;
};
/*----------------------------------------------------------------------------------------------*/
class CPieChart
{
public:
	CPieChart();
	~CPieChart();

public:
	CPieSlice* AddSlice(char* key_name, long value = 0);		// add a slice to the pie chart's list

	unsigned int GetRadius() { return this->m_Radius; }
	void SetRadius(unsigned int radius) {
		this->m_Radius = radius;
		CalculateSlicePos();								// need to recalc the slice positions
	}

protected:
	void CalculateSlicePos();

protected:
	List<CPieSliceEx> m_Items;
	unsigned short m_Radius;
};
/*----------------------------------------------------------------------------------------------*/
class CPieChartWnd : public wxWindow, public CPieChart
{
public:
	CPieChartWnd(wxWindow* parent, wxWindowID id, const wxPoint& pos = wxDefaultPosition,
		const wxSize& size = wxDefaultSize, long style = 0);

public:
	void OnPaint(wxPaintEvent& event);

private:
	unsigned int ConvertY(float y_coord);
	wxColour m_Colors[10];

private:
	DECLARE_EVENT_TABLE();
};
/*----------------------------------------------------------------------------------------------*/
#endif

