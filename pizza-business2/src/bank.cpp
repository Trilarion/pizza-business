#include "bank.h"
#include "datarest.h"
/*-----------------------------------------------------------------------------------------------+
|																								 |
|	CBankLoan implementation																	 |
|																								 |
+-----------------------------------------------------------------------------------------------*/
CBankLoan::CBankLoan(double amount, float rate)
{
	m_Data.m_Loan = amount;
	m_Data.m_Balance = 0;
	m_Data.m_Rate = rate;
	m_Data.m_TillEffect = 3;
	m_Data.m_LifeTime = 0;

	// calculate the balance the restaurant
	// owes the bank
	this->m_Data.m_Balance = this->m_Data.m_InitialLoan = amount;
}
/*----------------------------------------------------------------------------------------------*/
short CBankLoan::DecEffectDays()
{
	if(IsInEffect())
		return 0;

	return --this->m_Data.m_TillEffect;
}
/*----------------------------------------------------------------------------------------------*/
bool CBankLoan::IsInEffect()
{
	if(this->m_Data.m_TillEffect > 0)
		return false;
	else
		return true;
}
/*-----------------------------------------------------------------------------------------------+
|																								 |
|	CBankAccount implementation																	 |
|																								 |
+-----------------------------------------------------------------------------------------------*/
CBankAccount::CBankAccount(CRestaurant *pRestOwn, CBank *pBank) :
m_bExpired(false)
{
	this->m_pRestaurant = pRestOwn;
	this->m_pBank = pBank;
}
/*----------------------------------------------------------------------------------------------*/
CBankLoan* CBankAccount::CreateLoan(double amount, float rate)
{
	// reject loan if debt is alt the limit
	if(this->m_pBank->GetDebtLimit() <= this->m_pRestaurant->GetBudget())
		return 0;

	// create the new loan
	CBankLoan *pNewLoan = new CBankLoan(amount, rate);
	// add the loan to the list
	this->m_LoanList.AddItem(pNewLoan);

	return pNewLoan;
}
/*----------------------------------------------------------------------------------------------*/
double CBankAccount::GetAccountBalance()
{
	double balance = 0;
	for(unsigned int c = 1 ; c <= this->m_LoanList.GetCount() ; c++)
	{
		CBankLoan *pLoan = this->m_LoanList.GetItem(c);
		if(pLoan->IsInEffect())
			balance += pLoan->GetBalance();
	}

	return balance;
}
/*----------------------------------------------------------------------------------------------*/
unsigned int CBankAccount::GetActiveLoanCount()
{
	unsigned int count = 0;
	for(unsigned int c = 1 ; c <= this->m_LoanList.GetCount() ; c++)
	{
		CBankLoan *pLoan = this->m_LoanList.GetItem(c);
		if(pLoan->IsInEffect())
			count++;
	}
	return count;
}
/*----------------------------------------------------------------------------------------------*/
double CBankAccount::PayOffLoans(double amount)
{
	for(unsigned int c = 1 ; amount > 0 && this->m_LoanList.GetCount() >= c ;)
	{
		CBankLoan *pLoan = this->m_LoanList.GetItem(c);
		if(pLoan->IsInEffect() == false)
		{
			c++;
			continue;
		}

		double loan_cost = pLoan->GetBalance();
		if(loan_cost <= amount)
		{
			// remove the loan and sub funds from restaurant
			this->m_LoanList.DeleteItem(c);
			this->m_pRestaurant->SubractFromBudget(loan_cost);
			amount -= loan_cost;
		}
		else
		{
			// sub funds from restauarant and recalc the loan's balance
			this->m_pRestaurant->SubractFromBudget(amount);
			pLoan->SetBalance(loan_cost-amount);
			amount = 0;
		}
	}

	return 0;
}
/*----------------------------------------------------------------------------------------------*/
void CBankAccount::ReadFromFile(std::ifstream &infile, unsigned int loan_count)
{
	for(unsigned int c = 1 ; c <= loan_count ; c++)
	{
		CBankLoan *pNewLoan = new CBankLoan;
		pNewLoan->ReadFromFile(infile);

		this->m_LoanList.AddItem(pNewLoan);
	}
}
/*----------------------------------------------------------------------------------------------*/
void CBankAccount::SaveToFile(std::ofstream &outfile)
{
	unsigned int count = this->m_LoanList.GetCount();
	for(unsigned int c = 1 ; c <= count ; c++)
	{
		CBankLoan *pLoan = this->m_LoanList.GetItem(c);
		pLoan->SaveToFile(outfile);
	}
}
/*----------------------------------------------------------------------------------------------*/
void CBankAccount::ThumbLoans()
{
	for(unsigned int c = 1 ; c <= this->m_LoanList.GetCount() ; c++)
	{
		CBankLoan *pLoan = this->m_LoanList.GetItem(c);

		if(pLoan->IsInEffect() == false)
		{
			if(pLoan->DecEffectDays() == 0)
				// credit the restaurant's account (loan is now in effect)
				this->m_pRestaurant->AddToBudget(pLoan->GetBalance());
		}
		else // time for the restaurant to payup!
		{
			// if the life of the loan is greater than the first free
			// of interest period, take out the required interest payment
			// from the bank automatically; otherwise, add the interest
			// to the balance
			unsigned int loan_life = pLoan->GetLifeTime();
			double interest_payment = pLoan->GetInitialAmount() * pLoan->GetRate();
			if(loan_life >= 10)
			{
				this->m_pRestaurant->SubractFromBudget(interest_payment);
				
				// if the debt has gone past the limit, repossess some of the
				// restaurant' items to compensate
				double max_debt = this->m_pBank->GetDebtLimit();
				if(this->m_pRestaurant->GetBudget() > max_debt)
				{
					unsigned int c = 0;
					// first try selling the ingredients
					for(c = 1 ; c <= this->m_pRestaurant->m_IngList.GetCount() 
						&& this->m_pRestaurant->GetBudget() > max_debt ; c++)
					{
						IngCat *pCategory = this->m_pRestaurant->m_IngList.GetItem(c);
						for(unsigned int i = 1 ; pCategory->ingredients.GetCount()
							&& this->m_pRestaurant->GetBudget() > max_debt ; i++)
						{
							Ingredient *pIngredient = pCategory->ingredients.GetItem(i);
							this->m_pRestaurant->Ingredient_Sell(pIngredient->pIngredient, 1);
						}
					}

					// try selling the chairs
					for(c = 1 ; c <= this->m_pRestaurant->GetChairCount()
						&& this->m_pRestaurant->GetBudget() > max_debt ; c++)
						this->m_pRestaurant->SellChair(1, 25.00);

					// try selling the tables
					for(c = 1 ; c <= this->m_pRestaurant->m_TableList.GetCount()
						&& this->m_pRestaurant->GetBudget() > max_debt ; c++)
						this->m_pRestaurant->SellTable(this->m_pRestaurant->m_TableList.GetItem(c));

					// try selling the ovens
					for(c = 1 ; c <= this->m_pRestaurant->m_OvenList.GetCount()
						&& this->m_pRestaurant->GetBudget() > max_debt ; c++)
						this->m_pRestaurant->SellOven(this->m_pRestaurant->m_OvenList.GetItem(c));

				}

				// if the budget did not meet the limitations of the maximum allowed debt
				// then the restaurant is taken from the player
				if(this->m_pRestaurant->GetBudget() > max_debt)
					this->m_bExpired = true;
			}
			else
			{
				pLoan->SetBalance(pLoan->GetBalance() + interest_payment);
			}

			// just update the day count (life) of the loan
			pLoan->SetLifeTime(++loan_life);
		}
	}
}
/*-----------------------------------------------------------------------------------------------+
|																								 |
|	CBank implementation																		 |
|																								 |
+-----------------------------------------------------------------------------------------------*/
CBank::CBank() :
m_LoanRate(0.0), m_max_debt(50000)
{

}
/*----------------------------------------------------------------------------------------------*/
CBankAccount* CBank::CreateAccount(CRestaurant *pRestaurant)
{
	// create a new account and add it to the list of accounts
	CBankAccount *pNewAccount = new CBankAccount(pRestaurant, this);
	this->m_AccountList.AddItem(pNewAccount);

	return pNewAccount;
}
/*----------------------------------------------------------------------------------------------*/
void CBank::CollectLoanMoney()
{
	// iterate through the accounts
	for(unsigned int r = 1 ; r <= this->m_AccountList.GetCount() ; r++)
	{
		CBankAccount *pAccount = this->m_AccountList.GetItem(r);
		
		// subtract the money from
		// the restaurant's budget
		pAccount->ThumbLoans();
	}
}
/*----------------------------------------------------------------------------------------------*/
void CBank::DeleteAccount(CRestaurant *pRestaurant)
{
	// find the index of the account with a
	// matching restaurant and delete it from the list
	for(unsigned int c = 1 ; c <= this->m_AccountList.GetCount() ; c++)
	{
		CBankAccount *pAccount = this->m_AccountList.GetItem(c);
		if(pRestaurant->GetId() == pAccount->GetRestaurant()->GetId())
		{
			this->m_AccountList.DeleteItem(c);
			return;
		}
	}
}
/*----------------------------------------------------------------------------------------------*/
void CBank::DeleteAllAccounts()
{
	this->m_AccountList.DeleteAllItems();
}
/*----------------------------------------------------------------------------------------------*/
CBankAccount* CBank::GetAccount(unsigned int index)
{
	return this->m_AccountList.GetItem(index);
}
/*----------------------------------------------------------------------------------------------*/



