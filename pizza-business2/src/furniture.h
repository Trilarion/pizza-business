#ifndef _JS_FURNITURE_H
#define _JS_FURNITURE_H
/*----------------------------------------------------------------------------------------------*/
#include <string>
#include <fstream>
/*----------------------------------------------------------------------------------------------*/
enum Furniture_Type { FT_MISC, FT_TABLE, FT_OVEN, FT_CHAIR };
/*----------------------------------------------------------------------------------------------*/
class CFurniture
{
public:
	short GetID() { return m_FurnData.iId; }
	void SetID(short id) { m_FurnData.iId = id; }

	void GetName(char* buffer) { strcpy(buffer, m_FurnData.strName); }
	void SetName(char* name) { strcpy(m_FurnData.strName, name); }

	float GetPrice() { return m_FurnData.fPrice; }
	void SetPrice(float price) { m_FurnData.fPrice = price; }

protected:
	struct {
		char strName[256];
		float fPrice;
		short iId;
	} m_FurnData;
};
/*----------------------------------------------------------------------------------------------*/
class COven : public CFurniture
{
public:
	COven();

public:
	void ReadFromFile(std::ifstream &infile) {
		infile.read((char*) &m_OvenData, sizeof(m_OvenData));
		m_FurnData.iId = m_OvenData.iId;
	}

	void SaveToFile(std::ofstream &outfile) {
		m_OvenData.iId = m_FurnData.iId;
		outfile.write((const char*) &m_OvenData, sizeof(m_OvenData));
	}

public: // functions
	short GetPizzaSupport() { return m_OvenData.iPizza_support; }
	void SetPizzaSupport(short num_pizzas) { m_OvenData.iPizza_support = num_pizzas; }

	short GetCookTime() { return m_OvenData.iCooking_time; }
	void SetCookTime(short cook_time) { m_OvenData.iCooking_time = cook_time; }

private: // data
	struct {
		short iPizza_support;
		short iCooking_time;
	
		short iId;	// reserved for loading and saving
	} m_OvenData;
};
/*----------------------------------------------------------------------------------------------*/
class CTable : public CFurniture
{
public:
	CTable();

public:
	void ReadFromFile(std::ifstream &infile) {
		infile.read((char*) &m_TableData, sizeof(m_TableData));
		m_FurnData.iId = m_TableData.iId;
	}

	void SaveToFile(std::ofstream &outfile) {
		m_TableData.iId = m_FurnData.iId;
		outfile.write((const char*) &m_TableData, sizeof(m_TableData));
	}

public: // functions
	short GetChairsSupported() { return m_TableData.iChairSupport; }
	void SetChairsSupported(short num_chairs) { m_TableData.iChairSupport = num_chairs; }

private: // data
	struct {
		short iChairSupport;

		short iId;	// reserved for loading and saving
	} m_TableData;
};
/*----------------------------------------------------------------------------------------------*/
#endif


