#ifndef _JS_HOVERBUTTON_H
#define _JS_HOVERBUTTON_H
/*------------------------------------------------------------------------*/
#include <wx/event.h>
#include <wx/window.h>
#include <wx/button.h>
/*------------------------------------------------------------------------*/
// Declare the custom event type that tells the button's parent window that it needs
// ... to repaint a button
BEGIN_DECLARE_EVENT_TYPES()
    DECLARE_EVENT_TYPE(wxREPAINT_BUTTONS, 7777)
END_DECLARE_EVENT_TYPES()

#define REPAINT_BUTTONS(id, fn) \
    DECLARE_EVENT_TABLE_ENTRY( \
        wxREPAINT_BUTTONS, id, -1, \
        (wxObjectEventFunction)(wxEventFunction)(wxCommandEventFunction)&fn, \
        (wxObject *) NULL \
    ),
/*------------------------------------------------------------------------*/
class CHoverButton : public wxControl
{
public:
	CHoverButton(wxWindow *buttonParent, wxWindowID buttonID, const wxString& label, 
				const wxPoint& pos, const wxSize& size  = wxDefaultSize, long style = 0, 
				const wxValidator& validator = wxDefaultValidator,
				const wxString& name = "button");
	~CHoverButton();

	// Accessor functions
	bool IsHovered();
	void SetHovered(bool hovered);
	bool IsPressed();
	void SetPressed(bool pressed);

private:
	// Event Handling functions
	void OnPaint(wxPaintEvent &e);
	void OnHover(wxMouseEvent &e);
	void OnEndHover(wxMouseEvent &e);
	void OnPressed(wxMouseEvent &e);
	void OnReleased(wxMouseEvent &e);
private:
	bool isHovered;
	bool isPressed;
private:
	DECLARE_EVENT_TABLE();
	
};
/*------------------------------------------------------------------------*/
#endif

