#ifndef _JS_LOGGER_H
#define _JS_LOGGER_H
/*----------------------------------------------------------------------------------------------*/
class CLogFile
{
public:
	CLogFile();
	CLogFile(char* filename, bool bClear = true);

public:
	void Clear();
	void EnableLogging(bool enable) { m_bLogEnabled = enable; }
	void Write(char* text);
	void WriteError(char* text);
	void WriteLineBreak();

private:
	void SetFilename(char* filename);

private:
	char m_Filename[1024];
	bool m_bLogEnabled;
};
/*----------------------------------------------------------------------------------------------*/
#endif

