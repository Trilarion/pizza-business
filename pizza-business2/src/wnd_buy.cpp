#include <wx/msgdlg.h>
#include <wx/dcclient.h>
#include "wnd_buy.h"
#include "pbutils.h"
#include "wnd_contracts.h"
#include "capp.h"
#include <string>
#include <wx/dynarray.h>
#include <wx/intl.h>
/*----------------------------------------------------------------------------------------------*/
// CBuySellDlg event table
BEGIN_EVENT_TABLE(CBuySellDlg, wxDialog)
	EVT_BUTTON(BSD_BUTTON_BUY, CBuySellDlg::OnBuyClicked)
	EVT_BUTTON(BSD_BUTTON_SELL, CBuySellDlg::OnSellClicked)
	EVT_BUTTON(BSD_BUTTON_OK, CBuySellDlg::OnOkClicked)
    EVT_BUTTON(BSD_BUTTON_MANAGECONTRACTS, CBuySellDlg::OnManageContractsClicked)
	EVT_RADIOBUTTON(-1, CBuySellDlg::OnRadioButton) 
END_EVENT_TABLE()
/*----------------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------*/
// CBuySellDlg event table
BEGIN_EVENT_TABLE(CCatalogDlg, wxDialog)
	EVT_BUTTON(CD_BUTTON_BUY, CCatalogDlg::OnBuyClicked)
	EVT_BUTTON(CD_BUTTON_CANCEL, CCatalogDlg::OnCancelClicked)
END_EVENT_TABLE()
/*----------------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------*/
// CSellEditDlg event table
BEGIN_EVENT_TABLE(CSellEditDlg, wxDialog)
	EVT_BUTTON(SED_BUTTON_SELL, CSellEditDlg::OnSellClicked)
	EVT_BUTTON(SED_BUTTON_CANCEL, CSellEditDlg::OnCancelClicked)
END_EVENT_TABLE()
/*----------------------------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------+
|																		   |						 
|	CBuySellDlg implementation											   |						 
|																		   |						 
+-------------------------------------------------------------------------*/
CBuySellDlg::CBuySellDlg(wxWindow* parent, CRestaurant *pRestaurant, CDataPool *pData) : 
			wxDialog(parent, -1, "Buy_Sell Dialog", wxPoint(200, 150), 
						wxSize(450, 370), wxDEFAULT_DIALOG_STYLE, "dialogBox")
{
	this->Centre();

	this->m_pRestaurant = pRestaurant;

    this->m_pData = pData;

    this->m_pTableList = &this->m_pData->m_TableList;
    this->m_pOvenList = &this->m_pData->m_OvenList;
    this->m_pIngList = &this->m_pData->m_IngList;
    this->m_pAdsList = &this->m_pData->m_AdList;
    this->m_chair_price = this->m_pData->m_Chair_price;

	this->RefreshCaption();
	

	this->m_pIngredients = new wxRadioButton(this, BSD_RADIOBTN_INGREDIENTS, _("Ingredients"), wxPoint(10, 20), wxDefaultSize, wxRB_GROUP);
	this->m_pChairs = new wxRadioButton(this, BSD_RADIOBTN_INGREDIENTS, _("Chairs"), wxPoint(10, 60), wxDefaultSize);
	this->m_pOvens =  new wxRadioButton(this, BSD_RADIOBTN_OVENS, _("Ovens"), wxPoint(10, 100), wxDefaultSize);
	this->m_pTables = new wxRadioButton(this, BSD_RADIOBTN_TABLES, _("Tables"), wxPoint(10, 140), wxDefaultSize);
	this->m_pAds = new wxRadioButton(this, BSD_RADIOBTN_ADS, _("Ads"), wxPoint(10, 180), wxDefaultSize);
	this->m_pItems = new wxListBox(this, BSD_LISTBOX, wxPoint(100, 20), wxSize(320, 230));
	this->m_pBuy = new wxButton(this, BSD_BUTTON_BUY, _("Buy"), wxPoint(260, 260));
	this->m_pSell =  new wxButton(this, BSD_BUTTON_SELL, _("Sell"), wxPoint(340, 260));
    this->m_pManageContracts = new wxButton(this, BSD_BUTTON_MANAGECONTRACTS, _("Manage Contracts"), wxPoint(340, 260));
	this->m_pLine = new wxStaticLine(this, -1, wxPoint(10, 305), wxSize(430, 1));
	this->m_pOk = new wxButton(this, BSD_BUTTON_OK, _("Ok"), wxPoint(195, 315));
    this->hireManagerToUseFunc = new wxStaticText(this, -1, _("You must hire a manager\nto access this functionality"), wxPoint(-1, -1));

	this->mainSizer = new wxBoxSizer(wxVERTICAL);
	this->topSizer = new wxBoxSizer(wxHORIZONTAL);
	this->col1 =  new wxBoxSizer(wxVERTICAL);
	this->col2 = new wxBoxSizer(wxVERTICAL);

	this->col1->Add(this->m_pIngredients, 0, wxBOTTOM|wxTOP, 20);
	this->col1->Add(this->m_pChairs, 0, wxBOTTOM, 20);
	this->col1->Add(this->m_pOvens, 0, wxBOTTOM, 20);
	this->col1->Add(this->m_pTables, 0, wxBOTTOM, 20);
	this->col1->Add(this->m_pAds);
	
	this->topSizer->Add(this->col1, 0, wxRIGHT, 20);

	this->col2->Add(this->m_pItems, 1, wxALIGN_RIGHT|wxEXPAND);

	this->topSizer->Add(this->col2, 1, wxEXPAND);

	this->mainSizer->Add(topSizer, 1, wxALL|wxEXPAND, 10);

	this->midBtnSizer = new wxBoxSizer(wxHORIZONTAL);
    this->midBtnSizer->Add(this->m_pManageContracts, 0, wxALIGN_LEFT);
    this->midBtnSizer->Add(this->hireManagerToUseFunc, 1, wxALIGN_LEFT|wxLEFT, 5);
    //this->midBtnSizer->Add(1, 20, 1, wxEXPAND);
	this->midBtnSizer->Add(this->m_pBuy, 0, wxLEFT|wxALIGN_RIGHT|wxRIGHT, 10);
	this->midBtnSizer->Add(this->m_pSell, 0, wxALIGN_RIGHT);

	this->mainSizer->Add(this->midBtnSizer, 0, wxLEFT|wxBOTTOM|wxRIGHT|wxEXPAND/*|wxALIGN_RIGHT*/, 10);

	this->mainSizer->Add(this->m_pLine, 0, wxEXPAND|wxBOTTOM, 10);
	this->mainSizer->Add(this->m_pOk, 0, wxALIGN_CENTER_HORIZONTAL);

    List<CEmployee> list;
    this->m_pRestaurant->GetEmployees(&list, TITLE_MANAGER);
    if (list.GetCount() <= 0)
    {
        this->m_pManageContracts->Enable(false);
        this->hireManagerToUseFunc->SetLabel("You need to hire a manager to use this function");
    }

   	this->SetAutoLayout(true);
	this->SetSizer(mainSizer);
	mainSizer->Fit(this);
	mainSizer->SetSizeHints(this);

	this->RefreshListBox();
}
/*----------------------------------------------------------------------------------------------*/
CBuySellDlg::~CBuySellDlg()
{
	this->m_pBuy->Destroy();
	this->m_pItems->Destroy();
	this->m_pLine->Destroy();
	this->m_pSell->Destroy();
	this->m_pOk->Destroy();
    this->m_pManageContracts->Destroy();
    this->hireManagerToUseFunc->Destroy();
	// Destroy radio buttons...
	this->m_pIngredients->Destroy();
	this->m_pChairs->Destroy();
	this->m_pOvens->Destroy();
	this->m_pTables->Destroy();
	this->m_pAds->Destroy();
}
/*----------------------------------------------------------------------------------------------*/
void CBuySellDlg::RefreshListBox()
{
	this->m_pSell->Enable(true);

	if (this->m_pAds->GetValue())
	{
		this->LB_DumpAdvertisements();
		this->m_pSell->Disable();
	}
	else if (this->m_pChairs->GetValue())
		this->LB_DumpChairs();

	else if (this->m_pIngredients->GetValue())
		this->LB_DumpIngredients();

	else if (this->m_pOvens->GetValue())
		this->LB_DumpOvens();

	else
		this->LB_DumpTables();
}
/*----------------------------------------------------------------------------------------------*/
void CBuySellDlg::LB_DumpAdvertisements()
{
	this->m_pItems->Clear();
	for(unsigned int c = 1 ; c <= this->m_pRestaurant->m_AdsList.GetCount() ; c++)
	{
		AdItem *pItem = this->m_pRestaurant->m_AdsList.GetItem(c);

		//std::string text;
		wxString text;
		char desc[256];
		pItem->pAd->GetDescription(desc);
		text << ToString(pItem->quantity).c_str() << " " << _(desc);
		this->m_pItems->Append(text, pItem);
	}
}
/*----------------------------------------------------------------------------------------------*/
void CBuySellDlg::LB_DumpChairs()
{
	this->m_pItems->Clear();
	
	// NOTE: there is only type of chair, so we are just going
	// to display the number of chairs at the restaurant
	unsigned int chair_count = this->m_pRestaurant->GetChairCount();
	if(chair_count < 1)
		return;

	wxString text;
	text << ToString(chair_count).c_str() << " " << _("Generic Chair");
	this->m_pItems->Append(text);
}
/*----------------------------------------------------------------------------------------------*/
void CBuySellDlg::LB_DumpIngredients()
{
	this->m_pItems->Clear();
	// dump all the ingredients into the listbox
	for(unsigned int c = 1 ; c <= this->m_pRestaurant->m_IngList.GetCount() ; c++)
	{
		IngCat *pIngCat = this->m_pRestaurant->m_IngList.GetItem(c);
		for(unsigned int i = 1 ; i <= pIngCat->ingredients.GetCount() ; i++)
		{
			Ingredient *pIng = pIngCat->ingredients.GetItem(i);

			unsigned int quantity = pIng->IngList.GetCount();
			if(quantity < 1)
				continue;
			
			// add the ingredient to the listbox
			wxString text;
			char ing_name[256];
			pIng->pIngredient->GetName(ing_name);
			text << ToString(quantity).c_str() << " " << _(ing_name);
			this->m_pItems->Append(text, pIng);
		}
	}
}
/*----------------------------------------------------------------------------------------------*/
void CBuySellDlg::LB_DumpOvens()
{
	this->m_pItems->Clear();
	
	for(unsigned int c = 1 ; c <= this->m_pRestaurant->m_OvenList.GetCount() ; c++)
	{
		COven *pOven = this->m_pRestaurant->m_OvenList.GetItem(c);
		
		char name[256];
		pOven->GetName(name);

		this->m_pItems->Append(_(name), pOven);
	}
}
/*----------------------------------------------------------------------------------------------*/
void CBuySellDlg::LB_DumpTables()
{
	this->m_pItems->Clear();
	
	for(unsigned int c = 1 ; c <= this->m_pRestaurant->m_TableList.GetCount() ; c++)
	{
		CTable *pTable = this->m_pRestaurant->m_TableList.GetItem(c);
		
		char name[256];
		pTable->GetName(name);

		this->m_pItems->Append(_(name), pTable);
	}
}
/*----------------------------------------------------------------------------------------------*/
void CBuySellDlg::OnRadioButton(wxCommandEvent &e)
{
	this->RefreshListBox();
}
/*----------------------------------------------------------------------------------------------*/
void CBuySellDlg::OnOkClicked(wxMouseEvent &e)
{
	this->EndModal(wxOK);
}
/*----------------------------------------------------------------------------------------------*/
void CBuySellDlg::OnBuyClicked(wxMouseEvent &e)
{
	CCatalogDlg *pDlg = 0;
	if (this->m_pOvens->GetValue())
	{
		COvenCatDlg *dlg = new COvenCatDlg(this, this->m_pRestaurant, *this->m_pOvenList);
		pDlg = dlg;

	}
	else if (this->m_pAds->GetValue())
	{
		CAdsCatDlg *dlg = new CAdsCatDlg(this, this->m_pRestaurant, *this->m_pAdsList);
		pDlg = dlg;
	}
	else if (this->m_pTables->GetValue())
	{
		CTableCatDlg *dlg = new CTableCatDlg(this, this->m_pRestaurant, *this->m_pTableList);
		pDlg = dlg;
	}
	else if (this->m_pIngredients->GetValue())
	{
		CIngCatDlg *dlg = new CIngCatDlg(this, this->m_pRestaurant, *this->m_pIngList);
		pDlg = dlg;
	}
	else if (this->m_pChairs->GetValue())
	{
		
		CChairCatDlg *dlg = new CChairCatDlg(this, this->m_pRestaurant, this->m_chair_price);
		pDlg = dlg;
	
	}
	if (pDlg)
	{
		pDlg->ShowModal();
		pDlg->Destroy();
	}
	this->RefreshListBox(); // update the listbox
	this->RefreshCaption();
}
/*----------------------------------------------------------------------------------------------*/
void CBuySellDlg::RefreshCaption()
{
	short id = this->m_pRestaurant->GetId();
	double budget = this->m_pRestaurant->GetBudget();
	wxString text;
	text << _("Restaurant") << " #" << ToString(id).c_str() << " ($" << ToString(budget, 2).c_str() << ")"; 
	this->SetTitle(text);

    wxGetApp().GetTopWindow()->Refresh();
}
/*----------------------------------------------------------------------------------------------*/
void CBuySellDlg::OnSellClicked(wxMouseEvent &e)
{
	if (this->m_pIngredients->GetValue())
	{
		Ingredient *pIng = (Ingredient*)this->m_pItems->GetClientData(this->m_pItems->GetSelection());
		if(pIng)
		{
			// construct and set the description
			wxString text;
			char ing_name[256];
			unsigned int count = pIng->IngList.GetCount();
			pIng->pIngredient->GetName(ing_name);
			text << _("Enter the number of") << " " << _(ing_name) << _("s to sell.  You have a total of")
				 << " " << ToString(count).c_str() << " " << _(ing_name) << "s.";

			CSellEditDlg dlg(this, text, 0);

			// TRUE if use has pressed the "Sell" (Ok) button
			if(dlg.ShowModal() == wxOK)
			{
				int number = dlg.GetNumber();
				this->m_pRestaurant->Ingredient_Sell(pIng->pIngredient, number);
			}
		}
	}
	else if (this->m_pChairs->GetValue())
	{
		// construct and set the description
		wxString text;
		text << _("Enter the number of chairs to sell.  You have a total of") << " "
			 << ToString(this->m_pRestaurant->GetChairCount()).c_str() << " "
			 << _("chairs") << ".";

		CSellEditDlg dlg(this, text, 0);

		if(dlg.ShowModal() == wxOK)
		{
			int number = dlg.GetNumber();
			this->m_pRestaurant->SellChair(number, this->m_chair_price);
		}
	}
	else if (this->m_pOvens->GetValue())
	{
		COven *pOven = (COven*)this->m_pItems->GetClientData(this->m_pItems->GetSelection());
		if(pOven)
			this->m_pRestaurant->SellOven(pOven);
	}
	else if (this->m_pTables->GetValue())
	{
		CTable *pTable = (CTable*)this->m_pItems->GetClientData(this->m_pItems->GetSelection());
		if(pTable)
			this->m_pRestaurant->SellTable(pTable);
	}
	this->RefreshCaption();
	this->RefreshListBox();
}
/*----------------------------------------------------------------------------------------------*/
void CBuySellDlg::OnManageContractsClicked(wxMouseEvent &e)
{
    CContractsDlg dlg(this, &this->m_pRestaurant->contractManager, this->m_pData);
    dlg.ShowModal();
}
/*----------------------------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------+
|																		   |						 
|	CCatalogDlg implementation											   |						 
|																		   |						 
+-------------------------------------------------------------------------*/
CCatalogDlg::CCatalogDlg(wxWindow* parent, CRestaurant* pRestaurant) : 
					wxDialog(parent, -1, "Still needs a good caption", wxPoint(200, 150), 
								wxSize(405, 370), wxDEFAULT_DIALOG_STYLE|wxRESIZE_BORDER, "dialogBox")
{
	this->m_pRestaurant = pRestaurant;

	this->selectItem = new wxStaticText(this, -1, _("Select an item to purchase:"), wxPoint(10, 10));
	this->itemList = new wxListBox(this, -1, wxPoint(10, 30), wxSize(380, 135), 0, NULL, wxLB_EXTENDED);
	this->howMany = new wxStaticText(this, -1, _("How many would you like to purchase:"), wxPoint(10, 180));
	this->quantityText = new wxTextCtrl(this, -1, "", wxPoint(205, 175), wxSize(60, 25));
	this->buy = new wxButton(this, CD_BUTTON_BUY, _("Buy"), wxPoint(235, 315));
	this->cancel = new wxButton(this, CD_BUTTON_CANCEL, _("Cancel"), wxPoint(320, 315)); 
	this->chairSupportText = new wxStaticText(this, -1, "", wxPoint(10, 220));

	this->mainSizer = new wxBoxSizer(wxVERTICAL);
	this->mainSizer->Add(selectItem, 0, wxEXPAND|wxALL, 10 );
	this->mainSizer->Add(itemList, 1, wxLEFT|wxRIGHT|wxBOTTOM|wxEXPAND, 10);


	this->textAndEditSizer = new wxBoxSizer(wxHORIZONTAL);
	this->textAndEditSizer->Add(howMany, 0, wxALIGN_CENTER_VERTICAL, 10);
	this->textAndEditSizer->Add(quantityText, 0, wxLEFT, 10 );

	this->mainSizer->Add(textAndEditSizer, 0, wxLEFT|wxRIGHT|wxBOTTOM, 10);

	this->buttonsSizer = new wxBoxSizer(wxHORIZONTAL);
	this->buttonsSizer->Add(buy, 0, wxALIGN_RIGHT|wxRIGHT, 10);
	this->buttonsSizer->Add(cancel, 0, wxALIGN_RIGHT);

	this->mainSizer->Add(chairSupportText, 0, wxEXPAND|wxALL, 10);

	this->mainSizer->Add(buttonsSizer, 0, wxALIGN_RIGHT|wxRIGHT|wxLEFT, 10);

	this->SetAutoLayout(true);
	this->SetSizer(mainSizer);
	mainSizer->Fit(this);
	mainSizer->SetSizeHints(this);

}
/*----------------------------------------------------------------------------------------------*/
CCatalogDlg::~CCatalogDlg()
{
	this->selectItem->Destroy();
	this->itemList->Destroy();
	this->howMany->Destroy();
	this->quantityText->Destroy();
	this->buy->Destroy();
	this->cancel->Destroy();
	this->chairSupportText->Destroy();
}
/*----------------------------------------------------------------------------------------------*/
unsigned int CCatalogDlg::GetQuantity()
{
	wxString text;
	unsigned int quantity;

	if (this->quantityText->GetLineText(0) != "")
	{
		text = this->quantityText->GetValue();

		quantity = StringTo<int>(text.c_str());
	}
	else
		quantity = 0;

	return quantity;
}
/*----------------------------------------------------------------------------------------------*/
void CCatalogDlg::OnBuyClicked(wxMouseEvent &e)
{
	this->OnBuySelected();
	this->EndModal(wxOK);
}
/*----------------------------------------------------------------------------------------------*/
void CCatalogDlg::OnCancelClicked(wxMouseEvent &e)
{
	this->EndModal(wxCANCEL);
}
/*-----------------------------------------------------------------------------------------------+
|																								 |						 
|	CAdsCatDlg implementation																	 |						 
|																							     |						 
+-----------------------------------------------------------------------------------------------*/
CAdsCatDlg::CAdsCatDlg(wxWindow* parent, CRestaurant* pRestaurant, List<CAdCategory> &lsList) :
			CCatalogDlg(parent, pRestaurant)
{
	this->m_pAdsList = &lsList;

	// add all the advertisements to the listbox
	for(unsigned int c = 1 ; c <= this->m_pAdsList->GetCount() ; c++)
	{
		CAdCategory *pCategory = this->m_pAdsList->GetItem(c);
		for(unsigned int i = 1 ; i <= pCategory->AdList.GetCount() ; i++)
		{
			CAdvertisement *pAd = pCategory->AdList.GetItem(i);

			char desc[256];
			pAd->GetDescription(desc);

			this->itemList->Append(_(desc), (void*)pAd);
		}
	}
}
/*----------------------------------------------------------------------------------------------*/
CAdsCatDlg::~CAdsCatDlg()
{}
/*----------------------------------------------------------------------------------------------*/
void CAdsCatDlg::OnBuySelected()
{
	wxArrayInt selections;
	CAdvertisement *pAd;

	// get the quantity
	unsigned int quantity = this->GetQuantity();
	
	this->itemList->GetSelections(selections);

	for (unsigned int i = 0; i < selections.GetCount(); i++)
	{
		pAd = (CAdvertisement*)this->itemList->GetClientData(selections.Item(i));

		if(pAd && quantity > 0)
			this->m_pRestaurant->Advertisement_Buy(pAd, quantity);
	}
}
/*----------------------------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------+
|																		   |						 
|	CChairCatDlg implementation											   |						 
|																		   |						 
+-------------------------------------------------------------------------*/
CChairCatDlg::CChairCatDlg(wxWindow* parent, CRestaurant* pRestaurant, float chair_price):
				CCatalogDlg(parent, pRestaurant)
{
	this->m_chair_price = chair_price;

	// NOTE: as of now, we only have one generic chair

	// add chairs to the listbox
	wxString text = _("Generic Chair");
	this->itemList->Append(text, (void*)0);

	wxString text2;
	text2 << _("The restaurant can currently support") << " "
		  << ToString(this->m_pRestaurant->GetChairSupportCount() - this->m_pRestaurant->GetChairCount()).c_str()
		  << " " << _("more chairs") << ".";
	this->chairSupportText->SetLabel(text2);
}
/*----------------------------------------------------------------------------------------------*/
CChairCatDlg::~CChairCatDlg()
{
}
/*----------------------------------------------------------------------------------------------*/
void CChairCatDlg::OnBuySelected()
{
	// NOTE: as of now, we only have one generic chair,
	// so we will just use the price of the one chair

	unsigned int quantity = this->GetQuantity();

	if(quantity > 0)
		this->m_pRestaurant->BuyChair(quantity, this->m_chair_price);
}
/*----------------------------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------+
|																		   |						 
|	CIngCatDlg implementation											   |						 
|																		   |						 
+-------------------------------------------------------------------------*/
CIngCatDlg::CIngCatDlg(wxWindow* parent, CRestaurant* pRestaurant, List<CIngCategory> &lsList):
			CCatalogDlg(parent, pRestaurant)
{
	this->m_pIngList = &lsList;

	for(unsigned int c = 1 ; c <= this->m_pIngList->GetCount() ; c++)
	{
		CIngCategory *pCategory = this->m_pIngList->GetItem(c);
		for(unsigned int i = 1 ; i <= pCategory->IngList.GetCount() ; i++)
		{
			CIngredient *pIng = pCategory->IngList.GetItem(i);

			char name[256];
			pIng->GetName(name);

			this->itemList->Append(_(name), (void*)pIng);
		}
	}
}			
/*----------------------------------------------------------------------------------------------*/
CIngCatDlg::~CIngCatDlg()
{}
/*----------------------------------------------------------------------------------------------*/
void CIngCatDlg::OnBuySelected()
{
	wxArrayInt selections;
	// get the pointer to the CIngredient object
	// CIngredient *pIng = (CIngredient*)this->itemList->GetClientData(this->itemList->GetSelection());
	this->itemList->GetSelections(selections);
	
	// get the quantity
	unsigned int quantity = this->GetQuantity();

	for(unsigned int c = 0; c < selections.GetCount(); c++)
	{
		CIngredient *pIng = (CIngredient*)this->itemList->GetClientData(selections.Item(c));
		if(pIng && quantity > 0)
			this->m_pRestaurant->Ingredient_Buy(pIng, quantity);
	}
}
/*----------------------------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------+
|																		   |						 
|	COvenCatDlg implementation											   |						 
|																		   |						 
+-------------------------------------------------------------------------*/
COvenCatDlg::COvenCatDlg(wxWindow* parent, CRestaurant* pRestaurant, List<COven> &lsList):
				CCatalogDlg(parent, pRestaurant)
{
	this->m_pOvenList = &lsList;

	// add all the ovens to the listbox
	for(unsigned int c = 1 ; c <= this->m_pOvenList->GetCount() ; c++)
	{
		COven *pOven = this->m_pOvenList->GetItem(c);

		char name[256];
		pOven->GetName(name);

		this->itemList->Append(_(name), (void*)pOven);
	}
}
/*----------------------------------------------------------------------------------------------*/
COvenCatDlg::~COvenCatDlg()
{}
/*----------------------------------------------------------------------------------------------*/
void COvenCatDlg::OnBuySelected()
{
	wxArrayInt selections;
	COven *pOven;

	this->itemList->GetSelections(selections);
	
	// get the quantity
	unsigned int quantity = this->GetQuantity();

	for (unsigned int i = 0; i < selections.GetCount(); i++)
	{
		pOven = (COven*)this->itemList->GetClientData(selections.Item(i));	
		if(pOven && quantity > 0)
		{
			for(unsigned int c = 0 ; c < quantity ; c++)
				this->m_pRestaurant->BuyOven(pOven);
		}
	}
}
/*----------------------------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------+
|																		   |						 
|	CTableCatDlg implementation											   |						 
|																		   |						 
+-------------------------------------------------------------------------*/
CTableCatDlg::CTableCatDlg(wxWindow* parent, CRestaurant* pRestaurant, List<CTable> &lsList):
				CCatalogDlg(parent, pRestaurant)
{
	this->m_pTableList = &lsList;

	// add all the tables to the listbox
	for(unsigned int c = 1 ; c <= this->m_pTableList->GetCount() ; c++)
	{
		CTable *pTable = this->m_pTableList->GetItem(c);

		char name[256];
		pTable->GetName(name);

		this->itemList->Append(_(name), (void*)pTable);
	}
}
/*----------------------------------------------------------------------------------------------*/
CTableCatDlg::~CTableCatDlg()
{}
/*----------------------------------------------------------------------------------------------*/
void CTableCatDlg::OnBuySelected()
{
	wxArrayInt selections;
	CTable *pTable;

	this->itemList->GetSelections(selections);

	// get the quantity
	unsigned int quantity = this->GetQuantity();

	for (unsigned int i = 0; i < selections.GetCount(); i++)
	{
		pTable = (CTable*)this->itemList->GetClientData(selections.Item(i));
		if(pTable && quantity > 0)
		{
			for(unsigned int c = 0 ; c < quantity ; c++)
				this->m_pRestaurant->BuyTable(pTable);
		}
	}
}
/*----------------------------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------+
|																		   |						 
|	CSellEditDlg implementation											   |						 
|																		   |						 
+-------------------------------------------------------------------------*/
CSellEditDlg::CSellEditDlg(wxWindow* parent, wxString desc, int number) : 
				wxDialog(parent, -1, _("Sell how many"), wxPoint(0,0), wxSize(415, 140), wxDEFAULT_DIALOG_STYLE)
{
	this->m_desc = desc;
	this->m_Number = number;

	this->Centre();

	this->enterNumber = new wxStaticText(this, -1, desc, wxPoint(10, 10));
	this->numToSell = new wxSpinCtrl(this, -1, "0", wxPoint(10, 35), wxSize(390, 25));
	this->line = new wxStaticLine(this, -1, wxPoint(10, 70), wxSize(390, 1), wxLI_HORIZONTAL);
	this->sell = new wxButton(this, SED_BUTTON_SELL, _("Sell"), wxPoint(245, 85));
	this->cancel = new wxButton(this, SED_BUTTON_CANCEL, _("Cancel"), wxPoint(330, 85));

	this->mainSizer = new wxBoxSizer(wxVERTICAL);
	this->mainSizer->Add(this->enterNumber, 0, wxALL, 10);
	this->mainSizer->Add(this->numToSell, 0, wxEXPAND|wxLEFT|wxRIGHT|wxBOTTOM, 10);
	this->mainSizer->Add(this->line, 0, wxEXPAND|wxLEFT|wxRIGHT|wxBOTTOM, 10);

	this->bottomBtnSizer = new wxBoxSizer(wxHORIZONTAL);
	this->bottomBtnSizer->Add(this->sell, 0, wxRIGHT|wxALIGN_RIGHT, 10);
	this->bottomBtnSizer->Add(this->cancel, 0, wxALIGN_RIGHT);
	this->mainSizer->Add(this->bottomBtnSizer, 0, wxALIGN_RIGHT|wxRIGHT|wxLEFT|wxBOTTOM, 10);

	this->SetAutoLayout(true);
	this->SetSizer(this->mainSizer);
	this->mainSizer->Fit(this);
	this->mainSizer->SetSizeHints(this);
}
/*---------------------------------------------------------------------------------------------*/
CSellEditDlg::~CSellEditDlg()
{
}
/*----------------------------------------------------------------------------------------------*/
int CSellEditDlg::GetNumber()
{
	return this->m_Number;
}
/*----------------------------------------------------------------------------------------------*/
void CSellEditDlg::OnSellClicked(wxMouseEvent &e)
{
	this->m_Number = this->numToSell->GetValue();
	this->EndModal(wxOK);
}
/*----------------------------------------------------------------------------------------------*/
void CSellEditDlg::OnCancelClicked(wxMouseEvent &e)
{
	this->EndModal(wxCANCEL);
}
/*----------------------------------------------------------------------------------------------*/



