#include <fstream>
#include <cstdio>
#include "pbengine.h"
/*----------------------------------------------------------------------------------------------*/
CGameEngine::CGameEngine() : error(false), bInSession(false)
{
	// get a pointer to the Application object
	//this->m_pApplication = &wxGetApp();

	strcpy(this->error_msg, "");

	m_Data.iGameTurn = 0;
	m_Data.iPlayerCount = 0;
}
/*----------------------------------------------------------------------------------------------*/
short CGameEngine::AddPlayer(COwner *pOwner, bool create_restaurant)
{
	// add owner to the list
	this->m_OwnerList.AddItem(pOwner);

	if(create_restaurant == true)
	{
		// each player starts with one restaurant
		CRestaurant *pRestaurant = new CRestaurant(pOwner);
		pOwner->AddRestaurant(pRestaurant);
		// assign the restaurant an identification number
		pRestaurant->SetId(this->GetNextAvailableRestID());

		// initialize the ingredient list
		pRestaurant->InitIngredientList(&this->m_DataPool.m_IngList);
		// initialize the pizza list
		pRestaurant->InitPizzaList(&this->m_DataPool.m_PizzaList);

		// create an account at the bank
		// for the new restaurant
		pRestaurant->m_pBankAccount = this->m_Bank.CreateAccount(pRestaurant);
	}

	// return the number of players in the game
	this->m_Data.iPlayerCount = m_OwnerList.GetCount();
	return this->m_Data.iPlayerCount;
}
/*----------------------------------------------------------------------------------------------*/
int CGameEngine::DoEndofTurnBankStuff()
{
	// time to collect!
	this->m_Bank.CollectLoanMoney();

	// see if any accounts have expired. if one has expired, remove the account
	// and remove its corresponding restaurant from the game
	for(unsigned int c = 1 ; c <= this->m_Bank.GetAccountCount() ; c++)
	{
		CBankAccount *pAccount = this->m_Bank.GetAccount(c);
		if(pAccount->IsExpired())
		{
			CRestaurant *pRestaurant = pAccount->GetRestaurant();
			this->m_Bank.DeleteAccount(pAccount->GetRestaurant());
			this->RemoveRestaurant(pRestaurant);
			return 1;
		}
	}
	return 0;
}
/*----------------------------------------------------------------------------------------------*/
char* CGameEngine::GetCommaLimit(char* lpstrText, char* return_buffer)
{
	char* rtn_text = 0;
	// parse through the string looking for the
	// comma or the null
	for(short i = 0 ; i <= strlen(lpstrText) ; i++)
	{
		switch(lpstrText[i])
		{
		case '\0':
		case '\n':
			if(i == 0) return 0;
			return_buffer[i] = '\0';
			rtn_text = &lpstrText[i];
			return rtn_text;

		case ',':
			if(i == 0) return 0;
			return_buffer[i] = '\0';
			rtn_text = &lpstrText[i+1];
			return rtn_text;

		default:
			return_buffer[i] = lpstrText[i];
			break;
		}
	}

	return 0;
}
/*----------------------------------------------------------------------------------------------*/
COwner* CGameEngine::GetHumanPlayer()
{
	const short index_human = 1;
	return this->m_OwnerList.GetItem(index_human);
}
/*----------------------------------------------------------------------------------------------*/
unsigned int CGameEngine::GetPlayerCount()
{
	this->m_Data.iPlayerCount = m_OwnerList.GetCount();
	return this->m_Data.iPlayerCount;
}
/*----------------------------------------------------------------------------------------------*/
bool CGameEngine::Initialize()
{
	// initialize the data pool
	bool bResult = this->m_DataPool.Initialize();
	if(!bResult) {
		this->SetError("Error initializing DataPool. Be sure \"entities.pbd\" exists "
			"in the same directory as the executable.");
	}
	return bResult;
}
/*----------------------------------------------------------------------------------------------*/
void CGameEngine::RemoveRestaurant(CRestaurant *pRestaurant)
{
	for(unsigned int c = 1 ; c <= this->m_OwnerList.GetCount() ; c++)
	{
		COwner *pOwner = this->m_OwnerList.GetItem(c);
		
		for(unsigned int r = 1 ; r <= pOwner->m_StoreList.GetCount() ; r++)
		{
			CRestaurant *pRest = pOwner->m_StoreList.GetItem(r);
			if(pRest->GetId() == pRestaurant->GetId())
			{
				pOwner->m_StoreList.DeleteItem(r);
				return;
			}
		}
	}
}
/*----------------------------------------------------------------------------------------------*/
void CGameEngine::w_LoadGame(char* filename)
{
	std::ifstream infile;
	infile.open(filename, std::ios::binary);
	
	if(infile.is_open())
	{
		// delete all the players
		this->m_OwnerList.DeleteAllItems();

		// delete all accounts at the bank
		this->m_Bank.DeleteAllAccounts();

		// generate a new set of employees to hire
		this->m_DataPool.m_CanList.DeleteAllItems();
		PBD_DATA pbd;
		pbd.dwFlags = DATA_FIRSTNAMES|DATA_LASTNAMES|DATA_DESC|DATA_AGE|DATA_COMPETENCY/*|DATA_CHAIRPRICE*/;
		Entities_LoadFile("entities.pbd", pbd);
		this->m_DataPool.CreateEmployees(pbd, 20);
        //this->m_DataPool.m_Chair_price = pbd.Chair.price;

		// read the game data from the file
		infile.read((char*) &this->m_Data, sizeof(this->m_Data));

		// read each player from the file
		for(unsigned int c = 1 ; c <= this->m_Data.iPlayerCount ; c++)
		{
			COwner *pPlayer = new COwner;
			pPlayer->m_pGameEngine = this;
			// add the player to the current game
			this->AddPlayer(pPlayer, false);

			pPlayer->LoadFromFile(infile);
		}

		infile.close();
		bInSession = true;

	}
}
/*----------------------------------------------------------------------------------------------*/
bool CGameEngine::w_SaveGame(char* filename)
{
	std::ofstream outfile;
	outfile.open(filename, std::ios::out|std::ios::binary);

	if(outfile.is_open())
	{

		// update and write the game engine data
		this->GetPlayerCount();							// refreshes the count in the data structure
		outfile.write((const char*) &this->m_Data, sizeof(this->m_Data));

		// write each player to the file
		unsigned int player_count = this->m_OwnerList.GetCount();
		for(unsigned int c = 1 ; c <= player_count ; c++)
		{
			COwner *pOwner = this->m_OwnerList.GetItem(c);
			pOwner->SaveToFile(outfile);
		}

		outfile.close();
		return true;
	}
	else
		return false;
}
/*----------------------------------------------------------------------------------------------*/
void CGameEngine::SetError(char *message)
{
	this->error = true;
	strcpy(this->error_msg, message);
}
/*----------------------------------------------------------------------------------------------*/
void CGameEngine::StartNewGame()
{
	// there is always a player; create one
	// the human player is always index #1 in the list

	// delete all the players
	this->m_OwnerList.DeleteAllItems();

	// delete all accounts at the bank
	this->m_Bank.DeleteAllAccounts();

	// add one human player
	COwner *pOwner = new COwner;
	pOwner->m_pGameEngine = this;
	this->AddPlayer(pOwner);

	// generate a new set of employees to hire
	this->m_DataPool.m_CanList.DeleteAllItems();
	PBD_DATA pbd;
	pbd.dwFlags = DATA_FIRSTNAMES|DATA_LASTNAMES|DATA_DESC|DATA_AGE|DATA_COMPETENCY;
	Entities_LoadFile("entities.pbd", pbd);
	this->m_DataPool.CreateEmployees(pbd, 20);

	// reset the game turn
	this->SetTurn(0);

	bInSession = true;
}
/*----------------------------------------------------------------------------------------------*/
unsigned short CGameEngine::GetNextAvailableRestID()
{
	for(unsigned short c = 0 ; c < 256 ; c++)
	{
		if(this->RestIdAvailable(c) == true)
			return c;
	}

	return 0;
}
/*----------------------------------------------------------------------------------------------*/
bool CGameEngine::RestIdAvailable(unsigned short id)
{
	// iterate through all the players' restaurants
	for(unsigned short c = 1 ; c <= this->m_OwnerList.GetCount() ; c++)
	{
		COwner *pOwner = this->m_OwnerList.GetItem(c);
		for(unsigned short r = 1 ; r <= pOwner->GetRestaurantCount() ; r++)
		{
			CRestaurant *pRestaurant = pOwner->GetRestaurant(r);
			if(pRestaurant->GetId() != id)
				return true;
		}
	}
	return false;
}
/*----------------------------------------------------------------------------------------------*/



