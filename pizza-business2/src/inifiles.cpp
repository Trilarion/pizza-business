#include <cstdio>
#include "inifiles.h"
using std::ifstream;
/*----------------------------------------------------------------------------------------------*/
double CIniFile::GetDouble(char* section_name, char* key_name, double default_double)
{
	ifstream infile;
	// open the file
	infile.open(this->m_Filename);

	if(this->MoveToSection(infile, section_name) == true)
	{
		char buffer[256];
		if(this->GetKeyString(infile, key_name, buffer, 255) == true)
		{
		infile.close();	// close the file
			

			return atof(buffer);
		}
	}

	// close the file
	infile.close();

	return default_double;
}
/*----------------------------------------------------------------------------------------------*/
unsigned int CIniFile::GetInteger(char* section_name, char* key_name, unsigned int default_int)
{
	ifstream infile;
	// open the file
	infile.open(this->m_Filename);

	if(this->MoveToSection(infile, section_name) == true)
	{
		char buffer[256];
		if(this->GetKeyString(infile, key_name, buffer, 255) == true)
		{
			// close the file
			infile.close();

			return atoi(buffer);
		}
	}

	// close the file
	infile.close();

	return default_int;
}
/*----------------------------------------------------------------------------------------------*/
bool CIniFile::GetKeyString(ifstream &infile, char* key_name, char* returned_string,
	unsigned int rs_size)
{
	while(infile.eof() == false)
	{
		char *buffer = new char[rs_size];
		infile.getline(buffer, rs_size);

		switch(buffer[0])
		{
			// comments should be ignored
			case '#':
			case ';':
				continue;
			// we hit another section, key was not found
			// break from function with error
			case '[':
				return false;
		}

		// find the '=' character
		char* the_string = strchr(buffer, '=');
		if(the_string == 0) continue;	// a '=' was not found

		// get the key, add null char to it
		char the_key[256];
		strncpy(the_key, buffer, the_string-buffer);
		the_key[the_string-buffer] = '\0';
		
		// if we found a matching key name
		// get the data associated with it
		if(strcmp(the_key, key_name) == 0)
		{
			// get the characters past the '='
			strncpy(returned_string, the_string + 1, rs_size);
			delete buffer;
			return true;
		}
		delete buffer;
	}

	// if we made it this far, then apparently the end of the file 
	// was reached and key was not found
	return false;
}
/*----------------------------------------------------------------------------------------------*/
unsigned int CIniFile::GetString(char* section_name, char* key_name, char* default_string,
		char* returned_string, unsigned int rs_size)
{
	unsigned int return_size = 0;

	ifstream infile;
	// open the file
	infile.open(this->m_Filename);

	// later if this variable is true the default_string will be
	// copid to the returned_string
	bool do_default = true;

	if(this->MoveToSection(infile, section_name) == true)
		do_default = !(this->GetKeyString(infile, key_name, returned_string, rs_size));

	// if the section or key was not found
	// then copy the default string to the returned string
	if(do_default == true)
		strncpy(returned_string, default_string, rs_size);

	// close the file
	infile.close();

	return strlen(returned_string);
}
/*----------------------------------------------------------------------------------------------*/
bool CIniFile::MoveToSection(ifstream &infile, char* section_name)
{
	char section[256];
	sprintf(section, "[%s]", section_name);

	while(infile.eof() == false)
	{
		char buffer[256];
		infile.getline(buffer, 255);

		// skip the comments (';' and '#')
		if(buffer[0] == ';' || buffer[0] == '#')
			continue;

		// if the section was found, return true
		if(strcmp(buffer, section) == 0)
			return true;
	}

	// the section was not found, return false
	return false;
}
/*----------------------------------------------------------------------------------------------*/
bool CIniFile::IsFileValid()
{
	ifstream infile;
	infile.open(this->m_Filename);

	if(infile.fail() != 0)
		return false;

	infile.close();
	return true;
}
/*----------------------------------------------------------------------------------------------*/



