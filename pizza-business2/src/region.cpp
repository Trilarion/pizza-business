#include "region.h"
#include <string>
/*-----------------------------------------------------------------------------------------------+
|																								 |
|	CRegion implementation																		 |
|																								 |
+-----------------------------------------------------------------------------------------------*/
void CRegion::AddRestToRegion(int id)
{
	unsigned short* a = new unsigned short;
	(*a) = (unsigned short)id;
	m_restaurants.AddItem(a);
}
/*----------------------------------------------------------------------------------------------*/
void CRegion::RemoveRestFromRegion(int id)
{
	for (unsigned int i = 1; i <= m_restaurants.GetCount(); i++)
	{
		if ( (*(m_restaurants.GetItem(i))) == id )
		{
			m_restaurants.DeleteItem(i);
			break;
		}
	}
}
/*----------------------------------------------------------------------------------------------*/
char* CRegion::GetNameOfRegion()
{
	return data.name;
}
/*----------------------------------------------------------------------------------------------*/
void CRegion::SetNameOfRegion(char* newName)
{
	strcpy(data.name, newName);
}
/*----------------------------------------------------------------------------------------------*/
int CRegion::GetPopulationOfRegion()
{
	return data.population;
}
/*----------------------------------------------------------------------------------------------*/
void CRegion::SetPopulationOfRegion(int newPopulation)
{
	data.population = newPopulation;
}
/*----------------------------------------------------------------------------------------------*/
double CRegion::GetPropertyValueOfRegion()
{
	return data.propertyValue;
}
/*----------------------------------------------------------------------------------------------*/
void CRegion::SetPropertyValueOfRegion(double propValue)
{
	data.propertyValue = propValue;
}
/*----------------------------------------------------------------------------------------------*/



