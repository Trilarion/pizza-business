#ifndef _JS_WND_BUY_H
#define _JS_WND_BUY_H
/*----------------------------------------------------------------------------------------------*/
#include <wx/listbox.h>
#include <wx/dialog.h>
#include <wx/button.h>
#include <wx/radiobut.h>
#include <wx/statline.h>
#include <wx/spinctrl.h>
#include <wx/stattext.h>
#include <wx/textctrl.h>
#include <wx/sizer.h>
#undef new
#include "datarest.h"
#include "datapool.h"
/*----------------------------------------------------------------------------------------------*/
class CBuySellDlg : public wxDialog
{
public:
	CBuySellDlg(wxWindow* parent, CRestaurant *pRestaurant, CDataPool *pData);
	~CBuySellDlg();

	void RefreshCaption();

protected:
	// Event handling functions
	void OnRadioButton(wxCommandEvent &e);
	void OnOkClicked(wxMouseEvent &e);
	void OnBuyClicked(wxMouseEvent &e);
	void OnSellClicked(wxMouseEvent &e);
    void OnManageContractsClicked(wxMouseEvent &e);

private:
	void RefreshListBox();
	void LB_DumpAdvertisements();
	void LB_DumpChairs();
	void LB_DumpIngredients();
	void LB_DumpOvens();
	void LB_DumpTables();

private:
	wxRadioButton *m_pIngredients;
	wxRadioButton *m_pChairs;
	wxRadioButton *m_pOvens;
	wxRadioButton *m_pTables;
	wxRadioButton *m_pAds;
	wxListBox* m_pItems;
	wxButton* m_pBuy;
	wxButton* m_pSell;
	wxButton* m_pOk;
    wxButton* m_pManageContracts;
	wxStaticLine *m_pLine;
    wxStaticText *hireManagerToUseFunc;
	wxBoxSizer *mainSizer, *topSizer, *col1, *col2, *midBtnSizer;
	CRestaurant *m_pRestaurant;
	float m_chair_price;
	List<CTable> *m_pTableList;
	List<COven> *m_pOvenList;
	List<CAdCategory> *m_pAdsList;
	List<CIngCategory> *m_pIngList;
    CDataPool *m_pData;

	DECLARE_EVENT_TABLE()
};
/*----------------------------------------------------------------------------------------------*/
enum {
	BSD_LISTBOX = 10,
	BSD_BUTTON_BUY = 12,
	BSD_BUTTON_SELL = 13, 
	BSD_BUTTON_OK = 14,
	BSD_RADIOBTN_INGREDIENTS = 15,
	BSD_RADIOBTN_CHAIRS = 16,
	BSD_RADIOBTN_OVENS = 17,
	BSD_RADIOBTN_TABLES = 18,
	BSD_RADIOBTN_ADS = 19,
    BSD_BUTTON_MANAGECONTRACTS = 20
};
/*----------------------------------------------------------------------------------------------*/
class CCatalogDlg : public wxDialog
{
public:
	CCatalogDlg(wxWindow* parent, CRestaurant* pRestaurant);
	~CCatalogDlg();

protected:
	unsigned int GetQuantity();
	virtual void OnBuySelected() {};
	void OnBuyClicked(wxMouseEvent &e);
	void OnCancelClicked(wxMouseEvent &e);

protected:
	CRestaurant* m_pRestaurant;
	wxListBox* itemList;
	wxStaticText* selectItem;
	wxStaticText* howMany;
	wxTextCtrl* quantityText;
	wxStaticText* details;
	wxStaticText* chairSupportText;
	wxButton* buy;
	wxButton* cancel;
	wxBoxSizer *mainSizer;
	wxBoxSizer *textAndEditSizer;
	wxBoxSizer *buttonsSizer;

private:
	DECLARE_EVENT_TABLE()
};
/*----------------------------------------------------------------------------------------------*/
enum {
	CD_BUTTON_BUY = 15,
	CD_BUTTON_CANCEL
};
/*----------------------------------------------------------------------------------------------*/
class CAdsCatDlg : public CCatalogDlg
{
public:
	CAdsCatDlg(wxWindow* parent, CRestaurant* pRestaurant, List<CAdCategory> &list);
	~CAdsCatDlg();

protected:
	void OnBuySelected();

private:
	List<CAdCategory> *m_pAdsList;

};
/*----------------------------------------------------------------------------------------------*/
class CChairCatDlg : public CCatalogDlg
{
public:
	CChairCatDlg(wxWindow* parent, CRestaurant* pRestaurant, float chair_price);
	~CChairCatDlg();


protected:
	void OnBuySelected();

private:
	float m_chair_price;
};
/*----------------------------------------------------------------------------------------------*/
class CIngCatDlg : public CCatalogDlg
{
public:
	CIngCatDlg(wxWindow* parent, CRestaurant* pRestaurant, List<CIngCategory> &list);
	~CIngCatDlg();


protected:
	void OnBuySelected();

private:
	List<CIngCategory> *m_pIngList;
};
/*----------------------------------------------------------------------------------------------*/
class COvenCatDlg : public CCatalogDlg
{
public:
	COvenCatDlg(wxWindow* parent, CRestaurant* pRestaurant, List<COven> &list);
	~COvenCatDlg();



protected:
	void OnBuySelected();

private:
	List<COven> *m_pOvenList;
};
/*----------------------------------------------------------------------------------------------*/
class CTableCatDlg : public CCatalogDlg
{
public:
	CTableCatDlg(wxWindow* parent, CRestaurant* pRestaurant, List<CTable> &list);
	~CTableCatDlg();


protected:
	void OnBuySelected();

private:
	List<CTable> *m_pTableList;
};
/*----------------------------------------------------------------------------------------------*/
class CSellEditDlg : public wxDialog
{
public:
	CSellEditDlg(wxWindow* parent, wxString desc, int number);
	~CSellEditDlg();

	int GetNumber();

protected:
	void OnSellClicked(wxMouseEvent &e);
	void OnCancelClicked(wxMouseEvent &e);

private:
	wxSpinCtrl *numToSell;
	wxButton *sell;
	wxButton *cancel;
	wxStaticText *enterNumber;
	wxStaticLine *line;
	wxBoxSizer* mainSizer;
	wxBoxSizer* bottomBtnSizer;

private:
	wxString m_desc;
	int m_Number;

	DECLARE_EVENT_TABLE()
};
/*----------------------------------------------------------------------------------------------*/
enum {
	SED_BUTTON_SELL = 99,
	SED_BUTTON_CANCEL
};
/*----------------------------------------------------------------------------------------------*/
#endif

