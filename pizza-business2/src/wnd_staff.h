#ifndef _J_WND_STAFF_H
#define _J_WND_STAFF_H
/*------------------------------------------------------------------------*/
#include <wx/wx.h>
#include <wx/listctrl.h>
#include <wx/dialog.h>
#include <wx/button.h>
#include <wx/radiobox.h>
#include <wx/radiobut.h>
#include <wx/statline.h>
#include <wx/stattext.h>
#include <wx/sizer.h>
#undef new 
#include "datarest.h"
#include "list.h"
/*------------------------------------------------------------------------*/
/// The main staff dialog
class CStaffDlg: public wxDialog
{
public:
	/// The constructor for the staff dialog
	/// \param parent A pointer to the dialog's parent
	/// \param pRestaurant A pointer to the restaurant object from which to get necessary information
	/// \param pList A pointer to the list of people who are able to be hired, i.e. potential employees
	CStaffDlg(wxWindow *parent, CRestaurant* pRestaurant, List<CEmployee> *pList);
	~CStaffDlg();

protected:
	// Event handling functions
	void OnPaint(wxPaintEvent &e);
	void OnOKClicked(wxMouseEvent &e);
	void OnHireClicked(wxMouseEvent &e);
	void OnFireClicked(wxMouseEvent &e);
	void OnRadioBtn(wxCommandEvent &e);

private:
	void RefreshListView();
	/// Adds an employee to the staff listbox
	/// \param pEmployee A pointer to the employee
	void ListView_AddEmployee(CEmployee *pEmployee);
	void FireSelectedEmployees();

private:
	wxButton *m_pOkButton;
	wxButton *m_pInfoButton;
	wxButton *m_pHireButton;
	wxButton *m_pFireButton;
	wxListCtrl *m_pEmployeeList;
	wxStaticLine *m_pStaticLine;
	wxStaticText *yourStaff;
	CRestaurant* m_pRestaurant;
	List<CEmployee> *m_pPeople;
	wxRadioButton *all, *manager, *waiter, *cook;
	wxStaticBox *box;
	wxBoxSizer *mainSizer;
	wxBoxSizer *statBoxSizer;
	wxBoxSizer *midBtnSizer;
	wxStaticBoxSizer *boxSizer;

private:
	DECLARE_EVENT_TABLE()
};
/*------------------------------------------------------------------------*/
enum {
	BUTTON_OK = 1,
	BUTTON_HIRE = 4,
	BUTTON_FIRE = 5,
	BUTTON_INFO = 6,
	RADIO_BOX = 100,
	MY_LIST_CTRL = 101
};
/*------------------------------------------------------------------------*/
class CHireDlg : public wxDialog
{
public:
	CHireDlg::CHireDlg(wxWindow* parent, List<CEmployee> *pList, Emp_Title initType);
	CHireDlg::~CHireDlg();

	List<CEmployee>* GetHireList();
	Emp_Title GetJobTitle();

protected:
	void OnPaint(wxPaintEvent &e);

	// Event handling functions
	void OnHireClicked(wxMouseEvent &e);
	void OnCancelClicked(wxMouseEvent &e);
	void OnRadioBtn(wxCommandEvent &e);

private:
	void ListView_AddEmployee(CEmployee *pEmployee);
	void ListView_GetSelectEmployees();
	void RecalcSalaries();

private:
	wxListCtrl *m_pHireeList;
	//wxRadioBox *m_pJobsRadioBox;
	wxStaticBox *box;
	wxRadioButton *cook, *manager, *waiter;
	wxButton *m_pHireButton;
	wxButton *m_pCancelButton;
	wxStaticText* selectPerson;
	wxBoxSizer *mainSizer, *btnSizer;
	wxStaticBoxSizer *boxSizer;
	List<CEmployee> *m_pPeople;
	List<CEmployee> m_hireList;
	Emp_Title m_title;

private:
	DECLARE_EVENT_TABLE()
};
/*------------------------------------------------------------------------*/
enum {
	BUTTON_HIRE_HIRE = 10,
	BUTTON_CANCEL_HIRE = 11,
	RADIO_BOX_HIRE = 12,
	HIRE_LIST = 13
};
/*------------------------------------------------------------------------*/
#endif

