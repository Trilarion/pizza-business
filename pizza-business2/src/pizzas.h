#ifndef _JS_PIZZAS_H
#define _JS_PIZZAS_H
/*----------------------------------------------------------------------------------------------*/
#include "buysell.h"
/*----------------------------------------------------------------------------------------------*/
class CDataPool;
/*----------------------------------------------------------------------------------------------*/
class CPizza
{
public: // construction/destruction
	CPizza();

public: // functions
	// ingredients
	bool AddIngredient(CIngredient *pIngredient);
	CIngredient* GetIngredient(unsigned int index);
	unsigned int GetIngredientCount() { return m_IngList.GetCount(); }
	bool IsIngredientIncluded(CIngredient *pIngredient);
	void RemoveAllIngredients();
	void RemoveIngredient(CIngredient *pIngredient);

	// pricing
	float GetProductionCost();

	// name
	void GetName(char *buffer) { strcpy(buffer, m_Data.name); }
	void SetName(char *name) { strcpy(m_Data.name, name); }

	void LoadFromFile(std::ifstream &infile, CDataPool &dataPool);
	void SaveToFile(std::ofstream &outfile);

private: // data
	List<CIngredient> m_IngList;

	struct {
		char name[256];

		// reserverd for file loading/saving
		unsigned int ingCount;
	}m_Data;

	// ingredient item structure
	struct PizzaIng {
		unsigned int iId;
	};
};
/*----------------------------------------------------------------------------------------------*/
class CPizzaObject
{
public:
	CPizzaObject() {
		this->SetPrice(0.0f);
		this->ForSale(true);
	};
	CPizzaObject(CPizza *pPizza);

	void LoadFromFile(std::ifstream &infile, CDataPool &dataPool);
	void SaveToFile(std::ofstream &outfile);

public:
	CPizza* GetPizza() { return &m_Pizza; }

	float GetPrice() { return m_Data.fPrice; }
	void SetPrice(float price) { m_Data.fPrice = price; }

	void ForSale(bool sale) { m_Data.bForSale = sale; }
	bool IsForSale() { return m_Data.bForSale; }

private:
	CPizza m_Pizza;

	struct {
		float fPrice;
		bool bForSale;
	}m_Data;
};
/*----------------------------------------------------------------------------------------------*/
#endif

