#include <wx/string.h>
#include <wx/dcclient.h>
#include <wx/msgdlg.h>
#include "wnd_staff.h"
#undef new
#include <string>
#include "pbutils.h"
/*------------------------------------------------------------------------*/
// CStaffDlg event table
BEGIN_EVENT_TABLE(CStaffDlg, wxDialog)
	EVT_PAINT(CStaffDlg::OnPaint)
	EVT_BUTTON(BUTTON_OK, CStaffDlg::OnOKClicked)
	EVT_BUTTON(BUTTON_HIRE, CStaffDlg::OnHireClicked)
	EVT_BUTTON(BUTTON_FIRE, CStaffDlg::OnFireClicked)
	EVT_RADIOBUTTON(-1, CStaffDlg::OnRadioBtn)
END_EVENT_TABLE()
/*------------------------------------------------------------------------*/
// CHireDlg event table
BEGIN_EVENT_TABLE(CHireDlg, wxDialog)
	EVT_PAINT(CHireDlg::OnPaint)
	EVT_BUTTON(BUTTON_CANCEL_HIRE, CHireDlg::OnCancelClicked)
	EVT_BUTTON(BUTTON_HIRE_HIRE, CHireDlg::OnHireClicked)
	EVT_RADIOBUTTON(-1, CHireDlg::OnRadioBtn)
END_EVENT_TABLE()
/*------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------+
|																		   |						 
|	CStaffDlg implementation											   |						 
|																		   |						 
+-------------------------------------------------------------------------*/
CStaffDlg::CStaffDlg(wxWindow *parent, CRestaurant* pRestaurant, List<CEmployee> *pList):
	wxDialog(parent, -1, _("Staff Dialog"), wxPoint(200, 150), 
				wxSize(500, 500), wxDEFAULT_DIALOG_STYLE, "dialogBox")
{
	m_pRestaurant = pRestaurant;
	this->m_pPeople = pList;

	this->Centre();

	this->box = new wxStaticBox(this, -1, _("Show Jobs"), wxPoint(10, 5), wxSize(480, 50));

	this->all = new wxRadioButton(this, -1, _("All"), wxPoint(20, 25), wxDefaultSize, wxRB_GROUP);
	this->cook = new wxRadioButton(this, -1, _("Cooks"), wxPoint(85, 25), wxDefaultSize);
	this->manager =  new wxRadioButton(this, -1, _("Managers"), wxPoint(170, 25), wxDefaultSize);
	this->waiter = new wxRadioButton(this, -1, _("Waiters"), wxPoint(250, 25), wxDefaultSize);

	m_pEmployeeList = new wxListCtrl(this, MY_LIST_CTRL, wxPoint(10, 80), wxSize(475, 250), 
									wxLC_REPORT|wxSUNKEN_BORDER, wxDefaultValidator, "listCtrl");
	wxSize listSize = this->m_pEmployeeList->GetSize();
	m_pEmployeeList->InsertColumn(0, _("Name"), wxLIST_FORMAT_LEFT, (int)(.40*listSize.GetX()));
	m_pEmployeeList->InsertColumn(1, _("Job"), wxLIST_FORMAT_LEFT, (int)(.25*listSize.GetX()));
	m_pEmployeeList->InsertColumn(2, _("Salary"), wxLIST_FORMAT_LEFT, (int)(.25*listSize.GetX()));

	m_pStaticLine = new wxStaticLine(this, -1, wxPoint(10, 430), wxSize(450, 3), 
									wxLI_HORIZONTAL, "m_pStaticLine");

	m_pInfoButton = new wxButton(this, BUTTON_INFO, _("Info"), wxPoint(250, 330));
	m_pInfoButton->Disable();
	m_pHireButton = new wxButton(this, BUTTON_HIRE, _("Hire"), wxPoint(330, 330));
	m_pFireButton = new wxButton(this, BUTTON_FIRE, _("Fire"), wxPoint(410, 330));

	m_pOkButton = new wxButton(this, BUTTON_OK, _("Ok"), wxPoint(220, 450));

	this->yourStaff = new wxStaticText(this, -1, _("Your staff: "), wxPoint(10, 60));

	// Sizer stuff
	this->mainSizer = new wxBoxSizer(wxVERTICAL);

	this->boxSizer = new wxStaticBoxSizer(this->box, wxHORIZONTAL);
	this->boxSizer->Add(this->all, 0, wxALIGN_CENTER_VERTICAL|wxRIGHT|wxLEFT, 20);
	this->boxSizer->Add(this->cook, 0, wxALIGN_CENTER_VERTICAL|wxRIGHT, 20);
	this->boxSizer->Add(this->manager, 0, wxALIGN_CENTER_VERTICAL|wxRIGHT, 20);
	this->boxSizer->Add(this->waiter, 0, wxALIGN_CENTER_VERTICAL|wxRIGHT, 20);

	this->mainSizer->Add(this->boxSizer, 0, wxALL|wxEXPAND, 10);

	this->mainSizer->Add(this->yourStaff, 0, wxBOTTOM|wxLEFT, 10);

	this->mainSizer->Add(this->m_pEmployeeList, 1, wxALIGN_LEFT|wxEXPAND|wxLEFT|wxRIGHT|wxBOTTOM, 10);

	this->midBtnSizer = new wxBoxSizer(wxHORIZONTAL);
	this->midBtnSizer->Add(this->m_pInfoButton, 0, wxALIGN_RIGHT|wxRIGHT, 10);
	this->midBtnSizer->Add(this->m_pHireButton, 0, wxALIGN_RIGHT|wxRIGHT, 10);
	this->midBtnSizer->Add(this->m_pFireButton, 0, wxALIGN_RIGHT);

	this->mainSizer->Add(this->midBtnSizer, 0, wxLEFT|wxRIGHT|wxALIGN_RIGHT, 10);
	this->mainSizer->Add(500, 100, 0, wxEXPAND);

	this->mainSizer->Add(this->m_pStaticLine, 0, wxEXPAND|wxLEFT|wxRIGHT|wxBOTTOM, 10);
	this->mainSizer->Add(this->m_pOkButton, 0, wxALIGN_CENTER_HORIZONTAL);

	this->SetAutoLayout(true);
	this->SetSizer(this->mainSizer);
	mainSizer->Fit(this);
	mainSizer->SetSizeHints(this);

	this->RefreshListView();
}
/*------------------------------------------------------------------------*/
CStaffDlg::~CStaffDlg()
{
	this->m_pOkButton->Destroy();
	this->all->Destroy();
	this->box->Destroy();
	this->cook->Destroy();
	this->manager->Destroy();
	this->waiter->Destroy();
	this->m_pEmployeeList->Destroy();
	this->m_pHireButton->Destroy();
	this->m_pFireButton->Destroy();
	this->m_pStaticLine->Destroy();
	this->yourStaff->Destroy();
}
/*------------------------------------------------------------------------*/
void CStaffDlg::OnOKClicked(wxMouseEvent &e)
{
	this->EndModal(true);
}
/*------------------------------------------------------------------------*/
void CStaffDlg::OnHireClicked(wxMouseEvent &e)
{
    Emp_Title temp;
    if (this->all->GetValue() || this->cook->GetValue())
        temp = TITLE_CHEF;
    else if (this->manager->GetValue())
        temp = TITLE_MANAGER;
    else
        temp = TITLE_WAITER;
	CHireDlg hireDialog(this, this->m_pPeople, temp);
	if (hireDialog.ShowModal() == wxOK)
	{
		List<CEmployee> *pHireList = hireDialog.GetHireList();
		Emp_Title title = hireDialog.GetJobTitle();
		for(unsigned int c = 1 ; c <= pHireList->GetCount() ; c++)
			this->m_pRestaurant->HireEmployee(pHireList->GetItem(c), title);
	}
	this->RefreshListView();
}
/*------------------------------------------------------------------------*/
void CStaffDlg::OnFireClicked(wxMouseEvent &e)
{
	// Check for multiple selections and update game data...
	wxMessageDialog dialog(this, _("Are you sure you want to terminate the selected employee?"),
							_("Employee Termination"), wxYES_NO, wxDefaultPosition);
	dialog.Centre();
	if (dialog.ShowModal() == wxID_YES)
	{
		this->FireSelectedEmployees();
		this->RefreshListView();
	}
}   
/*------------------------------------------------------------------------*/
void CStaffDlg::OnPaint(wxPaintEvent &e)
{
	wxPaintDC paint(this);
}
/*------------------------------------------------------------------------*/
void CStaffDlg::OnRadioBtn(wxCommandEvent &e)
{
	this->RefreshListView();
}
/*------------------------------------------------------------------------*/
void CStaffDlg::ListView_AddEmployee(CEmployee *pEmployee)
{
	if(!pEmployee) 
		return;

	// get the data
	char name[256];
	pEmployee->GetName(name);

	Emp_Title title = pEmployee->GetTitle();
	float salary = pEmployee->GetSalary();

	// add the employee to the listview control
	int myIndex = this->m_pEmployeeList->GetItemCount();
	this->m_pEmployeeList->InsertItem(myIndex, name);

	//std::string str_title, str_salary;
	wxString str_title;
	if(title == TITLE_CHEF)
		str_title = _("Cook");
	else if(title == TITLE_MANAGER)
		str_title = _("Manager");
	else if(title == TITLE_WAITER)
		str_title = _("Waiter");
	m_pEmployeeList->SetItem(myIndex, 1, str_title);

	m_pEmployeeList->SetItem(myIndex, 2, ToString(salary, 2).c_str());
	

	m_pEmployeeList->SetItemData(myIndex, (long)pEmployee);
}
/*------------------------------------------------------------------------*/
void CStaffDlg::FireSelectedEmployees()
{
	// iterate through the listview items
	int count = this->m_pEmployeeList->GetItemCount();
	for(int index = 0 ; index < count ; index++)
	{
		// add selected employees to the list
		if(this->m_pEmployeeList->GetItemState(index, wxLIST_STATE_SELECTED))
		{
			this->m_pRestaurant->FireEmployee((CEmployee*)this->m_pEmployeeList->GetItemData(index));
		}
	}
}
/*------------------------------------------------------------------------*/
void CStaffDlg::RefreshListView()
{
	bool bShowAll = false;
	Emp_Title title;

	if (this->cook->GetValue())
		title = TITLE_CHEF;
	else if (this->manager->GetValue())
		title = TITLE_MANAGER;
	else if (this->waiter->GetValue())
		title = TITLE_WAITER;
	else
		bShowAll = true;
		

	this->m_pEmployeeList->DeleteAllItems();
	// add the hired employees to the listview control
	List<CEmployee> *pHiredList = &this->m_pRestaurant->m_EmpList;
	for(unsigned int c = 1 ; c <= pHiredList->GetCount() ; c++)
	{
		CEmployee *pEmployee = pHiredList->GetItem(c);
	
		if(bShowAll == true || pEmployee->GetTitle() == title)
			this->ListView_AddEmployee(pEmployee);
	}
}
/*-------------------------------------------------------------------------+
|																		   |						 
|	CHireDlg implementation											       |						 
|																		   |						 
+-------------------------------------------------------------------------*/
CHireDlg::CHireDlg(wxWindow* parent, List<CEmployee> *pList, Emp_Title initType):
	wxDialog(parent, -1, "Hire Staff", wxPoint(-1, -1), 
				wxSize(400, 450), wxDEFAULT_DIALOG_STYLE, "dialogBox")
{
	this->m_pPeople  = pList;
	this->m_hireList.SetDeleteFlag(false);

	this->Centre();

	//this->m_title = TITLE_CHEF;
    this->m_title = initType;

	this->m_pHireeList = new wxListCtrl(this, MY_LIST_CTRL, wxPoint(10, 80), 
										wxSize(380, 250), wxLC_REPORT|wxSUNKEN_BORDER, 
										wxDefaultValidator, "listCtrl");
		
	this->m_pHireeList->InsertColumn(0, _("Name"), wxLIST_FORMAT_LEFT, 160);
	this->m_pHireeList->InsertColumn(1, _("Salary"), wxLIST_FORMAT_LEFT, 95);

	this->box = new wxStaticBox(this, -1, _("Hire person for what job"), wxPoint(10, 5), wxSize(380, 45));
	this->cook = new wxRadioButton(this, -1, _("Cook"), wxPoint(15, 20), wxDefaultSize, wxRB_GROUP);
	this->manager =  new wxRadioButton(this, -1, _("Manager"), wxPoint(100, 20), wxDefaultSize);
	this->waiter = new wxRadioButton(this, -1, _("Waiter"), wxPoint(185, 20), wxDefaultSize);

    this->cook->SetValue(false);
    this->manager->SetValue(false);
    this->waiter->SetValue(false);

    if (m_title == TITLE_CHEF)
        this->cook->SetValue(true);
    else if (m_title == TITLE_MANAGER)
        this->manager->SetValue(true);
    else
        this->waiter->SetValue(true);

	this->m_pHireButton = new wxButton(this, BUTTON_HIRE_HIRE, _("Hire!"), wxPoint(155, 280));

	this->m_pCancelButton = new wxButton(this, BUTTON_CANCEL_HIRE, _("Cancel"), wxPoint(233, 280));

	for(unsigned int c = 1 ; c <= this->m_pPeople->GetCount() ; c++)
	{
		CEmployee *pEmployee = m_pPeople->GetItem(c);
		if(pEmployee->m_EmployedAt == NULL)
			this->ListView_AddEmployee(pEmployee);
	}

	this->selectPerson = new wxStaticText(this, -1, _("Select a person to hire:"), wxPoint(10, 60));

	// Sizer stuff
	this->mainSizer = new wxBoxSizer(wxVERTICAL);
	this->boxSizer = new wxStaticBoxSizer(this->box, wxHORIZONTAL);
	this->boxSizer->Add(this->cook, 0, wxLEFT|wxRIGHT, 20);
	this->boxSizer->Add(this->manager, 0, wxRIGHT, 20);
	this->boxSizer->Add(this->waiter);
	this->mainSizer->Add(this->boxSizer, 0, wxALL|wxEXPAND, 10);
	this->mainSizer->Add(this->selectPerson, 0, wxLEFT|wxRIGHT|wxBOTTOM, 10);
	this->mainSizer->Add(this->m_pHireeList, 0, wxLEFT|wxRIGHT|wxBOTTOM|wxEXPAND, 10);

	this->btnSizer = new wxBoxSizer(wxHORIZONTAL);
	this->btnSizer->Add(this->m_pHireButton, 0, wxRIGHT|wxALIGN_RIGHT, 10);
	this->btnSizer->Add(this->m_pCancelButton, 0, wxALIGN_RIGHT);

	this->mainSizer->Add(this->btnSizer, 0, wxLEFT|wxRIGHT|wxALIGN_RIGHT, 10);

	this->SetAutoLayout(true);
	this->SetSizer(this->mainSizer);
	mainSizer->Fit(this);
	mainSizer->SetSizeHints(this);

	this->RecalcSalaries();
}
/*------------------------------------------------------------------------*/
CHireDlg::~CHireDlg()
{
	this->m_pHireeList->Destroy();
	this->box->Destroy();
	this->cook->Destroy();
	this->manager->Destroy();
	this->waiter->Destroy();
	this->m_pHireButton->Destroy();
	this->m_pCancelButton->Destroy();
	this->selectPerson->Destroy();
}
/*------------------------------------------------------------------------*/
void CHireDlg::OnPaint(wxPaintEvent &e)
{
	wxPaintDC paint(this);
}
/*------------------------------------------------------------------------*/
void CHireDlg::OnCancelClicked(wxMouseEvent &e)
{
	this->EndModal(wxCANCEL);
}
/*------------------------------------------------------------------------*/
void CHireDlg::OnHireClicked(wxMouseEvent &e)
{
	this->ListView_GetSelectEmployees();
	this->EndModal(wxOK);
}
/*------------------------------------------------------------------------*/
void CHireDlg::OnRadioBtn(wxCommandEvent &e)
{
	if (this->cook->GetValue())
		this->m_title = TITLE_CHEF;
	else if (this->manager->GetValue())
		this->m_title = TITLE_MANAGER;
	else
		this->m_title = TITLE_WAITER;
	this->RecalcSalaries();
}
/*------------------------------------------------------------------------*/
List<CEmployee>* CHireDlg::GetHireList()
{
	return &(this->m_hireList);
}
/*------------------------------------------------------------------------*/
Emp_Title CHireDlg::GetJobTitle()
{
	return this->m_title;
}
/*------------------------------------------------------------------------*/
void CHireDlg::ListView_AddEmployee(CEmployee* pEmployee)
{
	char name[256];
	pEmployee->GetName(name);

	int index = this->m_pHireeList->GetItemCount();
	this->m_pHireeList->InsertItem(index, name);
	this->m_pHireeList->SetItemData(index, (long)pEmployee);
}
/*------------------------------------------------------------------------*/
void CHireDlg::ListView_GetSelectEmployees()
{
	// iterate through the listview items
	int count = this->m_pHireeList->GetItemCount();
	for(int index = 0 ; index < count ; index++)
	{
		// add selected employees to the list
		if(this->m_pHireeList->GetItemState(index, wxLIST_STATE_SELECTED))
		{
			this->m_hireList.AddItem((CEmployee*)this->m_pHireeList->GetItemData(index));
		}
	}
}
/*------------------------------------------------------------------------*/
void CHireDlg::RecalcSalaries()
{
	// iterate through the listview items
	int count = this->m_pHireeList->GetItemCount();
	for(int index = 0 ; index < count ; index++)
	{
		// claculate the salary of each person

		CEmployee *pEmployee = (CEmployee*)this->m_pHireeList->GetItemData(index);
		if(pEmployee == NULL)
		{
			wxMessageDialog dialog(this, _("Are you sure you want to terminate the selected employee?"),
							_("Employee Termination"), wxYES_NO, wxDefaultPosition);
			dialog.Centre();
			dialog.ShowModal();
			
			continue;
		}

		int competency = pEmployee->GetCompetency();
		short age = pEmployee->GetAge();
		float salary = ((((float)competency)/2.0) * 0.8) + (((float)age) * 0.2);
		salary += salary * (((float)this->m_title) * 0.4);
		pEmployee->SetSalary(salary);

		wxString str_salary;
		str_salary = ToString(salary, 2).c_str();
		this->m_pHireeList->SetItem(index, 1, str_salary);
	}
}
/*------------------------------------------------------------------------*/



