#include "wnd_bank.h"
//#undef new
#include "pbutils.h"
/*----------------------------------------------------------------------------------------------*/
// CBankDlg event table
BEGIN_EVENT_TABLE(CBankDlg, wxDialog)
	EVT_BUTTON(BD_BUTTON_APPLYFORLOAN, CBankDlg::OnApplyForLoansClicked)
	EVT_BUTTON(BD_BUTTON_PAYOFFLOANS, CBankDlg::OnPayOffLoansClicked)
	EVT_BUTTON(BD_BUTTON_OK, CBankDlg::OnOkClicked)
END_EVENT_TABLE()
/*----------------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------*/
// CApplyLoanDlg event table
BEGIN_EVENT_TABLE(CApplyLoanDlg, wxDialog)
	EVT_BUTTON(ALD_BUTTON_APPLY, CApplyLoanDlg::OnApplyClicked)
	EVT_BUTTON(ALD_BUTTON_CANCEL, CApplyLoanDlg::OnCancelClicked)
END_EVENT_TABLE()
/*----------------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------*/
// CPayLoanDlg event table
BEGIN_EVENT_TABLE(CPayLoanDlg, wxDialog)
	EVT_BUTTON(PLD_BUTTON_PAY, CPayLoanDlg::OnPayClicked)
	EVT_BUTTON(PLD_BUTTON_CANCEL, CPayLoanDlg::OnCancelClicked)
END_EVENT_TABLE()
/*----------------------------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------+
|																		   |						 
|	CBankDlg implementation											   |						 
|																		   |						 
+-------------------------------------------------------------------------*/
CBankDlg::CBankDlg(wxWindow* parent, CRestaurant *pRestaurant) :
			wxDialog(parent, -1, _("Banking Dialog"), wxDefaultPosition, wxSize(400, 360), wxDEFAULT_DIALOG_STYLE)
{
	this->m_pRestaurant = pRestaurant;

	this->Centre();

	this->outstandingLoansText =  new wxStaticText(this, -1, _("Outstanding loans:"), wxPoint(10, 10));
	this->loans = new wxListBox(this, -1, wxPoint(10, 30), wxSize(370, 220));
	this->totalDebt = new wxStaticText(this, -1, _("Total debt:"), wxPoint(10, 260));
	this->applyForLoan = new wxButton(this, BD_BUTTON_APPLYFORLOAN, _("Apply for Loan"), wxPoint(185, 255));
	this->payOffLoans = new wxButton(this, BD_BUTTON_PAYOFFLOANS, _("Pay Off Loans"), wxPoint(285, 255));
	this->line = new wxStaticLine(this, -1, wxPoint(10, 290), wxSize(380, 1), wxLI_HORIZONTAL);
	this->ok = new wxButton(this, BD_BUTTON_OK, _("Ok"), wxPoint(170, 300));

	this->mainSizer = new wxBoxSizer(wxVERTICAL);

	this->mainSizer->Add(this->outstandingLoansText, 0, wxALL, 10);
	this->mainSizer->Add(this->loans, 1, wxEXPAND|wxLEFT|wxRIGHT|wxBOTTOM, 10);

	this->middleBtnSizer = new wxBoxSizer(wxHORIZONTAL);
	this->middleBtnSizer->Add(this->totalDebt, 0, wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL);
	this->middleBtnSizer->Add(10, 30, 1);
	this->middleBtnSizer->Add(this->applyForLoan, 0, wxRIGHT|wxALIGN_RIGHT, 10);
	this->middleBtnSizer->Add(this->payOffLoans, 0, wxALIGN_RIGHT);

	this->mainSizer->Add(this->middleBtnSizer, 0, wxEXPAND|wxLEFT|wxRIGHT|wxBOTTOM, 10);
	this->mainSizer->Add(this->line, 0, wxEXPAND|wxLEFT|wxRIGHT|wxBOTTOM, 10);
	this->mainSizer->Add(this->ok, 0, wxALIGN_CENTER_HORIZONTAL);

	this->SetAutoLayout(true);
	this->SetSizer(this->mainSizer);
	this->mainSizer->Fit(this);
	this->mainSizer->SetSizeHints(this);

	this->RefreshLoanBox();
}
/*----------------------------------------------------------------------------------------------*/
CBankDlg::~CBankDlg()
{
	this->outstandingLoansText->Destroy();
	this->loans->Destroy();
	this->totalDebt->Destroy();
	this->applyForLoan->Destroy();
	this->payOffLoans->Destroy();
	this->line->Destroy();
	this->ok->Destroy();
}
/*----------------------------------------------------------------------------------------------*/
void CBankDlg::RefreshLoanBox()
{
	this->loans->Clear();

	// add an entry to the listbox for each loan
	CBankAccount *pAccount = this->m_pRestaurant->m_pBankAccount;
	for(unsigned int c = 1 ; c <= pAccount->m_LoanList.GetCount() ; c++)
	{
		CBankLoan *pLoan = pAccount->m_LoanList.GetItem(c);

		wxString text;
		text << _("Loan") << " " << ToString(c).c_str() << ", " << _("Amount") << ": $" 
			 << ToString(pLoan->GetInitialAmount(), 2).c_str() << ", " << _("Balance") << ": $" 
			 << ToString(pLoan->GetBalance(), 2).c_str() << " ";
		if (pLoan->IsInEffect())
			text << "(" << _("Active") << ")";
		else
			text << "(" << _("InActive") << ")";
		this->loans->Append(text);
	}

	// update the total debt label too
	wxString text;
	text << _("Total debt") << ": " << "$" << ToString(pAccount->GetAccountBalance(), 2).c_str();
	this->totalDebt->SetLabel(text);
}
/*----------------------------------------------------------------------------------------------*/
void CBankDlg::OnApplyForLoansClicked(wxMouseEvent &e)
{
	CApplyLoanDlg dlg(this, this->m_pRestaurant);

	if (dlg.ShowModal() == wxOK)
	{
		if(dlg.GetAmount() > 0)
			this->m_pRestaurant->m_pBankAccount->CreateLoan(dlg.GetAmount(), 0.03f);
	}
	this->RefreshLoanBox();
}
/*----------------------------------------------------------------------------------------------*/
void CBankDlg::OnOkClicked(wxMouseEvent &e)
{
	this->EndModal(wxOK);
}
/*----------------------------------------------------------------------------------------------*/
void CBankDlg::OnPayOffLoansClicked(wxMouseEvent &e)
{
	CPayLoanDlg dlg(this, this->m_pRestaurant);

	if (dlg.ShowModal() == wxOK)
	{
		if(dlg.GetAmount() <= this->m_pRestaurant->GetBudget())
			this->m_pRestaurant->m_pBankAccount->PayOffLoans(dlg.GetAmount());
	}
	this->RefreshLoanBox();
}
/*----------------------------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------+
|																		   |						 
|	CApplyLoanDlg implementation										   |						 
|																		   |						 
+-------------------------------------------------------------------------*/
CApplyLoanDlg::CApplyLoanDlg(wxWindow* parent, CRestaurant* pRestaurant) :
				wxDialog(parent, -1, _("Loan Application"), wxPoint(-1, -1), wxSize(280, 160), wxDEFAULT_DIALOG_STYLE)
{
	this->amount = 0;
	this->Centre();

	this->m_pRestaurant = pRestaurant;

	wxString loan_count;
	loan_count << _("The restaurant currently has") << " "
			   << ToString(this->m_pRestaurant->m_pBankAccount->m_LoanList.GetCount()) .c_str()
			   << " " << _("outstanding loans") << " ";
	this->numLoans = new wxStaticText(this, -1, loan_count, wxPoint(10, 10));
	wxString enterTxt;
	enterTxt << _("Enter the amount of") << "\n" << _("the loan that you would") << "\n" << _("like to apply for") << ":"; 
	this->enterAmountText = new wxStaticText(this, -1, enterTxt, wxPoint(10, 40));
	this->amountOfLoan = new wxTextCtrl(this, -1, "", wxPoint(140, 45), wxSize(120, 30));
	this->line = new wxStaticLine(this, -1, wxPoint(10, 100), wxSize(260, 1), wxLI_HORIZONTAL);
	this->apply = new wxButton(this, ALD_BUTTON_APPLY, _("Apply"), wxPoint(110, 105));
	this->cancel = new wxButton(this, ALD_BUTTON_CANCEL, _("Cancel"), wxPoint(190, 105));

	this->mainSizer = new wxBoxSizer(wxVERTICAL);
	this->mainSizer->Add(this->numLoans, 0, wxALL, 10);
	this->middleSizer = new wxBoxSizer(wxHORIZONTAL);
	this->middleSizer->Add(this->enterAmountText);
	this->middleSizer->Add(this->amountOfLoan, 0, wxLEFT, 10);
	this->mainSizer->Add(this->middleSizer, 0, wxLEFT|wxRIGHT, 10);
	this->mainSizer->Add(this->line, 0, wxEXPAND|wxALL, 10);
	this->bottomBtnSizer = new wxBoxSizer(wxHORIZONTAL);
	this->bottomBtnSizer->Add(this->apply, 0, wxALIGN_RIGHT|wxRIGHT, 10);
	this->bottomBtnSizer->Add(this->cancel, 0, wxALIGN_RIGHT);
	this->mainSizer->Add(this->bottomBtnSizer, 0, wxALIGN_RIGHT|wxLEFT|wxRIGHT, 10);

	this->SetAutoLayout(true);
	this->SetSizer(this->mainSizer);
	this->mainSizer->Fit(this);
	this->mainSizer->SetSizeHints(this);
}
/*----------------------------------------------------------------------------------------------*/
CApplyLoanDlg::~CApplyLoanDlg()
{
	this->numLoans->Destroy();
	this->enterAmountText->Destroy();
	this->amountOfLoan->Destroy();
	this->line->Destroy();
	this->apply->Destroy();
	this->cancel->Destroy();
}
/*----------------------------------------------------------------------------------------------*/
double CApplyLoanDlg::GetAmount()
{
	return this->amount;
}
/*----------------------------------------------------------------------------------------------*/
void CApplyLoanDlg::OnApplyClicked(wxMouseEvent &e)
{
	this->amountOfLoan->GetValue().ToDouble(&this->amount);
	this->EndModal(wxOK);
}
/*----------------------------------------------------------------------------------------------*/
void CApplyLoanDlg::OnCancelClicked(wxMouseEvent &e)
{
	this->EndModal(wxCANCEL);
}
/*----------------------------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------+
|																		   |						 
|	CPayLoanDlg implementation										       |						 
|																		   |						 
+-------------------------------------------------------------------------*/
CPayLoanDlg::CPayLoanDlg(wxWindow* parent, CRestaurant* pRestaurant) :
				wxDialog(parent, -1, _("Pay Off Loan"), wxPoint(-1, -1), wxSize(280, 160), wxDEFAULT_DIALOG_STYLE|wxRESIZE_BORDER)
{
	this->amount = 0;
	this->Centre();

	this->m_pRestaurant = pRestaurant;

	CBankAccount *pAccount = this->m_pRestaurant->m_pBankAccount;

	wxString text;
	text << _("You have a total of") << " " << ToString(pAccount->GetActiveLoanCount()).c_str()
         << " " << _("loans at a debt of") << " $" << ToString(pAccount->GetAccountBalance(), 2).c_str();
	this->numLoans = new wxStaticText(this, -1, text, wxPoint(10, 10));

	wxString text2;
	text2 << _("Enter the amount") << "\n" << _("that you would") << "\n" << _("like to pay off") << ":";
	this->enterAmountText = new wxStaticText(this, -1, text2, wxPoint(10, 40));

	this->amountToPay = new wxTextCtrl(this, -1, "", wxPoint(140, 45), wxSize(120, 30));
	this->line = new wxStaticLine(this, -1, wxPoint(10, 100), wxSize(260, 1), wxLI_HORIZONTAL);
	this->pay = new wxButton(this, PLD_BUTTON_PAY, _("Pay"), wxPoint(110, 105));
	this->cancel = new wxButton(this, PLD_BUTTON_CANCEL, _("Cancel"), wxPoint(190, 105));

	this->mainSizer = new wxBoxSizer(wxVERTICAL);
	this->mainSizer->Add(this->numLoans, 0, wxALL, 10);
	this->middleSizer = new wxBoxSizer(wxHORIZONTAL);
	this->middleSizer->Add(this->enterAmountText, 0, wxRIGHT, 10);
	this->middleSizer->Add(this->amountToPay);
	this->mainSizer->Add(this->middleSizer, 0, wxRIGHT|wxLEFT, 10);
	this->mainSizer->Add(this->line, 0, wxEXPAND|wxALL, 10);
	this->bottomBtnSizer = new wxBoxSizer(wxHORIZONTAL);
	this->bottomBtnSizer->Add(this->pay, 0, wxRIGHT|wxALIGN_RIGHT, 10);
	this->bottomBtnSizer->Add(this->cancel, 0, wxALIGN_RIGHT);
	this->mainSizer->Add(this->bottomBtnSizer, 0, wxALIGN_RIGHT|wxLEFT|wxRIGHT, 10);

	this->SetAutoLayout(true);
	this->SetSizer(this->mainSizer);
	this->mainSizer->Fit(this);
	this->mainSizer->SetSizeHints(this);
}
/*----------------------------------------------------------------------------------------------*/
CPayLoanDlg::~CPayLoanDlg()
{
	this->numLoans->Destroy();
	this->enterAmountText->Destroy();
	this->amountToPay->Destroy();
	this->line->Destroy();
	this->pay->Destroy();
	this->cancel->Destroy();
}
/*----------------------------------------------------------------------------------------------*/
double CPayLoanDlg::GetAmount()
{
	return this->amount;
}
/*----------------------------------------------------------------------------------------------*/
void CPayLoanDlg::OnPayClicked(wxMouseEvent &e)
{
	this->amountToPay->GetValue().ToDouble(&this->amount);
	this->EndModal(wxOK);
}
/*----------------------------------------------------------------------------------------------*/
void CPayLoanDlg::OnCancelClicked(wxMouseEvent &e)
{
	this->EndModal(wxCANCEL);
}
/*----------------------------------------------------------------------------------------------*/



