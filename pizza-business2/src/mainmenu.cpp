#include "mainmenu.h"
#include <wx/filedlg.h>
#include "mainframe.h"
/*###########################################################################*/
//
//	CMainMenu Implementation
//
/*###########################################################################*/
BEGIN_EVENT_TABLE(CMainMenu, wxDialog)
	EVT_PAINT(CMainMenu::OnPaint)
	EVT_LEFT_DOWN(CMainMenu::OnPressed)

	// button events
	EVT_BUTTON(IDC_NEWGAME, CMainMenu::OnNewGame)
	EVT_BUTTON(IDC_LOADGAME, CMainMenu::OnLoadGame)
	EVT_BUTTON(IDC_SAVEGAME, CMainMenu::OnSaveGame)
	EVT_BUTTON(IDC_QUIT, CMainMenu::OnQuit)

	REPAINT_BUTTONS(-1, CMainMenu::OnPaintButton)
END_EVENT_TABLE()
/*---------------------------------------------------------------------------*/
CMainMenu::CMainMenu(wxWindow* parent, wxWindowID id, const wxString& title,
		 const wxPoint& pos, const wxSize& size, long style, const wxString& name) :
wxDialog(parent, id, title, pos, size, style, name),
m_Bmp("menu.bmp", wxBITMAP_TYPE_BMP) // load the bitmap
{
	this->m_BtnNewGame = 0;
	this->m_BtnLoadGame = 0;
	this->m_BtnSaveGame = 0;
	this->m_BtnQuit = 0;

	this->Centre();

	// New Game Button: 105, 102, 125, 25
	this->m_BtnNewGame = new CHoverButton(this, IDC_NEWGAME, "New Game",
		wxPoint(105, 102), wxSize(125, 25));

	// Load Game Button: 173,138, 130, 25
	this->m_BtnLoadGame = new CHoverButton(this, IDC_LOADGAME, "Load Game",
		wxPoint(173, 138), wxSize(130, 25));

	// Save Game Button: 230, 172, 125, 25
	CApplication *pApp = &wxGetApp();
	if(pApp->m_Simulator.IsGameInSession() == true)
		this->m_BtnSaveGame = new CHoverButton(this, IDC_SAVEGAME, "Save Game",
			wxPoint(230, 172), wxSize(125, 25));

	// Quit Button: 315, 260, 56, 30
	this->m_BtnQuit = new CHoverButton(this, IDC_QUIT, "Quit",
		wxPoint(315, 260), wxSize(56, 30));
}
/*---------------------------------------------------------------------------*/
CMainMenu::~CMainMenu()
{
	if(this->m_BtnNewGame)
		this->m_BtnNewGame->Destroy();

	if(this->m_BtnLoadGame)
		this->m_BtnLoadGame->Destroy();

	if(this->m_BtnSaveGame)
		this->m_BtnSaveGame->Destroy();

	if(this->m_BtnQuit)
		this->m_BtnQuit->Destroy();
}
/*---------------------------------------------------------------------------*/
void CMainMenu::OnLoadGame(wxCommandEvent& event)
{
	wxFileDialog fileDialog(this, "Load file", "", "", "Saved games (*.pbg)|*.pbg", wxOPEN);

	if(fileDialog.ShowModal() == wxID_OK)
	{
		CApplication *pApp = &wxGetApp();
		pApp->m_Simulator.w_LoadGame((char*)fileDialog.GetPath().GetData());

		CMainPanel *pMainPanel = (CMainPanel*)this->GetParent();

		COwner *pPlayer = pApp->m_Simulator.m_OwnerList.GetItem(1);
		pMainPanel->SetCurrentRestaurant(pPlayer->GetRestaurant());

		this->EndModal(-1);
	}
}
/*---------------------------------------------------------------------------*/
void CMainMenu::OnNewGame(wxCommandEvent& event)
{
	this->EndModal(IDC_NEWGAME);
}
/*---------------------------------------------------------------------------*/
void CMainMenu::OnPaint(wxPaintEvent& event)
{
	wxPaintDC hdc(this);

	// draw the background bitmap
	hdc.DrawBitmap(this->m_Bmp, 0, 0, false);
}
/*---------------------------------------------------------------------------*/
void CMainMenu::OnQuit(wxCommandEvent& event)
{
	this->EndModal(IDC_QUIT);
}
/*---------------------------------------------------------------------------*/
void CMainMenu::OnPaintButton(wxEvent &e)
{
	CHoverButton *pButton = 0;
	int x = 0, y = 0;

	int id = e.GetId();
	switch(id)
	{
	case IDC_NEWGAME:
		{
			pButton = this->m_BtnNewGame;
			x = 0;
			if(pButton->IsHovered())
				y = 327;
			else
				y = 301;
		}
		break;

	case IDC_LOADGAME:
		{
			pButton = this->m_BtnLoadGame;
			x = 126;
			if(pButton->IsHovered())
				y = 327;
			else
				y = 301;
		}
		break;

	case IDC_SAVEGAME:
		{
			pButton = this->m_BtnSaveGame;
			x = 257;
			if(pButton->IsHovered())
				y = 327;
			else
				y = 301;
		}
		break;

	case IDC_QUIT:
		{
			pButton = this->m_BtnQuit;
			if(pButton->IsHovered())
			{
				x = 57;
				y = 353;
			}
			else
			{
				x = 0;
				y = 353;
			}
		}
		break;
	}

	if(pButton)
	{
		// create a memoryDC for the bitmap
		// and blit portion defined by x and y
		wxSize size = pButton->GetClientSize();

		wxClientDC dc(pButton);
		wxMemoryDC temp_dc;
		temp_dc.SelectObject(this->m_Bmp);
		dc.Blit(0, 0, size.GetWidth(), size.GetHeight(), &temp_dc, x, y);
		temp_dc.SelectObject(wxNullBitmap);
	}
}
/*---------------------------------------------------------------------------*/
void CMainMenu::OnPressed(wxMouseEvent &e)
{
	CApplication *pApp = &wxGetApp();
	if(pApp->m_Simulator.IsGameInSession() == true)
		this->EndModal(0);
}
/*---------------------------------------------------------------------------*/
void CMainMenu::OnSaveGame(wxCommandEvent& event)
{
	wxFileDialog fileDialog(this, "Save file", "", "", "Saved games (*.pbg)|*.pbg", wxSAVE);

	if(fileDialog.ShowModal() == wxID_OK)
	{
		CApplication *pApp = &wxGetApp();
		pApp->m_Simulator.w_SaveGame((char*)fileDialog.GetPath().GetData());
	}
}
/*---------------------------------------------------------------------------*/



