#ifndef _JS_REGION_H
#define _JS_REGION_H
#include "list.h"

using namespace ListH;
class CRegion
{
public:
	int GetPopulationOfRegion();
	void SetPopulationOfRegion(int newPopulation);
	char* GetNameOfRegion();
	void SetNameOfRegion(char* newName);
	double GetPropertyValueOfRegion();
	void SetPropertyValueOfRegion(double propValue);
	void AddRestToRegion(int id);
	void RemoveRestFromRegion(int id);
public:
	struct {
		char name[256];
		char description[1024];
		unsigned int population;
		double propertyValue;
		int x, y, width, height;
	} data;
	List <unsigned short> m_restaurants;	
};
#endif

